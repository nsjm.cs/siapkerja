from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import ProfilSKPD
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    ProfileSKPD = ProfilSKPD.objects.filter(status='Publish').order_by('typexs','title')
    archives = ProfilSKPD.objects.filter(status='Draft').order_by('typexs','title')
    paginator = Paginator(ProfileSKPD, 15)
    try:
        ProfileSKPD = paginator.page(page)
    except PageNotAnInteger:
        ProfileSKPD = paginator.page(1)
    except EmptyPage:
        ProfileSKPD = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Profil SKPD - Admin',
        'data_ProfileSKPD' : ProfileSKPD,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/profilSKPD/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = ProfilSKPD.objects.all()
        context = {
            'title' : 'ADD Profil SKPD',
            'form' : form,
        }

        template = 'profile/admin/profilSKPD/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        title = request.POST.get('title')
        tipe = request.POST.get('tipe')
        description = request.POST.get('description')
        foto = request.FILES['foto']
        if title is not None:
            profilskpdlist = ProfilSKPD()
            profilskpdlist.title = title
            profilskpdlist.typexs = tipe
            profilskpdlist.description = description
            profilskpdlist.foto = foto
            profilskpdlist.save()
            
            messages.success(request, 'Profil dinas berhasil disimpan.')
            return redirect('profile:admin_profilskpd')

        messages.error(request, 'Profil dinas gagal disimpan.')
        return render(request, 'profile/admin/profilSKPD/create.html', {'form': form,})
    
@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        layanandinas = ProfilSKPD.objects.get(id = id)

        context = {
            'title' : 'EDIT Profil SKPD',
            'form' : form,
            'edit' : 'true',
            'layanandinas' : layanandinas,
        }
        template = 'profile/admin/profilSKPD/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        layanandinas = ProfilSKPD.objects.get(id = id)
        path_file_lama = f"{settings.MEDIA_ROOT}/{layanandinas.foto}"
        foto_old = bool(layanandinas.foto)
        title = request.POST.get('title')
        typexs = request.POST.get('typexs')
        description = request.POST.get('description')

        if layanandinas is not None:
            layanandinas.title = title
            layanandinas.typexs = typexs
            layanandinas.description = description
            layanandinas.save()
    
            if 'foto' in request.FILES:
                if foto_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                foto = request.FILES['foto']
                layanandinas.foto = foto
                layanandinas.save()

            messages.success(request, 'Profil SKPD berhasil disimpan')
            return redirect('profile:admin_layanan')

        messages.error(request, 'Profil SKPD gagal disimpan.')
        return render(request, 'profile/admin/profilSKPD/create.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        status = 'Draft'
        doc = ProfilSKPD.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except ProfilSKPD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def permanentDelete(request, id):
    message = ''
    try:
        doc = ProfilSKPD.objects.get(id=id)
        try:
            doc.image.delete()
        except:
            pass
        doc.delete()
        message = 'success'
    except ProfilSKPD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        status = 'Publish'
        doc = ProfilSKPD.objects.get(id=id)
        doc.status = status
        doc.save()
        message = 'success'
    except ProfilSKPD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)