from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.urls import reverse
from django.contrib import messages
from ..models import OPD as m_skpd, MasterKegiatan as m_kegiatan, SPKRINCIAN as spk_rincian, SPK as spk, SPP
from django.utils import timezone
import os, re, traceback, json
from django.db import transaction
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.views import View

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest

from collections import defaultdict
from django.utils.formats import number_format
from rizq_sanjaya_app.context_processors import global_variables
from django.db.models import Sum, F, Value, DecimalField
from functools import wraps
from django.db.models.functions import Coalesce

def cek_ba_spk(id_spk):
    have_spp = SPP.objects.filter(SPK_id = id_spk).exists()
    return have_spp

def spk_validation(redirect_to, message = 'SPK sudah dibuat BA Pembayaran, tidak diperbolehkan merubah isi SPK.'):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk')
            have_spp = cek_ba_spk(id_spk)

            if have_spp:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator

def cek_ba_spk_exists(id_spk):
    have_ba = SPP.objects.filter(SPK_id = id_spk).exists()
    return have_ba

def ba_validation_exists(redirect_to, message = 'SPK ini belum dibuat BA pembayaran, silahkan buat terlebih dahulu BA Pembayaran.'):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk')
            have_ba = cek_ba_spk_exists(id_spk)

            if not have_ba:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator

def cek_bast_spk_exists(id_spk):
    have_bast = BAST.objects.filter(SPK_id = id_spk).exists()
    return have_bast

def bast_validation_exists(redirect_to, message = 'BA Pembayaran ini sudah dibuat BAST, tidak diperbolehkan merubah isi BA PEMBAYARAN.', cek_is = True):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk', request.GET.get('id_spk'))
            have_bast = cek_bast_spk_exists(id_spk)
            print('hasil cek', cek_is, have_bast, id_spk)
            if cek_is and have_bast:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)
            elif not cek_is and not have_bast:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator

def cek_bapa_spk_exists(id_spk):
    have_bapa = BAP.objects.filter(SPK_id = id_spk).exists()
    return have_bapa

def bapa_validation_exists(redirect_to, message = 'BAST Sudah dibuatkan BAPA, tidak diperbolehkan mengubah BAST.'):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk')
            have_bapa = cek_bapa_spk_exists(id_spk)

            if have_bapa:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator


def cek_faktur_spk_exists(id_spk):
    have_bapa = FakturPajak.objects.filter(SPK_id = id_spk).exists()
    return have_bapa


def cek_ba_exists(id_spk):
    have_bapa = SPP.objects.filter(SPK_id = id_spk).exists()
    return have_bapa

def ba_validation_exists(redirect_to, message = 'Ba Pembayaran Sudah dibuat, tidak diperbolehkan menambahkan Ba Pembayaran kembali.'):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk')
            have_bapa = cek_ba_exists(id_spk)

            if have_bapa:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator


def faktur_validation_exists(redirect_to, message = 'BAPA Sudah dibuatkan Faktur Tagihan, tidak diperbolehkan mengubah BAPA.'):
    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            id_spk = kwargs.get('id_spk')
            have_bapa = cek_faktur_spk_exists(id_spk)

            if have_bapa:
                if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                    return JsonResponse({'status': 'error', 'message': message}, status = 400)
                else:
                    messages.warning(request, message)
                    return redirect(redirect_to)

            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return decorator

@method_decorator(login_required(), name='dispatch')
class render_kegiatan(View):
    def post(self, request):
        data = {
            'status': 'error',
            'message': 'Terjadi kesalahan server.'
        }
        
        type_ = request.POST.get('type_')
        value = request.POST.get('value')

        kwargs_filter = {
            type_: True
        }

        if type_ == 'is_program':
            kwargs_filter['skpd_id'] = value
        elif type_ == 'is_bagian':
            data_kegiatan_desc = m_kegiatan.objects.get(kegiatan_id = value).get_descendants().filter(is_bagian = True).values('kegiatan_id', 'kegiatan_nama').order_by('kegiatan_nama', 'created_at').distinct('kegiatan_nama')
            data['kegiatan'] = list(data_kegiatan_desc)
            data['status'] = 'success'
            data['message'] = 'OK'
            return JsonResponse(data, status = 201)
        else:
            kwargs_filter['parent'] = value

        try:
            with transaction.atomic():
                data_kegiatan = m_kegiatan.objects.filter(**kwargs_filter).values('kegiatan_kode', 'kegiatan_nama', 'kegiatan_id')
                data['kegiatan'] = list(data_kegiatan)
                data['status'] = 'success'
                data['message'] = 'OK'
            return JsonResponse(data, status = 201)
        except Exception as e:
            traceback.print_exc()
            print('Error get data', e)
            return JsonResponse(data, status = 400)

        return JsonResponse(data, status = 400)

def get_sisa_pagu(tahapan, rekening_id, spk_id, exclude_current = True):
    sisa_pagu = 0
    dt_rekening = m_kegiatan.objects.get(kegiatan_id = rekening_id)
    if exclude_current:
        dt_spk = spk.objects.get(id = spk_id)

        # ini nanti yang akan dibingungkan ketika berganti tahapan

        dt_spk_rincian = spk_rincian.objects.select_for_update().filter(ssh = dt_rekening, SPK__tahapan = tahapan, SPK__status = 'Aktif').exclude(SPK = dt_spk)
    else:
        dt_spk_rincian = spk_rincian.objects.select_for_update().filter(ssh = dt_rekening, SPK__tahapan = tahapan, SPK__status = 'Aktif')
    
    total_realisasi_lainnya = 0
    for realisasi in dt_spk_rincian:
        total_realisasi_lainnya += realisasi.realisasi

    # print(total_realisasi_lainnya)
    pagu_rekening = dt_rekening.volume * dt_rekening.nilai

    sisa_pagu = pagu_rekening - total_realisasi_lainnya

    # print(pagu_rekening, total_realisasi_lainnya, sisa_pagu)

    # dt_spk_rincian = 
    return sisa_pagu

def get_sisa_pagu_laporan(tahapan, rekening_id):
    sisa_pagu = 0
    dt_rekening = m_kegiatan.objects.get(kegiatan_id = rekening_id)
    dt_spk_rincian = spk_rincian.objects.select_for_update().filter(ssh = dt_rekening, SPK__tahapan = tahapan, SPK__status = 'Aktif')
    total_realisasi_lainnya = 0
    for realisasi in dt_spk_rincian:
        total_realisasi_lainnya += realisasi.realisasi

    # print(total_realisasi_lainnya)
    pagu_rekening = dt_rekening.volume * dt_rekening.nilai

    sisa_pagu = pagu_rekening - total_realisasi_lainnya

    # print(pagu_rekening, total_realisasi_lainnya, sisa_pagu)

    # dt_spk_rincian = 
    return sisa_pagu

def get_total_realisasi(tahapan, rekening_id):
    sisa_pagu = 0
    dt_rekening = m_kegiatan.objects.get(kegiatan_id = rekening_id)
    dt_spk_rincian = spk_rincian.objects.select_for_update().filter(ssh = dt_rekening, SPK__tahapan = tahapan, SPK__status = 'Aktif')
    total_realisasi_lainnya = 0
    for realisasi in dt_spk_rincian:
        total_realisasi_lainnya += realisasi.realisasi


    return total_realisasi_lainnya


from django.template.loader import render_to_string
import tempfile, uuid, json, subprocess

def generate_report(html_file, data_html, pdf_options):
    
    report_id = str(uuid.uuid4())

    html_content = render_to_string(html_file, data_html)

    tipe_render = 'using_file_path'

    custom_dir = os.path.join(settings.MEDIA_ROOT, 'temp_files')
    os.makedirs(custom_dir, exist_ok=True)

    with tempfile.NamedTemporaryFile(delete=False, prefix=report_id, suffix='.html', dir=custom_dir, mode="w") as temp_html_file:
        temp_html_file.write(html_content)
        temp_html_file.flush()  # Ensure data is written to disk
        tmp_html_path = temp_html_file.name
        os.chmod(tmp_html_path, 0o770)

    with tempfile.NamedTemporaryFile(delete=False, suffix=".pdf") as tmp_pdf:
        output_path = tmp_pdf.name

    pdf_options['path'] = output_path

    subprocess.run([settings.DEFAULT_PYTHON, os.path.join(settings.BASE_DIR, 'rizq_sanjaya_app', "generate_report_pdf.py"), tipe_render, json.dumps(pdf_options), report_id, tmp_html_path])

    file_read = None
    with open(output_path, "rb") as pdf_file:
        file_read =  pdf_file.read()

    os.remove(output_path)

    return file_read

def get_total_nilai_spk(spk_id):
    dt_spk = spk.objects.get(id = spk_id)

    total_spk = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']

    return total_spk if total_spk is not None else 0

def cek_nomor_terbaru(qset, nomor_field, nomor_format = None,nomor = '001'):

    with transaction.atomic():
        all_data = [x.split('/')[0] for x in qset.objects.select_for_update().values_list(nomor_field, flat=True)]

        try:
            new_number = int(max(all_data))+1
        except Exception as e:
            new_number = '001'

        if nomor_format is not None:
            nomor_format = nomor_format.split('/')
            nomor_format[0] = f"{new_number:03d}"
            nomor_format = '/'.join(nomor_format)

    return nomor_format

    # if all_data:
    #     nomor_urut_list = [
    #         int(regex.search(spk.nospk).group(1)) 
    #         for spk in all_data if regex.search(spk.nospk)
    #     ]
        
    #     last_number =  max(nomor_urut_list) if nomor_urut_list else 0

        
    #     no_spk = last_number + 1
    #     spk_no = f"{no_spk:03d}"

