from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import perusahaan as tb_perusahaan, OPD as m_skpd, MasterKegiatan as m_kegiatan, SPKRINCIAN as spk_rincian
from django.utils import timezone
import os, io
from django.urls import reverse
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db import transaction
import re, pprint, json, math
from rizq_sanjaya_app.context_processors import global_variables
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, spk_validation, cek_ba_spk, cek_nomor_terbaru
from decimal import Decimal
from django.db.models import Sum, F, Value, DecimalField, Q
from django.db.models.functions import Coalesce
import fitz
from rizq_sanjaya_app.support_function import PagingHelper

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    orgid = request.user.id_opd_id
    if request.user.is_staff or request.user.role == "admin" :
        skpd_list = OPD.objects.filter(status='Aktif')
    else:
        skpd_list = OPD.objects.filter(status='Aktif', id = orgid )

    # Keperluan Datatable
    page_obj = PagingHelper.spk(request)
    # END Keperluan Datatable

    wilayah_list = lokasi.objects.filter(status='Aktif')
    perusahaan_list = tb_perusahaan.objects.filter(status='Aktif')
    archives = SPK.objects.filter(status='Tidak aktif')
    pengesah = pejabatpengesah.objects.filter(status='Aktif')

    context = {
        'title' : 'Data SPK',
        'page_obj': page_obj,
        'wilayah_list' : wilayah_list,
        'skpd_list' : skpd_list,
        
        'archives' : archives,
        'pengesah' : pengesah,

    }
    
    return render(request, 'profile/admin/spk/index.html', context)

@login_required
@is_verified()
def create(request):
    if request.method == 'GET':
        orgid = request.user.id_opd_id
        if request.user.is_staff or request.user.role == "admin" :
            skpd_list = OPD.objects.filter(status='Aktif')
        else:
            skpd_list = OPD.objects.filter(status='Aktif', id = orgid )
        tahapan = MasterTahapan.objects.get(status=True)
        perusahaan_list = tb_perusahaan.objects.filter(status='Aktif')
        regex = re.compile(r'^(\d{3})/')

        spk_no = f"{1:03d}"


        all_spk = SPK.objects.all()
        if all_spk:
            nomor_urut_list = [
                int(regex.search(spk.nospk).group(1)) 
                for spk in all_spk if regex.search(spk.nospk)
            ]

            last_number =  max(nomor_urut_list) if nomor_urut_list else 0

            
            no_spk = last_number + 1
            spk_no = f"{no_spk:03d}"

        context = {
            'title' : 'Tambah Surat SPK',
            'skpd_list' : skpd_list,
            'tahapan' : tahapan,
            'spk_no' : spk_no,
            'perusahaan_list' : perusahaan_list,
        }
        
        return render(request, 'profile/admin/spk/form.html', context)
    
    if request.method == 'POST':
        nospk = request.POST.get('nospk')
        uraipekerjaan = request.POST.get('uraipekerjaan')
        perusahaan = request.POST.get('perusahaan')
        tglspk = request.POST.get('tglspk')
        # sumberdana = request.POST.get('sumberdana')
        waktupelaksanaan = request.POST.get('waktupelaksanaan')
        opd = request.POST.get('opd')

        frm_program = request.POST.get('frm_program')
        frm_kegiatan = request.POST.get('frm_kegiatan')
        frm_subkegiatan = request.POST.get('frm_subkegiatan')
        frm_bagian = request.POST.get('frm_bagian')

        nospk = cek_nomor_terbaru(SPK, 'nospk', nomor_format = nospk)
        
        if nospk is not None:
            try:
                with transaction.atomic():
                    insert_SPK = SPK()
                    insert_SPK.nospk = nospk
                    insert_SPK.tglspk = tglspk
                    insert_SPK.uraipekerjaan = uraipekerjaan
                    # insert_SPK.sumberdana = sumberdana
                    insert_SPK.waktupelaksanaan = waktupelaksanaan
                    insert_SPK.opd_id = opd
                    insert_SPK.perusahaan_id = perusahaan
                    insert_SPK.tahapan = global_variables(request)['tahapan_aktif']
                    
                    insert_SPK.program = m_kegiatan.objects.get(kegiatan_id = frm_program)
                    insert_SPK.kegiatan = m_kegiatan.objects.get(kegiatan_id = frm_kegiatan)
                    insert_SPK.subkegiatan = m_kegiatan.objects.get(kegiatan_id = frm_subkegiatan)
                    insert_SPK.bagian = m_kegiatan.objects.get(kegiatan_id = frm_bagian)

                    insert_SPK.save()
                messages.success(request, 'Data spk berhasil disimpan.')
                return redirect(reverse('profile:admin_spk_detail', args=[insert_SPK.id]))
            except Exception as e:
                print('Error insert data SPK', e)
        messages.error(request, 'Data spk gagal disimpan.')
        return redirect('profile:admin_spk_create')



@login_required
@is_verified()
def edit(request, id_spk):
    if request.method == 'GET':
        orgid = request.user.id_opd_id
        if request.user.is_staff or request.user.role == "admin" :
            skpd_list = OPD.objects.filter(status='Aktif')
        else:
            skpd_list = OPD.objects.filter(status='Aktif', id = orgid )
        tahapan = MasterTahapan.objects.get(status=True)
        perusahaan_list = tb_perusahaan.objects.filter(status='Aktif')
        item_spk =  SPK.objects.get(id = id_spk)

        context = {
            'title' : 'Tambah Surat SPK',
            'skpd_list' : skpd_list,
            'tahapan' : tahapan,
            'item_spk' : item_spk,
            'edit' : True,
            'perusahaan_list' : perusahaan_list,
        }
        
        return render(request, 'profile/admin/spk/form.html', context)
    
    if request.method == 'POST':
        data_spk = SPK.objects.get(id = id_spk)
        nospk = request.POST.get('nospk')
        uraipekerjaan = request.POST.get('uraipekerjaan')
        perusahaan = request.POST.get('perusahaan')
        tglspk = request.POST.get('tglspk')
        # sumberdana = request.POST.get('sumberdana')
        waktupelaksanaan = request.POST.get('waktupelaksanaan')
        opd = request.POST.get('opd')

        frm_program = request.POST.get('frm_program')
        frm_kegiatan = request.POST.get('frm_kegiatan')
        frm_subkegiatan = request.POST.get('frm_subkegiatan')
        frm_bagian = request.POST.get('frm_bagian')

        remove_all_rincian = False

        if data_spk is not None:
            try:
                with transaction.atomic():
                    data_spk.nospk = nospk
                    data_spk.tglspk = tglspk
                    data_spk.uraipekerjaan = uraipekerjaan
                    # data_spk.sumberdana = sumberdana
                    data_spk.waktupelaksanaan = waktupelaksanaan
                    data_spk.opd_id = opd
                    data_spk.perusahaan_id = perusahaan

                    if data_spk.program.kegiatan_id != frm_program:
                        remove_all_rincian = True
                    elif data_spk.kegiatan.kegiatan_id != frm_kegiatan:
                        remove_all_rincian = True
                    elif data_spk.subkegiatan.kegiatan_id != frm_subkegiatan:
                        remove_all_rincian = True
                    elif data_spk.bagian.kegiatan_id != frm_bagian:
                        remove_all_rincian = True


                    data_spk.program = m_kegiatan.objects.get(kegiatan_id = frm_program)
                    data_spk.kegiatan = m_kegiatan.objects.get(kegiatan_id = frm_kegiatan)
                    data_spk.subkegiatan = m_kegiatan.objects.get(kegiatan_id = frm_subkegiatan)
                    data_spk.bagian = m_kegiatan.objects.get(kegiatan_id = frm_bagian)

                 
                    data_spk.save()

                    if remove_all_rincian:
                        spk_rincian.objects.filter(SPK = data_spk).delete()
                        print('Rincian dihapus semua...')
                        messages.success(request, 'Data SPK berhasil Diubah, semua rincian telah terhapus karena Data Awal tidak sama dengan Data Terbaru.')
                    else:
                        messages.success(request, 'Data SPK berhasil Diubah')

                    return redirect(reverse('profile:admin_spk_detail', args=[id_spk]))
            except Exception as e:
                print('Gagal edit SPK', e)
                messages.error(request, 'Data gagal diubah.')
                return redirect('profile:admin_spk')


@login_required
@is_verified()
def softDelete(request, id_spk):
    context = {
            'message' : '',
        }

    try:
        with transaction.atomic():
            sekarang = timezone.now()
            doc = SPK.objects.get(id=id_spk)
            doc.deleted_at = sekarang
            doc.status = 'Tidak aktif'
            doc.save()
            context['message'] = 'success'
            return JsonResponse(context, status = 200)
    except SPK.DoesNotExist:
        context['message'] = 'error'
        return JsonResponse(context, status = 400)
    except Exception as e:
        print('arsip spk error', e)
        context['message'] = 'Terjadi kesalahan server.'
        return JsonResponse(context, status = 400)

    

@login_required
@is_verified()
def restore(request, id):
    context = {
            'message' : '',
        }

    try:
        with transaction.atomic():
            doc = SPK.objects.get(id=id, status = 'Tidak aktif')
            doc.deleted_at = None
            doc.status = 'Aktif'

            rekening = spk_rincian.objects.filter(SPK = doc)

            for rek in rekening:
                dt_ssh = rek.ssh

                sisa_pagu = get_sisa_pagu(doc.tahapan, dt_ssh.kegiatan_id, doc.id)

                if sisa_pagu - Decimal(str(rek.realisasi)) >= 0:
                    continue
                else:
                    dt_rekening_belanja = dt_ssh.get_ancestors().get(is_rekening_belanja = True)
                    context['status'] = 'error'
                    context['message'] = f'Rekening <b>{dt_rekening_belanja.kegiatan_kode} - {dt_rekening_belanja.kegiatan_nama}<b> <b>({dt_ssh.uraian_ssh})</b> Melebihi Pagu Anggaran.'
                    context['rekening'] = dt_ssh.kegiatan_id
                    context['sisa_pagu_tersedia'] = sisa_pagu
                    return JsonResponse(context, status = 400)

            doc.save()
            context['message'] = 'success'
            return JsonResponse(context, status = 200)

    except SPK.DoesNotExist:
        context['message'] = 'error'

    return JsonResponse(context, status = 400)

# # # # # # # # # # #DETAIL SPK # # # # # # # # # # # # # # # # 
@login_required
@is_verified()
def detailSPK(request, id_spk):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        try:
            data_detail_spk = get_object_or_404(SPK, id=id_spk)
        except Exception as e:
            messages.error(request, 'SPK tidak ditemukan.')
            return redirect('profile:admin_spk')

        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id_spk)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id_spk)
        data_rincian = []

        data_subkegiatan = m_kegiatan.objects.get(kegiatan_id = data_detail_spk.subkegiatan.kegiatan_id).get_descendants().filter(is_sumberdana = True)

        pagu_subkegiatan = 0

        try:
            with transaction.atomic():
                for x in data_subkegiatan:
                    for rekening in x.get_ancestors().filter(is_rekening_belanja = True):
                        data_rekening = {'kegiatan_kode': rekening.kegiatan_kode, 'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 'children': []}
                        for bagian in rekening.get_descendants().filter(is_bagian = True, kegiatan_nama = data_detail_spk.bagian.kegiatan_nama):
                            data_bagian = {'kegiatan_kode': '', 'kegiatan_nama': bagian.kegiatan_nama, 'children': []}
                            for keterangan in bagian.get_descendants().filter(is_subrekening_belanja = True):
                                pagu_per_keterangan = 0
                                data_keterangan = {'kegiatan_kode': '', 'kegiatan_nama': keterangan.kegiatan_nama, 'children': [], 'pagu_per_keterangan': pagu_per_keterangan}
                                for ssh in keterangan.get_descendants():
                                    subtotal = ssh.nilai * ssh.volume
                                    pagu_subkegiatan+=subtotal
                                    try:
                                        current_value = SPKRINCIAN.objects.get(SPK = data_detail_spk, ssh__kegiatan_id= ssh.kegiatan_id).realisasi
                                    except Exception as e:
                                        print('Error get current value', e)
                                        current_value = 0

                                    data_ssh = {
                                            'kegiatan_kode': '', 'kegiatan_nama': ssh.uraian_ssh, 
                                            'volume': ssh.volume,
                                            'satuan': ssh.satuan,
                                            'nilai': ssh.nilai,
                                            'subtotal': subtotal,
                                            'kegiatan_id': ssh.kegiatan_id,
                                            'sisa_pagu': round(get_sisa_pagu(data_detail_spk.tahapan, ssh.kegiatan_id, data_detail_spk.id), 2),
                                            'current_value': current_value,
                                    }

                                    data_keterangan['children'].append(data_ssh)
                                    data_keterangan['pagu_per_keterangan'] += subtotal
                                data_rekening['children'].append(data_keterangan)
                        data_rincian.append(data_rekening)
        except Exception as e:
            print('Error get detail SPK', e)
            pass

        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            "data_rincian": data_rincian,
            "pagu_subkegiatan": pagu_subkegiatan,
            'have_spp': cek_ba_spk(id_spk),
           
        }
        template = 'profile/admin/spk/detail_spk.html'
        return render(request, template, context)

    if request.method == 'POST':
        data = {
            'status': 'error',
            'message': 'Terjadi kesalahan server.'
        }

        spk = request.POST.get('spk')
        rekening = json.loads(request.POST.get('rekening', []))

        if len(rekening) == 0:
            data['message'] = 'Silahkan isikan realisasi terlebih dahulu.'
            return JsonResponse(data, status = 400)
        try:
            with transaction.atomic():
                dt_spk = SPK.objects.get(id = spk)
                spk_rincian.objects.filter(SPK = dt_spk).delete()
                for rek in rekening:

                    try:
                        dt_rincianspk = spk_rincian.objects.select_for_update().get(SPK = dt_spk, ssh__kegiatan_id = rek['rekening'])
                    except Exception as e:
                        dt_rincianspk = spk_rincian()
                        dt_rincianspk.SPK = dt_spk

                    dt_ssh = m_kegiatan.objects.get(kegiatan_id = rek['rekening'], tahapan = dt_spk.tahapan)
                    dt_rincianspk.ssh = dt_ssh

                    dt_rincianspk.uraian_ssh = dt_ssh.uraian_ssh 
                    dt_rincianspk.volume = dt_ssh.volume
                    dt_rincianspk.satuan = dt_ssh.satuan
                    dt_rincianspk.nilai = dt_ssh.nilai

                    sisa_pagu = get_sisa_pagu(dt_spk.tahapan, rek['rekening'], dt_spk.id)

                    if sisa_pagu - Decimal(str(rek['value'])) >= 0:
                        dt_rincianspk.realisasi = rek['value']
                        dt_rincianspk.save()
                    else:
                        dt_rekening_belanja = dt_ssh.get_ancestors().get(is_rekening_belanja = True)
                        data['status'] = 'error'
                        data['message'] = f'Rekening <b>{dt_rekening_belanja.kegiatan_kode} - {dt_rekening_belanja.kegiatan_nama}<b> <b>({dt_ssh.uraian_ssh})</b> Melebihi Pagu Anggaran.'
                        data['rekening'] = rek['rekening']
                        data['sisa_pagu_tersedia'] = sisa_pagu
                        return JsonResponse(data, status = 400)

                data['status'] = 'success'
                data['message'] = 'Detail SPK berhasil disimpan.'

                return JsonResponse(data, status = 201)
        except Exception as e:
            print('Error ubah detail SPK', e)
            return JsonResponse(data, status = 400)
    
@login_required
@is_verified()
def createRincianSPK(request, id):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = SPKRINCIAN.objects.all()
        data_rincianspk = SPKRINCIAN.objects.all()
        context = {
            'title' : 'Data Rincian SPK',
            'form' : form,
            'data_rincianspk':data_rincianspk,
        }

        template = 'profile/admin/spk/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        uraianbelanja = request.POST.get('uraianbelanja')
        
       
        if uraianbelanja is not None:
            insert_SPKRINCIAN = SPKRINCIAN()
            insert_SPKRINCIAN.uraianbelanja = uraianbelanja
            insert_SPKRINCIAN.SPK_id = id

            insert_SPKRINCIAN.save()
            messages.success(request, 'Data spk berhasil disimpan.')
            return redirect('profile:admin_spk_detail',id)
        messages.error(request, 'Data spk gagal disimpan.')
        return render(request, 'profile/admin/spk/index.html', {'form': form,})
    
# insertsubrian
@login_required
@is_verified()
def insertsubrian(request, id):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = SPKSUBRINCIAN.objects.all()
        data_rincianspk = SPKSUBRINCIAN.objects.all()
        context = {
            'title' : 'Data Sub Rincian SPK',
            'form' : form,
            'data_rincianspk':data_rincianspk,
        }

        template = 'profile/admin/spk/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        spkrincian_id = request.POST.get('spk_idspkrincian_id')
        suburaianbelanja = request.POST.get('suburaianbelanja')
        qty = request.POST.get('qty')
        harga = request.POST.get('harga')
        satuan = request.POST.get('satuan')
        
       
        if spkrincian_id is not None:
            insert_SPKSUBRINCIAN = SPKSUBRINCIAN()
            insert_SPKSUBRINCIAN.SPKRINCIAN_id = spkrincian_id
            insert_SPKSUBRINCIAN.suburaianbelanja = suburaianbelanja
            insert_SPKSUBRINCIAN.qty = qty
            insert_SPKSUBRINCIAN.satuan = satuan
            insert_SPKSUBRINCIAN.harga = harga
            insert_SPKSUBRINCIAN.save()
            messages.success(request, 'Data spk berhasil disimpan.')
            return redirect('profile:admin_spk_detail',id)
        messages.error(request, 'Data spk gagal disimpan.')
        return render(request, 'profile/admin/spk/index.html', {'form': form,})


@login_required
@is_verified()
def editsubrian(request, id, id_sub):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = SPKSUBRINCIAN.objects.all()
        data_rincianspk = SPKSUBRINCIAN.objects.all()
        context = {
            'title' : 'Data Sub Rincian SPK',
            'form' : form,
            'data_rincianspk':data_rincianspk,
        }

        template = 'profile/admin/spk/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        spkrincian_id = request.POST.get('spk_idspkrincian_id')
        suburaianbelanja = request.POST.get('suburaianbelanja')
        qty = request.POST.get('qty')
        harga = request.POST.get('harga')
        satuan = request.POST.get('satuan')
        created_at = request.POST.get('created_at')
        
       
        if spkrincian_id is not None:
            insert_SPKSUBRINCIAN = SPKSUBRINCIAN(id=id_sub)
            insert_SPKSUBRINCIAN.SPKRINCIAN_id = spkrincian_id
            insert_SPKSUBRINCIAN.suburaianbelanja = suburaianbelanja
            insert_SPKSUBRINCIAN.qty = qty
            insert_SPKSUBRINCIAN.satuan = satuan
            insert_SPKSUBRINCIAN.harga = harga
            insert_SPKSUBRINCIAN.created_at = created_at
            insert_SPKSUBRINCIAN.save()
            messages.success(request, 'Data spk berhasil disimpan.')
            return redirect('profile:admin_spk_detail',id)
        messages.error(request, 'Data spk gagal disimpan.')
        return render(request, 'profile/admin/spk/index.html', {'form': form,})
    
# CETAK DOKUMEN
@login_required
@is_verified()
def cetakSPK(request, id):
    template = ''
    context = {} 
    form = ''
    
    
    if request.method == 'GET':
        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        id_pengesah = request.GET.get('pengesah')

        dt_pengesah = get_object_or_404(pejabatpengesah, id=id_pengesah)
        jumlah_harga = 0

        for x in data_rincian_spk:
            for subspk in x.spksubrincian_set.all():
                dt_harga = subspk.harga * subspk.qty
                jumlah_harga += dt_harga

        list_rekening = []

        jumlah_harga = 0
        nilai_pekerjaan = 0

        subkegiatan_id_list = []

        rincian_rekening = {}

        for x in data_detail_spk.spkrincian_set.all():
            subkegiatan = x.ssh.get_ancestors().get(is_subkegiatan = True)
            list_rekening.append(subkegiatan.kegiatan_nama)
            jumlah_harga += x.realisasi
            subkegiatan_id_list.append(subkegiatan.kegiatan_id)

            if x.ssh.parent.kegiatan_nama not in rincian_rekening.keys():
                rincian_rekening[x.ssh.parent.kegiatan_nama] = []

            rincian_rekening[x.ssh.parent.kegiatan_nama].append(
                        {'uraian_ssh': x.ssh.uraian_ssh,
                        'volume': x.realisasi / x.ssh.nilai,
                        'satuan': x.ssh.satuan,
                        'nilai': x.ssh.nilai,
                        'realisasi': x.realisasi
                        }
                    )

        unique_subkegiatan_id_list = list(set(subkegiatan_id_list))
        unique_list_rekening = list(set(list_rekening))

        for subkeg in unique_subkegiatan_id_list:
            sshnya = m_kegiatan.objects.get(kegiatan_id = subkeg).get_descendants().filter(is_ssh = True, parent__parent__kegiatan_nama = data_detail_spk.bagian.kegiatan_nama).aggregate(total_per_subkeg = Sum(F('volume') * F('nilai')))['total_per_subkeg']
            nilai_pekerjaan+=sshnya


        data_detail_spk.list_rekening = unique_list_rekening
        data_detail_spk.jumlah_harga = jumlah_harga
        data_detail_spk.nilai_pekerjaan = nilai_pekerjaan
        data_detail_spk.rincian_rekening = rincian_rekening
                
        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            'dt_pengesah' : dt_pengesah,
            'jumlah_harga' : jumlah_harga,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',           
        }

        pdf_format = {
            'format': 'A4',
            'prefer_css_page_size': True,
            # 'outline': True,
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2cm', 'right':'2cm', 'bottom':'2cm', 'left':'2cm'},
            'display_header_footer': False,
            'header_template':'<div></div>',
            'footer_template': '<div></div>',
            # 'landscape': True,
        }

        template = 'profile/admin/spk/print_spk.html'

        pdf_file = generate_report(template, context, pdf_format)

        doc = fitz.open(stream=pdf_file, filetype="pdf")
        total_pages = doc.page_count
        doc.close()

        context['totalPage'] = total_pages
        
        pdf_file = generate_report(template, context, pdf_format)


        return HttpResponse(pdf_file, content_type='application/pdf')

# CETAK DOKUMEN
@login_required
@is_verified()
def printSPK(request, id):
    template = ''
    context = {} 
    form = ''
    
    data_detail_spk = get_object_or_404(SPK, id=id)
    data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
    data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
    
    context = {
        'title' : 'Detail SPK',
        'form' : form,
        'edit' : 'true',
        'data_spk' : data_detail_spk,
        'data_rincianSPK' : data_rincian_spk,
        'data_subrincian_spk' : data_subrincian_spk,
    }
        
    template = 'profile/admin/spk/print_spk.html'
    return render(request, template, context)
 
    
 