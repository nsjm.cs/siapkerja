from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from ..models import FakturPajak, FakturPajakRincian
from django.utils import timezone
import os
from django.urls import reverse
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db import transaction
import re, pprint, json, math
from rizq_sanjaya_app.context_processors import global_variables
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, spk_validation, cek_ba_spk
from decimal import Decimal
from django.db.models import Sum, F, Value, DecimalField, Q, F
from django.db.models.functions import Coalesce
from datetime import datetime
import pandas as pd

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    tanggal = request.GET.get('tanggal', '')
    search = request.GET.get('search', '')

    kwargs_filter = {}
    kwargs_search = {}

    if tanggal:
        try:
            tanggal_parsed = datetime.strptime(tanggal, "%Y-%m-%d").date()
            kwargs_filter['tglfaktur'] = tanggal_parsed
        except ValueError:
            tanggal_parsed = None
            tanggal = ''
    else:
        tanggal_parsed = None

    if search:
        hasil_search = search
        kwargs_search['SPK__nospk__icontains'] = search
        kwargs_search['SPK__opd__namaopd__icontains'] = search
        kwargs_search['SPK__uraipekerjaan__icontains'] = search
        kwargs_search['SPK__bagian__kegiatan_nama__icontains'] = search
        kwargs_search['SPK__subkegiatan__kegiatan_nama__icontains'] = search
    else:
        hasil_search = ''


    queryset = FakturPajakRincian.objects.filter(SPK__status='Aktif')

    if kwargs_filter:
        queryset = queryset.filter(**kwargs_filter)

    if kwargs_search:
        q_objects = Q()
        for key, value in kwargs_search.items():
            q_objects |= Q(**{key: value})
        queryset = queryset.filter(q_objects)


    list_faktur = queryset.order_by('-created_at')

    total_keseluruhan = 0  
    grouped_data = {}

    for data in list_faktur:
        bagian = data.SPK.bagian.kegiatan_nama
        kegiatan = data.SPK.subkegiatan.kegiatan_nama
        skpd = data.SPK.opd.namaopd
        no_spk = data.SPK.nospk
        key = (bagian, kegiatan, skpd)  

        data_spk = {
            'nospk': no_spk,
            'uraispk': data.SPK.uraipekerjaan,
            'perusahaan': data.SPK.perusahaan.namaperusahaan,
            'total_tagihan': data.totalpembayaran,
        }

        if key not in grouped_data:
            grouped_data[key] = {
                'tanggal': data.tglfaktur,
                'bagian': bagian,
                'kegiatan': kegiatan,
                'skpd': skpd,
                'total_spk': 0,
                'children': [],
                'existing_nospk': set()  
            }

        if no_spk not in grouped_data[key]['existing_nospk']:
            grouped_data[key]['children'].append(data_spk)
            grouped_data[key]['total_spk'] += data.totalpembayaran
            grouped_data[key]['existing_nospk'].add(no_spk)

    for item in grouped_data.values():
        del item['existing_nospk']

    list_data_faktur = list(grouped_data.values())
    total_keseluruhan = sum(item['total_spk'] for item in grouped_data.values())

    print(list_data_faktur)

    
    tanggal_search = tanggal
   
    context = {
        'title' : 'Master Pajak',
        'list_faktur' : list_faktur,
        'list_data_faktur' : list_data_faktur,
        'total_keseluruhan' : total_keseluruhan,
        'tanggal' : tanggal,
        'hasil_search' : hasil_search,
        'tanggal_search' : tanggal_search,
    }
    
    return render(request, 'profile/admin/laporan/rekapitulasi_tagihan.html', context)

@login_required
@is_verified()
def CetakLaporanTagihan(request):
    tanggal = request.GET.get('tanggal', '')
    search = request.GET.get('search', '')

    kwargs_filter = {}
    kwargs_search = {}

    if tanggal:
        try:
            tanggal_format = datetime.strptime(tanggal, "%Y-%m-%d")
            tanggal_parsed = datetime.strptime(tanggal, "%Y-%m-%d").date()
            kwargs_filter['tglfaktur'] = tanggal_parsed
        except ValueError:
            tanggal = ''
    else:
        tanggal_format = ''

    if search:
        kwargs_search['SPK__nospk__icontains'] = search
        kwargs_search['SPK__opd__namaopd__icontains'] = search
        kwargs_search['SPK__uraipekerjaan__icontains'] = search
        kwargs_search['SPK__bagian__kegiatan_nama__icontains'] = search
        kwargs_search['SPK__subkegiatan__kegiatan_nama__icontains'] = search


    queryset = FakturPajakRincian.objects.filter(SPK__status='Aktif')

    if kwargs_filter:
        queryset = queryset.filter(**kwargs_filter)

    if kwargs_search:
        q_objects = Q()
        for key, value in kwargs_search.items():
            q_objects |= Q(**{key: value})
        queryset = queryset.filter(q_objects)


    list_faktur = queryset.order_by('-created_at')


    grouped_data = {}
    total_keseluruhan = 0  

    for data in list_faktur:
        bagian = data.SPK.bagian.kegiatan_nama
        kegiatan = data.SPK.subkegiatan.kegiatan_nama
        skpd = data.SPK.opd.namaopd
        no_spk = data.SPK.nospk
        key = (bagian, kegiatan, skpd)  

        data_spk = {
            'nospk': no_spk,
            'uraispk': data.SPK.uraipekerjaan,
            'perusahaan': data.SPK.perusahaan.namaperusahaan,
            'total_tagihan': data.totalpembayaran,
        }

        if key not in grouped_data:
            grouped_data[key] = {
                'tanggal': data.tglfaktur,
                'bagian': bagian,
                'kegiatan': kegiatan,
                'skpd': skpd,
                'total_spk': 0,
                'children': [],
                'existing_nospk': set()  
            }

        if no_spk not in grouped_data[key]['existing_nospk']:
            grouped_data[key]['children'].append(data_spk)
            grouped_data[key]['total_spk'] += data.totalpembayaran
            grouped_data[key]['existing_nospk'].add(no_spk)

    for item in grouped_data.values():
        del item['existing_nospk']

    list_data_faktur = list(grouped_data.values())
    total_keseluruhan = sum(item['total_spk'] for item in grouped_data.values())


   
    context = {
        'title' : 'Master Pajak',
        'list_faktur' : list_faktur,
        'list_data_faktur' : list_data_faktur,
        'total_keseluruhan' : total_keseluruhan,
        'tanggal' : tanggal_format,
        'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    }
            
    pdf_format = {
        'format': 'A4',
        # 'width': "21cm",
        # "height": "21.97cm",
        'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
        'display_header_footer':True,
        'header_template':"<div></div>",
        'footer_template':"<div></div>",
        'landscape': True,
    }

    pdf_file = generate_report('profile/admin/laporan/print_tagihan.html', context, pdf_format)

    return HttpResponse(pdf_file, content_type='application/pdf')


@login_required
@is_verified()
def CetakLaporanTagihanExcel(request):
    tanggal = request.GET.get('tanggal', '')
    search = request.GET.get('search', '')

    kwargs_filter = {}
    kwargs_search = {}

    if tanggal:
        try:
            tanggal_format = datetime.strptime(tanggal, "%Y-%m-%d")
            tanggal_parsed = datetime.strptime(tanggal, "%Y-%m-%d").date()
            kwargs_filter['tglfaktur'] = tanggal_parsed
        except ValueError:
            tanggal = ''
    else:
        tanggal_format = ''

    if search:
        kwargs_search['SPK__nospk__icontains'] = search
        kwargs_search['SPK__opd__namaopd__icontains'] = search
        kwargs_search['SPK__uraipekerjaan__icontains'] = search
        kwargs_search['SPK__bagian__kegiatan_nama__icontains'] = search
        kwargs_search['SPK__subkegiatan__kegiatan_nama__icontains'] = search

    queryset = FakturPajakRincian.objects.filter(SPK__status='Aktif')

    if kwargs_filter:
        queryset = queryset.filter(**kwargs_filter)

    if kwargs_search:
        q_objects = Q()
        for key, value in kwargs_search.items():
            q_objects |= Q(**{key: value})
        queryset = queryset.filter(q_objects)

    list_faktur = queryset.order_by('-created_at')

    grouped_data = {}
    total_keseluruhan = 0

    for data in list_faktur:
        bagian = data.SPK.bagian.kegiatan_nama
        kegiatan = data.SPK.subkegiatan.kegiatan_nama
        skpd = data.SPK.opd.namaopd
        no_spk = data.SPK.nospk
        key = (bagian, kegiatan, skpd)

        data_spk = {
            'nospk': no_spk,
            'uraispk': data.SPK.uraipekerjaan,
            'perusahaan': data.SPK.perusahaan.namaperusahaan,
            'total_tagihan': data.totalpembayaran,
        }

        if key not in grouped_data:
            grouped_data[key] = {
                'tanggal': data.tglfaktur,
                'bagian': bagian,
                'kegiatan': kegiatan,
                'skpd': skpd,
                'total_spk': 0,
                'children': [],
                'existing_nospk': set()
            }

        if no_spk not in grouped_data[key]['existing_nospk']:
            grouped_data[key]['children'].append(data_spk)
            grouped_data[key]['total_spk'] += data.totalpembayaran
            grouped_data[key]['existing_nospk'].add(no_spk)

    for item in grouped_data.values():
        del item['existing_nospk']

    list_data_faktur = list(grouped_data.values())
    total_keseluruhan = sum(item['total_spk'] for item in grouped_data.values())

    # Membuat data untuk Excel
    data_excel = []
    row_index = 1  # Untuk nomor urut

    for data_faktur in list_data_faktur:
        # Baris utama (induk)
        data_excel.append([
            row_index,
            data_faktur['tanggal'],
            data_faktur['skpd'],
            data_faktur['bagian'],
            data_faktur['kegiatan'],
            '',  # No SPK kosong
            '',  # Urai SPK kosong
            '',  # Pihak Ketiga kosong
            data_faktur['total_spk']
        ])
        row_index += 1

        # Baris anak (detail SPK)
        for dt_children in data_faktur['children']:
            data_excel.append([
                '',  # Nomor urut kosong
                '',  # Tanggal kosong
                '',  # SKPD kosong
                '',  # Bagian kosong
                '',  # Kegiatan kosong
                dt_children['nospk'],
                dt_children['uraispk'],
                dt_children['perusahaan'],
                dt_children['total_tagihan']
            ])

    # Baris total
    data_excel.append([
        '',  # Nomor urut kosong
        '',  # Tanggal kosong
        '',  # SKPD kosong
        '',  # Bagian kosong
        '',  # Kegiatan kosong
        '',  # No SPK kosong
        '',  # Urai SPK kosong
        'Total Tagihan',
        total_keseluruhan
    ])

    # Membuat DataFrame
    df = pd.DataFrame(data_excel, columns=[
        'No', 'Tanggal', 'SKPD', 'Bagian', 'Kegiatan', 'No SPK', 'Urai SPK', 'Pihak Ketiga', 'Nilai Tagihan'
    ])

    # Membuat file Excel
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="laporan_tagihan.xlsx"'

    with pd.ExcelWriter(response, engine='openpyxl') as writer:
        df.to_excel(writer, index=False, sheet_name='Laporan Tagihan')

    return response




    