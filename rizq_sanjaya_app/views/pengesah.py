from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from rizq_sanjaya_app.models.profile import JABATAN_CHOICE

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    pejabatpengesah_list    = pejabatpengesah.objects.filter(status='Aktif')
    SKPD_list               = OPD.objects.filter(status='Aktif')
    archives                = pejabatpengesah.objects.filter(status='Tidak aktif')
    context = {
        'title' : 'Master Pejabatpengesah',
        'pejabatpengesah_list' : pejabatpengesah_list,
        'SKPD_list' :SKPD_list,
        'archives' : archives,
        'jabatan_choice': JABATAN_CHOICE,
    }
    
    return render(request, 'profile/admin/pejabatpengesah/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = pejabatpengesah.objects.all()
        data_pejabatpengesah = pejabatpengesah.objects.all()
        context = {
            'title' : 'Data Pejabat Pengesah',
            'form' : form,
            'data_pejabatpengesah':data_pejabatpengesah,
        }

        template = 'profile/admin/pejabatpengesah/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        nip = request.POST.get('nip')
        namapegawai = request.POST.get('namapegawai')
        jabatan = request.POST.get('jabatan')
        jabatankontrak = request.POST.get('jabatankontrak')
        opd = request.POST.get('opd')

        if nip is not None:
            insert_pejabat_pengesah = pejabatpengesah()
            insert_pejabat_pengesah.nip = nip
            insert_pejabat_pengesah.namapegawai = namapegawai
            insert_pejabat_pengesah.jabatan = jabatan
            insert_pejabat_pengesah.jabatankontrak = jabatankontrak
            insert_pejabat_pengesah.opd_id = opd
            
            insert_pejabat_pengesah.save()
            messages.success(request, 'Data pejabat pengesah berhasil disimpan.')
            return redirect('profile:admin_pejabat_pengesah')
        messages.error(request, 'Data pejabat pengesah gagal disimpan.')
        return render(request, 'profile/admin/pejabatpengesah/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_pejabatpengesah = pejabatpengesah.objects.get(id = id)

        context = {
            'title' : 'Ubah Data Pejabat Pengesah',
            'form' : form,
            'edit' : 'true',
            'data_skpd_' : dt_pejabatpengesah,
        }
        template = 'profile/admin/pejabatpengesah/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_pejabatpengesah = pejabatpengesah.objects.get(id = id)
        nip = request.POST.get('nip')
        namapegawai = request.POST.get('namapegawai')
        jabatan = request.POST.get('jabatan')
        jabatankontrak = request.POST.get('jabatankontrak')
        opd = request.POST.get('opd')
       

        if data_pejabatpengesah is not None:
            
            data_pejabatpengesah.nip = nip
            data_pejabatpengesah.namapegawai = namapegawai
            data_pejabatpengesah.jabatan = jabatan
            data_pejabatpengesah.jabatankontrak = jabatankontrak
            data_pejabatpengesah.opd_id = opd
            data_pejabatpengesah.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_pejabat_pengesah')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/pejabatpengesah/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = pejabatpengesah.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except pejabatpengesah.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = pejabatpengesah.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except pejabatpengesah.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)