from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.urls import reverse
from django.contrib import messages
from ..models import PenerimaBAST, MasterKegiatan as m_kegiatan
from django.utils import timezone
import os, re
from django.db import transaction
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db.models import Sum, F, Q, DecimalField, Value
from ..support_function import format_clear as sup 
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, ba_validation_exists, get_total_nilai_spk, bast_validation_exists, bapa_validation_exists, faktur_validation_exists, cek_nomor_terbaru
from django.db.models.functions import Coalesce
from rizq_sanjaya_app.support_function import PagingHelper

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    
    wilayah_list = lokasi.objects.filter(status='Aktif')
    skpd_list = OPD.objects.filter(status='Aktif')
    perusahaan_list = perusahaan.objects.filter(status='Aktif')
    statuspembayaran_list = masterpembayaran.objects.filter(status='Aktif')
    permohonan_list = SPP.objects.filter(status='Aktif')
    BAST_list = BAST.objects.filter(status='Aktif')
    archives = SPK.objects.filter(status='Tidak aktif')
    pengesah = pejabatpengesah.objects.filter(status='Aktif')

    spk_sudah_ada_di_spp = SPK.objects.filter(status='Aktif', id__in=SPP.objects.filter(status='Aktif').values_list('SPK_id', flat=True).order_by('created_at'))
    # spk_list = SPK.objects.filter(status='Aktif').annotate(total_tagihan = Sum(F('spkrincian__realisasi'))).order_by('-created_at')

    # Keperluan Datatable
    page_obj = PagingHelper.bast(request)
    # END Keperluan Datatable

    regex = re.compile(r'^(\d{3})/')

    bast_no = f"{1:03d}"

    all_bast = BAST.objects.all()
    if all_bast:
        nomor_urut_list = [
            int(regex.search(bast.nobap).group(1)) 
            for bast in all_bast if regex.search(bast.nobap)
        ]
        last_number =  max(nomor_urut_list) if nomor_urut_list else 0

        
        no_bast = last_number + 1
        bast_no = f"{no_bast:03d}"
    
   

    context = {
        'title' : 'Managemen Berkas',
        'page_obj' : page_obj,
        'wilayah_list' : wilayah_list,
        'skpd_list' : skpd_list,
        'perusahaan_list' : perusahaan_list,
        'statuspembayaran_list' : statuspembayaran_list,
        'permohonan_list' : permohonan_list,
        'bast_list' : BAST_list,
        'archives' : archives,
        'pengesah' : pengesah,

        
    }
    
    return render(request, 'profile/admin/managemenberkas/index.html', context)

@login_required
@is_verified()
@ba_validation_exists(redirect_to = 'profile:admin_managemen_berkas')
def create(request, id_spk):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id=id_spk)
        total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']

        
        regex = re.compile(r'^(\d{3})/')

        bast_no = f"{1:03d}"

        all_bast = BAST.objects.all()
        if all_bast:
            nomor_urut_list = [
                int(regex.search(bast.nobap).group(1)) 
                for bast in all_bast if regex.search(bast.nobap)
            ]
            last_number =  max(nomor_urut_list) if nomor_urut_list else 0

            
            no_bast = last_number + 1
            bast_no = f"{no_bast:03d}"
        
        context = {
            'title' : 'Ubah Data SPK',
            'dt_spk' : dt_spk,
            'total_tagihan' : total_tagihan,
            'bast_no' : bast_no,
        }
        return render(request, 'profile/admin/managemenberkas/modal_form.html', context)

    if request.method == 'POST':
        nobap = request.POST.get('nobap')
        nospk = request.POST.get('nospk')
        tglbap = request.POST.get('tglbap')
        # totalpembayaran = request.POST.get('totalpembayaran')
        # totalpembayaran = sup(totalpembayaran)
        # uraitgl = request.POST.get('uraitgl')
        ketua = request.POST.get('ketua')
        sekertaris = request.POST.get('sekertaris')
        anggota = request.POST.get('anggota')
        list_penerima = [
            {'nama': ketua, 'jabatan': 'Ketua PPHP'},
            {'nama': sekertaris, 'jabatan': 'Sekertaris PPHP'},
            {'nama': anggota, 'jabatan': 'Anggota PPHP'}
        ]

        totalpembayaran = get_total_nilai_spk(nospk)

        nobap = cek_nomor_terbaru(BAST, 'nobap', nomor_format = nobap)

        try:

            with transaction.atomic():
                insert_BAST = BAST()
                insert_BAST.nobap = nobap
                insert_BAST.SPK_id = nospk
                insert_BAST.tglbap = tglbap
                insert_BAST.totalpembayaran = totalpembayaran
                insert_BAST.uraitgl = ''
                insert_BAST.save()

                for dt in list_penerima:
                    insert_detail = PenerimaBAST()
                    insert_detail.bast_id = insert_BAST.id
                    insert_detail.name = dt['nama']
                    insert_detail.jabatan = dt['jabatan']
                    insert_detail.save()
                    

            messages.success(request, 'Data BAST berhasil disimpan.')
            return redirect('profile:admin_managemen_berkas')
                
        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_berkas')

            

            
        
@login_required
@is_verified()
def editBAST(request, id_spk):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id_spk)
        dt_bast = BAST.objects.get(SPK = dt_spk)
        
        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'dt_bast' : dt_bast,
    
        }
        template = 'profile/admin/managemenberkas/modal_form.html'
        return render(request, template, context)
    
    
    if request.method == 'POST':
        dt_spk = SPK.objects.get(id = id_spk)

        bap_instance = get_object_or_404(BAST, SPK_id=dt_spk.id)
        nobap = request.POST.get('nobap')
        nospk = request.POST.get('nospk')
        tglbap = request.POST.get('tglbap')
        # totalpembayaran = request.POST.get('totalpembayaran')
        # totalpembayaran = sup(totalpembayaran)
        # uraitgl = request.POST.get('uraitgl')
        ketua = request.POST.get('ketua')
        sekertaris = request.POST.get('sekertaris')
        anggota = request.POST.get('anggota')

        list_penerima = [
            {'nama': ketua, 'jabatan': 'Ketua PPHP'},
            {'nama': sekertaris, 'jabatan': 'Sekertaris PPHP'},
            {'nama': anggota, 'jabatan': 'Anggota PPHP'}
        ]

        totalpembayaran = get_total_nilai_spk(nospk)

        try:
            with transaction.atomic():
                bap_instance.nobap = nobap
                bap_instance.SPK_id = nospk
                bap_instance.tglbap = tglbap
                bap_instance.totalpembayaran = totalpembayaran
                bap_instance.uraitgl = ''
                bap_instance.save()

                # Hapus penerima yang sebelumnya (opsional, jika ingin reset data lama)
                PenerimaBAST.objects.filter(bast_id=bap_instance.id).delete()

                # Simpan data penerima baru
                for dt in list_penerima:
                    insert_detail = PenerimaBAST()  # Buat objek baru untuk setiap penerima
                    insert_detail.bast_id = bap_instance.id
                    insert_detail.name = dt['nama']
                    insert_detail.jabatan = dt['jabatan']
                    insert_detail.save()

            messages.success(request, 'Data BAST berhasil disimpan.')
            return redirect('profile:admin_managemen_berkas')

        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_berkas')

    


# CETAK DOKUMEN
@login_required
@is_verified()
def printBAST(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        pengesah = pejabatpengesah.objects.filter(status='Aktif')
        context = {
            'dt_spk':dt_spk,
            'pengesah':pengesah,
        }

        template = 'profile/admin/managemenberkas/modal_print.html'
        return render(request, template, context)


@login_required
@is_verified()
def cetakBAST(request, id):
    if request.method == 'GET':
        data_detail_bast = get_object_or_404(SPK, id=id)
        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        data_SPP = SPP.objects.get(SPK_id = id)
        data_bast = BAST.objects.prefetch_related('penerimabast_set').filter(SPK_id=id)
        get_tgl = request.GET.get('tanggal')
        id_pengesah = request.GET.get('pengesah')
        tgl_print = get_tgl
        dt_pengesah = get_object_or_404(pejabatpengesah, id=id_pengesah)

        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        id_pengesah = request.GET.get('pengesah')

        dt_pengesah = get_object_or_404(pejabatpengesah, id=id_pengesah)
        jumlah_harga = 0

        for x in data_rincian_spk:
            for subspk in x.spksubrincian_set.all():
                dt_harga = subspk.harga * subspk.qty
                jumlah_harga += dt_harga

        list_rekening = []

        jumlah_harga = 0
        nilai_pekerjaan = 0

        subkegiatan_id_list = []

        rincian_rekening = {}

        for x in data_detail_spk.spkrincian_set.all():
            subkegiatan = x.ssh.get_ancestors().get(is_subkegiatan = True)
            list_rekening.append(subkegiatan.kegiatan_nama)
            jumlah_harga += x.realisasi
            subkegiatan_id_list.append(subkegiatan.kegiatan_id)

            if x.ssh.parent.kegiatan_nama not in rincian_rekening.keys():
                rincian_rekening[x.ssh.parent.kegiatan_nama] = []

            rincian_rekening[x.ssh.parent.kegiatan_nama].append(
                        {'uraian_ssh': x.ssh.uraian_ssh,
                        'volume': x.realisasi / x.ssh.nilai,
                        'satuan': x.ssh.satuan,
                        'nilai': x.ssh.nilai,
                        'realisasi': x.realisasi
                        }
                    )

        unique_subkegiatan_id_list = list(set(subkegiatan_id_list))
        unique_list_rekening = list(set(list_rekening))

        for subkeg in unique_subkegiatan_id_list:
            sshnya = m_kegiatan.objects.get(kegiatan_id = subkeg).get_descendants().filter(is_ssh = True, parent__parent__kegiatan_nama = data_detail_spk.bagian.kegiatan_nama).aggregate(total_per_subkeg = Sum(F('volume') * F('nilai')))['total_per_subkeg']
            nilai_pekerjaan+=sshnya


        data_detail_spk.list_rekening = unique_list_rekening
        data_detail_spk.jumlah_harga = jumlah_harga
        data_detail_spk.nilai_pekerjaan = nilai_pekerjaan
        data_detail_spk.rincian_rekening = rincian_rekening
                

        counter = 1
        for data in data_bast:
            dt_pihak_list = data.penerimabast_set.all()
            for dt_pihak in dt_pihak_list:
                dt_pihak.counter = counter
                counter += 1

        context = {
            'title' : 'Berita Acara Serah Terima Barang/Jasa',
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'jumlah_harga' : jumlah_harga,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            'item_spp' : data_SPP,
            'data_bast' : data_bast,
            'tgl_print' : tgl_print,
            'dt_pengesah' : dt_pengesah,
            'counter': counter,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }

        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/print.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')




    
 


@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        

        context = {
            'title' : 'Ubah Data SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk_' : dt_spk,
    
        }
        template = 'profile/admin/trans_skp/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_spk = SPK.objects.get(id = id)
        nospk = request.POST.get('nospk')
        uraipekerjaan = request.POST.get('uraipekerjaan')
        perusahaan = request.POST.get('perusahaan')
        tglspk = request.POST.get('tglspk')
        sumberdana = request.POST.get('sumberdana')
        waktupelaksanaan = request.POST.get('waktupelaksanaan')
        opd = request.POST.get('opd')
        perusahaan = request.POST.get('perusahaan')

        if data_spk is not None:
            
            data_spk.nospk = nospk
            data_spk.tglspk = tglspk
            data_spk.uraipekerjaan = uraipekerjaan
            data_spk.sumberdana = sumberdana
            data_spk.waktupelaksanaan = waktupelaksanaan
            data_spk.opd_id = opd
            data_spk.perusahaan_id = perusahaan
         
            data_spk.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_spk')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/trans_spk/index.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = SPK.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except SPK.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        
        doc = SPK.objects.get(id=id)
        doc.deleted_at = None
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except SPK.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

# # # # # # # # # # #DETAIL # # # # # # # # # # # # # # # # 
@login_required
@is_verified()
def detailSPK(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_detail_spk = get_object_or_404(SPK, id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            
           
        }
        template = 'profile/admin/spk/detail_spk.html'
        return render(request, template, context)

# # # # # # # # # # # # # # # # # # BAP # # # # # # # # # # # # # # # # # #
@login_required
@is_verified()
@require_http_methods(["GET"])
def bap(request):
    wilayah_list = lokasi.objects.filter(status='Aktif')
    skpd_list = OPD.objects.filter(status='Aktif')
    perusahaan_list = perusahaan.objects.filter(status='Aktif')
    statuspembayaran_list = masterpembayaran.objects.filter(status='Aktif')
    permohonan_list = SPP.objects.filter(status='Aktif')
    BAST_list = BAST.objects.filter(status='Aktif')
    BAP_list       = BAP.objects.filter(status='Aktif')
    archives = SPK.objects.filter(status='Tidak aktif')
    data_spp = SPP.objects.all()
    pengesah = pejabatpengesah.objects.filter(status='Aktif')
    

    # Keperluan Datatable
    page_obj = PagingHelper.bap(request)
    # END Keperluan Datatable
    

    context = {
        'title' : 'Managemen Berkas',
        'page_obj' : page_obj,
        'wilayah_list' : wilayah_list,
        'skpd_list' : skpd_list,
        'perusahaan_list' : perusahaan_list,
        'statuspembayaran_list' : statuspembayaran_list,
        'permohonan_list' : permohonan_list,
        'bast_list' : BAST_list,
        'bap_list':BAP_list,
        'archives' : archives,
        'data_spp' : data_spp,
        'pengesah' : pengesah,
        
    }
    
    return render(request, 'profile/admin/managemenberkas/bap/index.html', context)

@login_required
@is_verified()
def create_BAP(request, id_spk):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id=id_spk)
        dt_bast = BAST.objects.get(SPK_id=id_spk)
        total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']

        dt_penerima = []

        for penerima in dt_bast.penerimabast_set.all():
            nama = penerima.name
            jabatan = penerima.jabatan
            dt_penerima.append({"nama": nama, "jabatan": jabatan})

        regex = re.compile(r'^(\d{3})/')

        bapa_no = f"{1:03d}"

        all_bapa = BAP.objects.all()
        if all_bapa:
            nomor_urut_list = [
                int(regex.search(bapa.nobap).group(1)) 
                for bapa in all_bapa if regex.search(bapa.nobap)
            ]
            last_number =  max(nomor_urut_list) if nomor_urut_list else 0

            
            no_bapa = last_number + 1
            bapa_no = f"{no_bapa:03d}"
        context = {
            'title' : 'Ubah Data SPK',
            'dt_spk' : dt_spk,
            'total_tagihan' : total_tagihan,
            'bapa_no' : bapa_no,
            'dt_penerima' : dt_penerima,
        }
        return render(request, 'profile/admin/managemenberkas/bap/modal_form.html', context)
    
    if request.method == 'POST':
        nobap = request.POST.get('nobap')
        nospk = request.POST.get('nospk')
        tglbap = request.POST.get('tglbap')
        uraitgl = request.POST.get('uraitgl')
        ketua = request.POST.get('ketua')
        sekertaris = request.POST.get('sekertaris')
        anggota = request.POST.get('anggota')
        list_penerima = [
            {'nama': ketua, 'jabatan': 'Ketua PPHP'},
            {'nama': sekertaris, 'jabatan': 'Sekertaris PPHP'},
            {'nama': anggota, 'jabatan': 'Anggota PPHP'}
        ]

        nobap = cek_nomor_terbaru(BAP, 'nobap', nomor_format = nobap)

        try:

            with transaction.atomic():
                insert_BAP = BAP()
                insert_BAP.nobap = nobap
                insert_BAP.SPK_id = nospk
                insert_BAP.tglbap = tglbap
                insert_BAP.uraitgl = uraitgl
                insert_BAP.save()

                for dt in list_penerima:
                    insert_detail = PenerimaBAP()
                    insert_detail.bap_id = insert_BAP.id
                    insert_detail.name = dt['nama']
                    insert_detail.jabatan = dt['jabatan']
                    insert_detail.save()
                    

            messages.success(request, 'Data BAPA berhasil disimpan.')
            return redirect('profile:admin_managemen_bap')
                
        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_bap')

@login_required
@is_verified()
def edit_BAP(request, id_spk):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id=id_spk)
        dt_bap = BAP.objects.get(SPK = dt_spk)
        
        
        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'dt_bap' : dt_bap,
    
        }
        template = 'profile/admin/managemenberkas/bap/modal_form.html'
        return render(request, template, context)

    
    if request.method == 'POST':
        dt_spk = SPK.objects.get(id=id_spk)
        bap_instance = get_object_or_404(BAP, SPK_id=dt_spk.id)
        nobap = request.POST.get('nobap')
        nospk = request.POST.get('nospk')
        tglbap = request.POST.get('tglbap')
        uraitgl = request.POST.get('uraitgl')
        ketua = request.POST.get('ketua')
        sekertaris = request.POST.get('sekertaris')
        anggota = request.POST.get('anggota')

        list_penerima = [
            {'nama': ketua, 'jabatan': 'Ketua PPHP'},
            {'nama': sekertaris, 'jabatan': 'Sekertaris PPHP'},
            {'nama': anggota, 'jabatan': 'Anggota PPHP'}
        ]

        try:
            with transaction.atomic():
                bap_instance.nobap = nobap
                bap_instance.tglbap = tglbap
                bap_instance.uraitgl = uraitgl
                bap_instance.save()

                print(list_penerima)

                # Hapus penerima yang sebelumnya (opsional, jika ingin reset data lama)
                PenerimaBAP.objects.filter(bap_id=bap_instance.id).delete()

                # Simpan data penerima baru
                for dt in list_penerima:
                    insert_detail = PenerimaBAP()  # Buat objek baru untuk setiap penerima
                    insert_detail.bap_id = bap_instance.id
                    insert_detail.name = dt['nama']
                    insert_detail.jabatan = dt['jabatan']
                    insert_detail.save()

            messages.success(request, 'Data BAPA berhasil disimpan.')
            return redirect('profile:admin_managemen_bap')

        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_bap')

        
    




# CETAK DOKUMEN
@login_required
@is_verified()
def printBAP(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        pengesah = pejabatpengesah.objects.filter(status='Aktif')
        context = {
            'dt_spk':dt_spk,
            'pengesah':pengesah,
        }

        template = 'profile/admin/managemenberkas/bap/modal_print.html'
        return render(request, template, context)

    
@login_required
@is_verified()
def cetakBAP(request, id):
    if request.method == 'GET':
        data_detail_bap = get_object_or_404(SPK, id=id)
        data_bap = BAP.objects.prefetch_related('penerimabap_set').filter(SPK_id=id)
        get_tgl = request.GET.get('tglprint')
        tgl_print = get_tgl
        id_pengesah = request.GET.get('pengesah')
        dt_pengesah = get_object_or_404(pejabatpengesah, id=id_pengesah)
        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)

        for x in data_rincian_spk:
            for subspk in x.spksubrincian_set.all():
                dt_harga = subspk.harga * subspk.qty
                jumlah_harga += dt_harga

        list_rekening = []

        jumlah_harga = 0
        nilai_pekerjaan = 0

        subkegiatan_id_list = []

        rincian_rekening = {}

        for x in data_detail_spk.spkrincian_set.all():
            subkegiatan = x.ssh.get_ancestors().get(is_subkegiatan = True)
            list_rekening.append(subkegiatan.kegiatan_nama)
            jumlah_harga += x.realisasi
            subkegiatan_id_list.append(subkegiatan.kegiatan_id)

            if x.ssh.parent.kegiatan_nama not in rincian_rekening.keys():
                rincian_rekening[x.ssh.parent.kegiatan_nama] = []

            rincian_rekening[x.ssh.parent.kegiatan_nama].append(
                        {'uraian_ssh': x.ssh.uraian_ssh,
                        'volume': x.realisasi / x.ssh.nilai,
                        'satuan': x.ssh.satuan,
                        'nilai': x.ssh.nilai,
                        'realisasi': x.realisasi
                        }
                    )

        unique_subkegiatan_id_list = list(set(subkegiatan_id_list))
        unique_list_rekening = list(set(list_rekening))

        for subkeg in unique_subkegiatan_id_list:
            sshnya = m_kegiatan.objects.get(kegiatan_id = subkeg).get_descendants().filter(is_ssh = True, parent__parent__kegiatan_nama = data_detail_spk.bagian.kegiatan_nama).aggregate(total_per_subkeg = Sum(F('volume') * F('nilai')))['total_per_subkeg']
            nilai_pekerjaan+=sshnya


        data_detail_spk.list_rekening = unique_list_rekening
        data_detail_spk.jumlah_harga = jumlah_harga
        data_detail_spk.nilai_pekerjaan = nilai_pekerjaan
        data_detail_spk.rincian_rekening = rincian_rekening
                
        
        context = {
            'title' : 'Berita Acara Pemeriksaan Administrsi (BAP)',
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'jumlah_harga' : jumlah_harga,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            'data_bap' : data_bap,
            'tgl_print' : tgl_print,
            'dt_pengesah' : dt_pengesah,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/bap/print.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')
    
# # # # # # # # # # # # # # # # # # FAKTUR TAGIHAN # # # # # # # # # # # # # # # # # #
@login_required
@is_verified()
@require_http_methods(["GET"])
def faktur(request):
    wilayah_list = lokasi.objects.filter(status='Aktif')
    skpd_list = OPD.objects.filter(status='Aktif')
    perusahaan_list = perusahaan.objects.filter(status='Aktif')
    statuspembayaran_list = masterpembayaran.objects.filter(status='Aktif')
    permohonan_list = SPP.objects.filter(status='Aktif')
    BAST_list = BAST.objects.filter(status='Aktif')
    FAKTURTAGIHAN_list = FakturPajak.objects.filter(status='Aktif')
    archives = SPK.objects.filter(status='Tidak aktif')
    fakturpajakrincian_list = FakturPajakRincian.objects.filter(status='Aktif')
    
    # Keperluan Datatable
    page_obj = PagingHelper.fakturpajak(request)
    # END Keperluan Datatable

    context = {
        'title' : 'Managemen Berkas',
        'page_obj' : page_obj,
        'wilayah_list' : wilayah_list,
        'skpd_list' : skpd_list,
        'perusahaan_list' : perusahaan_list,
        'statuspembayaran_list' : statuspembayaran_list,
        'permohonan_list' : permohonan_list,
        'bast_list' : BAST_list,
        'FAKTURTAGIHAN_list' : FAKTURTAGIHAN_list,
        'archives' : archives,
        'fakturpajakrincian_list':fakturpajakrincian_list,
        
    }
    
    return render(request, 'profile/admin/managemenberkas/faktur/index.html', context)

@login_required
@is_verified()
def create_NOFAKTUR(request, id_spk):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id=id_spk)
        regex = re.compile(r'^(\d{3})/')

        faktur_no = f"{1:03d}"

        all_faktur = FakturPajak.objects.all()
        if all_faktur:
            nomor_urut_list = [
                int(regex.search(faktur.nofaktur).group(1)) 
                for faktur in all_faktur if regex.search(faktur.nofaktur)
            ]
            last_number =  max(nomor_urut_list) if nomor_urut_list else 0

            
            no_faktur = last_number + 1
            faktur_no = f"{no_faktur:03d}"

        context = {
            'title' : 'Ubah Data SPK',
            'dt_spk' : dt_spk,
            'faktur_no' : faktur_no,
        }
        return render(request, 'profile/admin/managemenberkas/faktur/modal_form.html', context)
    
    if request.method == 'POST':
        nofaktur = request.POST.get('nofaktur')
        nospk = request.POST.get('nospk')
        tglfaktur = request.POST.get('tglfaktur')
        uraitgl = request.POST.get('uraitgl')


        print(nofaktur)
        nofaktur = cek_nomor_terbaru(FakturPajak, 'nofaktur', nomor_format = nofaktur)
        try:
            with transaction.atomic():
                if nofaktur is not None:
                    insert_FAKTUR_PAKAK = FakturPajak()
                    insert_FAKTUR_PAKAK.nofaktur = nofaktur
                    insert_FAKTUR_PAKAK.SPK_id = nospk
                    insert_FAKTUR_PAKAK.tglfaktur = tglfaktur
                    insert_FAKTUR_PAKAK.uraitgl = uraitgl
                    insert_FAKTUR_PAKAK.save()

                    messages.success(request, 'Data Faktur Tagihan berhasil disimpan.')
                    return redirect(reverse('profile:admin_faktur_detail', args=[nospk]))

        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_faktur')


@login_required
@is_verified()
def edit_NOFAKTUR(request, id_faktur):
    if request.method == 'GET':
        dt_faktur = FakturPajak.objects.get(id = id_faktur)
        
        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'dt_faktur' : dt_faktur,
    
        }
        template = 'profile/admin/managemenberkas/faktur/modal_form.html'
        return render(request, template, context)

    
    if request.method == 'POST':
        faktur_instance = get_object_or_404(FakturPajak, id=id_faktur)
        nofaktur = request.POST.get('nofaktur')
        nospk = request.POST.get('nospk')
        tglfaktur = request.POST.get('tglfaktur')
        uraitgl = request.POST.get('uraitgl')

        try:
            with transaction.atomic():
                if nofaktur is not None:
                    faktur_instance.nofaktur = nofaktur
                    faktur_instance.SPK_id = nospk
                    faktur_instance.tglfaktur = tglfaktur
                    faktur_instance.uraitgl = uraitgl
                    faktur_instance.save()

                    messages.success(request, 'Data Faktur Tagihan berhasil disimpan.')
                    return redirect(reverse('profile:admin_faktur_detail', args=[nospk]))

        except Exception as e:
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:admin_managemen_faktur')

        

# # # # # # # # # # #CREATE RINCIAN FAKTUR TAGIHAN # # # # # # # # # # # # # # # # 
@login_required
@is_verified()
def create_RINCIANFAKTUR(request, id_faktur):

    if request.method == 'GET':
        dt_faktur = FakturPajak.objects.get(id = id_faktur)
        pajaklist = masterpajak.objects.filter(status='Aktif')
        bast_list = BAST.objects.filter(SPK_id=dt_faktur.SPK.id)
        dt_spk = SPK.objects.filter(id=dt_faktur.SPK.id).annotate(total_tagihan = Sum(F('spkrincian__realisasi'))).order_by('-created_at')
        context = {
            'title' : 'Data Rincian Faktur Tagihan',
            'dt_faktur':dt_faktur,
            'pajak_list': pajaklist,
            'bast_list': bast_list,
            'dt_spk': dt_spk,
        }

        template = 'profile/admin/managemenberkas/faktur/form_pajak.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        nofaktur = request.POST.get('nofaktur')
        nospk = request.POST.get('nospk')
        tglfaktur = request.POST.get('tglfaktur')
        jenispajak = request.POST.get('jenispajak')
        nilaitagihan = request.POST.get('nilaitagihan')
        nilaipajak = request.POST.get('nilaipajak')
        totalpembayaran = sup(nilaitagihan)

        try:
            with transaction.atomic():
                insert_Rincian_Faktur_tagihan = FakturPajakRincian.objects.create(
                    nofaktur=nofaktur,
                    SPK_id=nospk,
                    tglfaktur=tglfaktur,
                    totalpembayaran=totalpembayaran,
                    pajak_id=jenispajak,
                    nilaipajak=nilaipajak
                )

            messages.success(request, 'Data Rincian Faktur Tagihan berhasil disimpan.')
            return redirect(reverse('profile:admin_faktur_detail', args=[insert_Rincian_Faktur_tagihan.SPK_id]))

        except Exception as e:
            messages.error(request, f'Data gagal disimpan: {str(e)}')
            return redirect(reverse('profile:admin_faktur_detail', args=[nospk]))

@login_required
@is_verified()
def edit_RINCIANFAKTUR(request, id):
    if request.method == 'GET':
        dt_faktur = FakturPajakRincian.objects.get(id = id)
        pajaklist = masterpajak.objects.filter(status='Aktif')
        bast_list = BAST.objects.filter(SPK_id=dt_faktur.SPK.id)
        dt_spk = SPK.objects.filter(id=dt_faktur.SPK.id).annotate(total_tagihan = Sum(F('spkrincian__realisasi'))).order_by('-created_at')
        
        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'pajak_list': pajaklist,
            'dt_faktur' : dt_faktur,
            'bast_list' : bast_list,
            'dt_spk' : dt_spk,
    
        }
        template = 'profile/admin/managemenberkas/faktur/form_pajak.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        nofaktur = request.POST.get('nofaktur')
        nospk = request.POST.get('nospk')
        tglfaktur = request.POST.get('tglfaktur')
        jenispajak = request.POST.get('jenispajak')
        nilaitagihan = request.POST.get('nilaitagihan')
        nilaipajak = request.POST.get('nilaipajak')
        totalpembayaran = sup(nilaitagihan)

        try:
            with transaction.atomic():
                insert_Rincian_Faktur_tagihan = FakturPajakRincian.objects.get(id = id)
                insert_Rincian_Faktur_tagihan.nofaktur = nofaktur
                insert_Rincian_Faktur_tagihan.SPK_id = nospk
                insert_Rincian_Faktur_tagihan.tglfaktur = tglfaktur
                insert_Rincian_Faktur_tagihan.totalpembayaran = totalpembayaran
                insert_Rincian_Faktur_tagihan.pajak_id = jenispajak
                insert_Rincian_Faktur_tagihan.nilaipajak = nilaipajak
                insert_Rincian_Faktur_tagihan.save()

            messages.success(request, f'Data rincian faktur tagihan berhasil disimpan.')
            return redirect(reverse('profile:admin_faktur_detail', args=[insert_Rincian_Faktur_tagihan.SPK.id]))
                
        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal diubah.')
            return redirect(reverse('profile:admin_faktur_detail', args=[insert_Rincian_Faktur_tagihan.SPK.id]))
        

# # # # # # # # # # #DETAIL SPK # # # # # # # # # # # # # # # # 
@login_required
@is_verified()
def detailFAKTUR(request, id):
    if request.method == 'GET':
        bast_list = BAST.objects.filter(SPK_id=id)
        pajaklist = masterpajak.objects.filter(status='Aktif')
        dt_faktur = FakturPajak.objects.get(SPK_id = id)
        fakturpajak_list = FakturPajak.objects.filter(SPK_id = id)
        fakturpajakrincian_list = FakturPajakRincian.objects.filter(SPK_id = id)

        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)

        for x in data_rincian_spk:
            for subspk in x.spksubrincian_set.all():
                dt_harga = subspk.harga * subspk.qty
                jumlah_harga += dt_harga

        list_rekening = []

        jumlah_harga = 0
        nilai_pekerjaan = 0

        subkegiatan_id_list = []

        rincian_rekening = {}

        for x in data_detail_spk.spkrincian_set.all():
            subkegiatan = x.ssh.get_ancestors().get(is_subkegiatan = True)
            list_rekening.append(subkegiatan.kegiatan_nama)
            jumlah_harga += x.realisasi
            subkegiatan_id_list.append(subkegiatan.kegiatan_id)

            if x.ssh.parent.kegiatan_nama not in rincian_rekening.keys():
                rincian_rekening[x.ssh.parent.kegiatan_nama] = []

            rincian_rekening[x.ssh.parent.kegiatan_nama].append(
                        {'uraian_ssh': x.ssh.uraian_ssh,
                        'volume': x.realisasi / x.ssh.nilai,
                        'satuan': x.ssh.satuan,
                        'nilai': x.ssh.nilai,
                        'realisasi': x.realisasi
                        }
                    )

        unique_subkegiatan_id_list = list(set(subkegiatan_id_list))
        unique_list_rekening = list(set(list_rekening))

        for subkeg in unique_subkegiatan_id_list:
            sshnya = m_kegiatan.objects.get(kegiatan_id = subkeg).get_descendants().filter(is_ssh = True, parent__parent__kegiatan_nama = data_detail_spk.bagian.kegiatan_nama).aggregate(total_per_subkeg = Sum(F('volume') * F('nilai')))['total_per_subkeg']
            nilai_pekerjaan+=sshnya


        data_detail_spk.list_rekening = unique_list_rekening
        data_detail_spk.jumlah_harga = jumlah_harga
        data_detail_spk.nilai_pekerjaan = nilai_pekerjaan
        data_detail_spk.rincian_rekening = rincian_rekening

        context = {
            'title' : 'Detail SPK',
            'edit' : 'true',
            'bast_list' : bast_list,
            'dt_faktur' : dt_faktur,
            'jumlah_harga' : jumlah_harga,
            'pajak_list' : pajaklist,
            'fakturpajakrincian_list':fakturpajakrincian_list,
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            
        }
        template = 'profile/admin/managemenberkas/faktur/detail_faktur.html'
        return render(request, template, context)


@login_required
@is_verified()
def printFaktur(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        context = {
            'dt_spk':dt_spk,
        }

        template = 'profile/admin/managemenberkas/faktur/modal_print.html'
        return render(request, template, context)

# CETAK DOKUMEN
@login_required
@is_verified()
def cetakFaktur(request, id):
    if request.method == 'GET':
        
        bast_list = BAST.objects.filter(SPK_id=id)
        fakturpajak_list = FakturPajak.objects.filter(SPK_id = id)
        fakturpajakrincian_list = FakturPajakRincian.objects.filter(SPK_id = id)

        data_detail_bap = get_object_or_404(SPK, id=id)
        data_bap = BAP.objects.prefetch_related('penerimabap_set').filter(SPK_id=id)
        get_tgl = request.GET.get('tanggal')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')

        data_detail_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)

        for x in data_rincian_spk:
            for subspk in x.spksubrincian_set.all():
                dt_harga = subspk.harga * subspk.qty
                jumlah_harga += dt_harga

        list_rekening = []

        jumlah_harga = 0
        nilai_pekerjaan = 0

        subkegiatan_id_list = []

        rincian_rekening = {}

        for x in data_detail_spk.spkrincian_set.all():
            subkegiatan = x.ssh.get_ancestors().get(is_subkegiatan = True)
            list_rekening.append(subkegiatan.kegiatan_nama)
            jumlah_harga += x.realisasi
            subkegiatan_id_list.append(subkegiatan.kegiatan_id)

            if x.ssh.parent.kegiatan_nama not in rincian_rekening.keys():
                rincian_rekening[x.ssh.parent.kegiatan_nama] = []

            rincian_rekening[x.ssh.parent.kegiatan_nama].append(
                        {'uraian_ssh': x.ssh.uraian_ssh,
                        'volume': x.realisasi / x.ssh.nilai,
                        'satuan': x.ssh.satuan,
                        'nilai': x.ssh.nilai,
                        'realisasi': x.realisasi
                        }
                    )

        unique_subkegiatan_id_list = list(set(subkegiatan_id_list))
        unique_list_rekening = list(set(list_rekening))

        for subkeg in unique_subkegiatan_id_list:
            sshnya = m_kegiatan.objects.get(kegiatan_id = subkeg).get_descendants().filter(is_ssh = True, parent__parent__kegiatan_nama = data_detail_spk.bagian.kegiatan_nama).aggregate(total_per_subkeg = Sum(F('volume') * F('nilai')))['total_per_subkeg']
            nilai_pekerjaan+=sshnya


        data_detail_spk.list_rekening = unique_list_rekening
        data_detail_spk.jumlah_harga = jumlah_harga
        data_detail_spk.nilai_pekerjaan = nilai_pekerjaan
        data_detail_spk.rincian_rekening = rincian_rekening

        context = {
            'title' : 'Faktur Tagihan',
            'data_detail_spk' : data_detail_spk,
            'bast_list':bast_list,
            'fakturpajak_list' : fakturpajak_list,
            'fakturpajakrincian_list' : fakturpajakrincian_list,
            'tgl_print' : tgl_print,
            'data_spk' : data_detail_spk,
            'jumlah_harga' : jumlah_harga,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            'data_bap' : data_bap,
            'tgl_print' : tgl_print,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }
                
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/faktur/print_faktur.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')


@login_required
@is_verified()
def ModalFakturPajak(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        context = {
            'dt_spk':dt_spk,
        }

        template = 'profile/admin/managemenberkas/faktur/modal_print_faktur_pajak.html'
        return render(request, template, context)

# CETAK DOKUMEN
@login_required
@is_verified()
def cetakFakturPajak(request, id):
    if request.method == 'GET':
        fakturpajakrincian_list = FakturPajakRincian.objects.filter(SPK_id = id)
        dt_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        get_tgl = request.GET.get('tanggal')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')

        total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']
        nilai_dpp = round(int(total_tagihan) * (100/111))
        

        context = {
            'title' : 'Faktur Pajak',
            'dt_spk' : dt_spk,
            'tgl_print' : tgl_print,
            'nilai_dpp' : nilai_dpp,
            'total_tagihan' : total_tagihan,
            'fakturpajakrincian_list' : fakturpajakrincian_list,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }
                
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/faktur/faktur_pajak.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')
        
@login_required
@is_verified()
def ModalSSP(request, id):
    if request.method == 'GET':
        fakturpajakrincian_list = FakturPajakRincian.objects.get(id = id)
        dt_spk = SPK.objects.get(id = fakturpajakrincian_list.SPK.id)
        context = {
            'dt_spk':dt_spk,
            'fakturpajakrincian_list':fakturpajakrincian_list,
        }

        template = 'profile/admin/managemenberkas/faktur/modal_print_ssp.html'
        return render(request, template, context)

# CETAK DOKUMEN
@login_required
@is_verified()
def cetakSSP(request, id):
    if request.method == 'GET':
        fakturpajakrincian_list = FakturPajakRincian.objects.get(id = id)
        dt_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=fakturpajakrincian_list.SPK.id)
        get_tgl = request.GET.get('tanggal')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')

        total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']
        nilai_dpp = round(int(total_tagihan) * (100/111))
        

        context = {
            'title' : 'Surat Setoran Pajak',
            'dt_spk' : dt_spk,
            'tgl_print' : tgl_print,
            'nilai_dpp' : nilai_dpp,
            'total_tagihan' : total_tagihan,
            'fakturpajakrincian_list' : fakturpajakrincian_list,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }
                
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/faktur/surat_ssp.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')


@login_required
@is_verified()
def ModalBAP(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        pengesah = pejabatpengesah.objects.filter(status='Aktif')
        context = {
            'dt_spk':dt_spk,
            'pengesah':pengesah,
        }

        template = 'profile/admin/managemenberkas/faktur/modal_print_bap.html'
        return render(request, template, context)

# CETAK DOKUMEN
@login_required
@is_verified()
def cetakBAPFaktur(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.prefetch_related('spkrincian_set').get(id=id)
        get_tgl = request.GET.get('tanggal')
        id_pptk = request.GET.get('pptk')
        id_pengguna = request.GET.get('pengguna')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')

        total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']

        nilai_dpp = round(int(total_tagihan) * (100/111))

        dt_faktur = FakturPajak.objects.get(SPK_id=id)
        nilai_pajak = nilai_dpp * (11/100)
        print(nilai_pajak)

        faktur_rincian = int(FakturPajakRincian.objects.filter(SPK_id=id).aggregate(total_pajak=Sum('nilaipajak'))['total_pajak'] or 0)

        print(faktur_rincian, nilai_dpp)

        pptk = pejabatpengesah.objects.get(id=id_pptk)
        pengguna = pejabatpengesah.objects.get(id=id_pengguna)

        dt_bast = BAST.objects.get(SPK_id=id)
        print(dt_bast)


        context = {
            'title' : 'Berita Acara Pembayaran',
            'dt_spk' : dt_spk,
            'dt_faktur' : dt_faktur,
            'pptk' : pptk,
            'nilai_dpp' : nilai_dpp,
            'nilai_pajak' : nilai_pajak,
            'dt_bast' : dt_bast,
            'pengguna' : pengguna,
            'tgl_print' : tgl_print,
            'total_tagihan' : total_tagihan,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
            
           
        }
                
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2cm', 'right':'2.5cm', 'bottom':'2cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/managemenberkas/faktur/print_bap.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')
    
# # # # # # # # # # # # # # # # # # FAKTUR TAGIHAN # # # # # # # # # # # # # # # # # #
def DeleteDetail(request, id):
    message = ''
    try:
        doc = FakturPajakRincian.objects.get(id=id)
        print('sknseknfjks')
        doc.delete()
        message = 'success'
    except FakturPajakRincian.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)