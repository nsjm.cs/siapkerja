from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    
    perusahaan_list = perusahaan.objects.filter(status='Aktif')
    archives = perusahaan.objects.filter(status='Tidak aktif')
    context = {
        'title' : 'Master Perusahaan',
        'perusahaan_list' : perusahaan_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/perusahaan/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = perusahaan.objects.all()
        data_perusahaan = perusahaan.objects.all()
        context = {
            'title' : 'Data Perusahaan',
            'form' : form,
            'data_perusahaan':data_perusahaan,
        }

        template = 'profile/admin/perusahaan/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        namaperusahaan = request.POST.get('namaperusahaan')
        npwp = request.POST.get('npwp')
        bank = request.POST.get('bank')
        norek = request.POST.get('norek')
        namadirektur = request.POST.get('namadirektur')
        alamatperusahaan = request.POST.get('alamatperusahaan')
        emailperusahaan = request.POST.get('emailperusahaan')
        kontakperusahaan = request.POST.get('kontakperusahaan')
        logo = request.FILES['logo']
        
        
        if namaperusahaan is not None:
            insert_perusahaan = perusahaan()
            insert_perusahaan.namaperusahaan = namaperusahaan
            insert_perusahaan.npwp = npwp
            insert_perusahaan.bank = bank
            insert_perusahaan.norek = norek
            insert_perusahaan.namadirektur = namadirektur
            insert_perusahaan.alamatperusahaan = alamatperusahaan
            insert_perusahaan.emailperusahaan = emailperusahaan
            insert_perusahaan.kontakperusahaan = kontakperusahaan
            insert_perusahaan.logo = logo
            insert_perusahaan.save()
            messages.success(request, 'Data perusahaan berhasil disimpan.')
            return redirect('profile:admin_perusahaan')
        messages.error(request, 'Data perusahaan gagal disimpan.')
        return render(request, 'profile/admin/perusahaan/index.html', {'form': form,})


@login_required
@is_verified()
def edit(request, id):
    edit_data_perusahaan = get_object_or_404(perusahaan, id=id) 
    form = ''
    
   
    if request.method == 'POST':
        path_file_lama = f"{settings.MEDIA_ROOT}/{perusahaan.logo}"
        logo_old = bool(perusahaan.logo)
        image = request.FILES.get('logo')

        edit_data_perusahaan = perusahaan.objects.get(id = id)
        namadirektur = request.POST.get('namadirektur')
        namaperusahaan = request.POST.get('namaperusahaan')
        npwp = request.POST.get('npwp')
        bank = request.POST.get('bank')
        norek = request.POST.get('norek')

        alamatperusahaan = request.POST.get('alamatperusahaan')
        emailperusahaan = request.POST.get('emailperusahaan')
        kontakperusahaan = request.POST.get('kontakperusahaan')
        

        if edit_data_perusahaan is not None:
            
            edit_data_perusahaan.namaperusahaan = namaperusahaan
            edit_data_perusahaan.npwp = npwp
            edit_data_perusahaan.bank = bank
            edit_data_perusahaan.norek = norek
            edit_data_perusahaan.namadirektur = namadirektur
            edit_data_perusahaan.alamatperusahaan = alamatperusahaan
            edit_data_perusahaan.emailperusahaan = emailperusahaan
            edit_data_perusahaan.kontakperusahaan = kontakperusahaan
            
           
            edit_data_perusahaan.save()
            if len(request.FILES) > 0:
                if logo_old : 
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
                form_logo = perusahaan.objects.get(id = id)
                form_logo.logo = image
                form_logo.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_perusahaan')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/perusahaan/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = perusahaan.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except perusahaan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = perusahaan.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except perusahaan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)