from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from ..models import FakturPajak, FakturPajakRincian, MasterKegiatan as m_kegiatan, MasterTahapan
from django.utils import timezone
import os
from django.urls import reverse
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db import transaction
import re, pprint, json, math
from rizq_sanjaya_app.context_processors import global_variables
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, spk_validation, cek_ba_spk, get_sisa_pagu_laporan, get_total_realisasi
from decimal import Decimal
from django.db.models import Sum, F, Value, DecimalField, Q, F
from django.db.models.functions import Coalesce
from datetime import datetime

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    data_subkegiatan = m_kegiatan.objects.all().get_descendants().filter(is_sumberdana = True)
    data_keterangan = []  
    data_rincian = []

    pagu_subkegiatan = 0
    total_keseluruhan_sisa_pagu = 0
    total_keseluruhan_subtotal_ssh = 0
    total_keseluruhan_pagu_per_keterangan = 0

    try: 
        with transaction.atomic():
            for x in data_subkegiatan:
                for rekening in x.get_ancestors().filter(is_rekening_belanja=True):
                    data_rekening = {
                        'kegiatan_kode': rekening.kegiatan_kode, 
                        'skpd': rekening.skpd.namaopd, 
                        'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                        'children': [], 
                        'total_sisa_pagu': 0  
                    }
                    
                    for bagian in rekening.get_descendants().filter(is_bagian=True):
                        for keterangan in bagian.get_descendants().filter(is_subrekening_belanja=True):
                            pagu_per_keterangan = 0
                            total_sisa_pagu_keterangan = 0  
                            subtotal_ssh_keterangan = 0
                            
                            data_keterangan_entry = {
                                'kegiatan_kode': keterangan.kegiatan_kode, 
                                'rekening_kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                                'rekening_kegiatan_kode': rekening.kegiatan_kode,
                                'kegiatan_nama': keterangan.kegiatan_nama, 
                                'skpd': rekening.skpd.namaopd, 
                                'pagu_per_keterangan': 0,
                                'total_sisa_pagu': 0,
                                'bagian_nama': bagian.kegiatan_nama,
                                'subtotal_ssh': 0
                            }
                            
                            for ssh in keterangan.get_descendants():
                                subtotal = ssh.nilai * ssh.volume
                                pagu_subkegiatan += subtotal
                                
                                tahapan = MasterTahapan.objects.get(status=True)
                                sisa_pagu = round(get_sisa_pagu_laporan(tahapan, ssh.kegiatan_id))  
                                total_realisasi = round(get_total_realisasi(tahapan, ssh.kegiatan_id))  
                                
                                data_keterangan_entry['pagu_per_keterangan'] += subtotal
                                total_sisa_pagu_keterangan += sisa_pagu  
                                subtotal_ssh_keterangan += total_realisasi 
                            
                            data_keterangan_entry['total_sisa_pagu'] = total_sisa_pagu_keterangan
                            data_keterangan_entry['subtotal_ssh'] = subtotal_ssh_keterangan
                            
                            data_rekening['children'].append(data_keterangan_entry)
                            data_rekening['total_sisa_pagu'] += total_sisa_pagu_keterangan  

                            total_keseluruhan_sisa_pagu += total_sisa_pagu_keterangan
                            total_keseluruhan_subtotal_ssh += subtotal_ssh_keterangan
                            total_keseluruhan_pagu_per_keterangan += data_keterangan_entry['pagu_per_keterangan']

                            data_keterangan.append(data_keterangan_entry)
                    
                    data_rincian.append(data_rekening) 

    except Exception as e:
        print('Error get detail Kegiatan', e)




    context = {
        'title' : 'Realisasi Kegiatan',
        'data_rincian' : data_rincian,
        'data_keterangan' : data_keterangan,
        'total_keseluruhan_sisa_pagu' : total_keseluruhan_sisa_pagu,
        'total_keseluruhan_subtotal_ssh' : total_keseluruhan_subtotal_ssh,
        'total_keseluruhan_pagu_per_keterangan' : total_keseluruhan_pagu_per_keterangan,
    }
    
    return render(request, 'profile/admin/laporan/realisasi.html', context)

@login_required
@is_verified()
def CetakLaporanRealisasi(request):
    data_subkegiatan = m_kegiatan.objects.all().get_descendants().filter(is_sumberdana = True)
    data_keterangan = []  
    data_rincian = []

    pagu_subkegiatan = 0
    total_keseluruhan_sisa_pagu = 0
    total_keseluruhan_subtotal_ssh = 0
    total_keseluruhan_pagu_per_keterangan = 0

    try: 
        with transaction.atomic():
            for x in data_subkegiatan:
                for rekening in x.get_ancestors().filter(is_rekening_belanja=True):
                    data_rekening = {
                        'kegiatan_kode': rekening.kegiatan_kode, 
                        'skpd': rekening.skpd.namaopd, 
                        'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                        'children': [], 
                        'total_sisa_pagu': 0  
                    }
                    
                    for bagian in rekening.get_descendants().filter(is_bagian=True):
                        for keterangan in bagian.get_descendants().filter(is_subrekening_belanja=True):
                            pagu_per_keterangan = 0
                            total_sisa_pagu_keterangan = 0  
                            subtotal_ssh_keterangan = 0
                            
                            data_keterangan_entry = {
                                'kegiatan_kode': keterangan.kegiatan_kode, 
                                'rekening_kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                                'rekening_kegiatan_kode': rekening.kegiatan_kode,
                                'kegiatan_nama': keterangan.kegiatan_nama, 
                                'skpd': rekening.skpd.namaopd, 
                                'pagu_per_keterangan': 0,
                                'total_sisa_pagu': 0,
                                'bagian_nama': bagian.kegiatan_nama,
                                'subtotal_ssh': 0
                            }
                            
                            for ssh in keterangan.get_descendants():
                                subtotal = ssh.nilai * ssh.volume
                                pagu_subkegiatan += subtotal
                                
                                tahapan = MasterTahapan.objects.get(status=True)
                                sisa_pagu = round(get_sisa_pagu_laporan(tahapan, ssh.kegiatan_id))  
                                total_realisasi = round(get_total_realisasi(tahapan, ssh.kegiatan_id))  
                                
                                data_keterangan_entry['pagu_per_keterangan'] += subtotal
                                total_sisa_pagu_keterangan += sisa_pagu  
                                subtotal_ssh_keterangan += total_realisasi 
                            
                            data_keterangan_entry['total_sisa_pagu'] = total_sisa_pagu_keterangan
                            data_keterangan_entry['subtotal_ssh'] = subtotal_ssh_keterangan
                            
                            data_rekening['children'].append(data_keterangan_entry)
                            data_rekening['total_sisa_pagu'] += total_sisa_pagu_keterangan  

                            total_keseluruhan_sisa_pagu += total_sisa_pagu_keterangan
                            total_keseluruhan_subtotal_ssh += subtotal_ssh_keterangan
                            total_keseluruhan_pagu_per_keterangan += data_keterangan_entry['pagu_per_keterangan']

                            # Simpan data keterangan ke dalam list
                            data_keterangan.append(data_keterangan_entry)
                    
                    data_rincian.append(data_rekening)  # Simpan data rekening

    except Exception as e:
        print('Error get detail Kegiatan', e)



    context = {
        'title' : 'Realisasi Kegiatan',
        'data_rincian' : data_rincian,
        'data_keterangan' : data_keterangan,
        'total_keseluruhan_sisa_pagu' : total_keseluruhan_sisa_pagu,
        'total_keseluruhan_subtotal_ssh' : total_keseluruhan_subtotal_ssh,
        'total_keseluruhan_pagu_per_keterangan' : total_keseluruhan_pagu_per_keterangan,
        'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    }
   
            
    pdf_format = {
        'format': 'A4',
        # 'width': "21cm",
        # "height": "21.97cm",
        'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
        'display_header_footer':True,
        'header_template':"<div></div>",
        'footer_template':"<div></div>",
        'landscape': True,
    }
    print(pdf_format)
    pdf_file = generate_report('profile/admin/laporan/print_realisasi.html', context, pdf_format)

    return HttpResponse(pdf_file, content_type='application/pdf')




    