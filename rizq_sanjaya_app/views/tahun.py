from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db import transaction

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    tahun_list = Tahun.objects.filter(deleted_at=None)
    archives = Tahun.objects.filter(deleted_at__isnull=False)
    paginator = Paginator(tahun_list, 15)
    try:
        tahun_list = paginator.page(page)
    except PageNotAnInteger:
        tahun_list = paginator.page(1)
    except EmptyPage:
        tahun_list = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master Tahun',
        'tahun_list' : tahun_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/tahun/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = Tahun.objects.all()
        data_tahun= Tahun.objects.all()
        context = {
            'title' : 'Data Tahun',
            'form' : form,
            'data_tahun':data_tahun,
        }

        template = 'profile/admin/tahun/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        status = request.POST.get('status')
        if tahun is not None:
            try:
                with transaction.atomic():
                    insert_tahun = Tahun()
                    insert_tahun.tahun = tahun
                    insert_tahun.status = status
                    insert_tahun.save()
                    messages.success(request, 'Data Tahun berhasil disimpan.')

            except Exception as e:
                messages.error(request, str(e))

            return redirect('profile:admin_tahun')
        return render(request, 'profile/admin/tahun/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_tahun = Tahun.objects.get(id = id)

        context = {
            'title' : 'Ubah Data Tahun',
            'form' : form,
            'edit' : 'true',
            'data_tahun' : data_tahun,
        }
        template = 'profile/admin/tahun/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_tahun = Tahun.objects.get(id = id)
        tahun = request.POST.get('tahun')
        status = request.POST.get('status')

        if data_tahun is not None:
            data_tahun.tahun = tahun
            data_tahun.status = status
            data_tahun.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_tahun')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/tahun/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = Tahun.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Tahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = Tahun.objects.get(id=id)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Tahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
