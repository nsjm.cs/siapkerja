from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    wilayah_list = lokasi.objects.filter(status='Aktif')
    tahun = Tahun.objects.filter(deleted_at=None)
    archives = lokasi.objects.filter(status='Tidak aktif')
    paginator = Paginator(wilayah_list, 15)
    try:
        wilayah_list = paginator.page(page)
    except PageNotAnInteger:
        wilayah_list = paginator.page(1)
    except EmptyPage:
        wilayah_list = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master Wilayah',
        'wilayah_list' : wilayah_list,
        'tahun' : tahun,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/wilayah/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = lokasi.objects.all()
        data_wilayah = lokasi.objects.all()
        context = {
            'title' : 'Data Wilayah',
            'form' : form,
            'data_wilayah':data_wilayah,
        }

        template = 'profile/admin/wilayah/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        provinsi = request.POST.get('provinsi')
        kabupaten = request.POST.get('kabupaten')
        
        if kabupaten is not None:
            insert_wilayah = lokasi()
            insert_wilayah.tahun_id = tahun
            insert_wilayah.provinsi = provinsi
            insert_wilayah.kabupaten = kabupaten
            insert_wilayah.save()
            messages.success(request, 'Data Wilayah berhasil disimpan.')
            return redirect('profile:admin_wilayah')
        messages.error(request, 'Data Wilayah gagal disimpan.')
        return render(request, 'profile/admin/wilayah/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = 'profile/admin/wilayah/index.html'
    context = {} 
    form = ''
    
    try:
        dt_wilayah = get_object_or_404(lokasi, id=id)
    except lokasi.DoesNotExist:
        # Handle the case where the record with the given id doesn't exist
        return HttpResponseNotFound("Record not found")

    if request.method == 'GET':
        context = {
            'title': 'Ubah Data Wilayah',
            'form': form,
            'edit': 'true',
            'data_wilayah': dt_wilayah,
        }
        return render(request, template, context)
    
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        provinsi = request.POST.get('provinsi')
        kabupaten = request.POST.get('kabupaten')

        # No need to check if data_wilayah is not None, get_object_or_404 already handles it

        dt_wilayah.tahun_id = tahun
        dt_wilayah.provinsi = provinsi
        dt_wilayah.kabupaten = kabupaten
        dt_wilayah.save()

        messages.success(request, 'Data berhasil Diubah')
        return redirect('profile:admin_wilayah')

    messages.error(request, 'Data gagal disimpan.')
    return render(request, template, {'form': form})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = lokasi.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except Lokasi.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = lokasi.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except lokasi.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)