from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from ..forms import *
from django.contrib import messages
from ..models import *
from django.utils import timezone
import os
import json
from ..decorators import *
from ..helpers import *
from datetime import datetime
from django.db import IntegrityError

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_running_text(request):
    if request.method == 'GET':
        running_text = RunningText.objects.all().order_by('-created_at')
        archives = RunningText.objects.filter(deleted_at__isnull=False).order_by('-created_at')
        form = RunningTextForm()
        context = {
            'title' : 'Running Text - Admin',
            'running_texts' : running_text,
            'archives' : archives,
            'breadcrumb' : 'Running Text',
            'form': form,
        }
        
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        form = RunningTextForm(data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Running Text berhasil disimpan.')
            return redirect('profile:admin_setting_running_text',)
        messages.error(request, 'Running Text gagal disimpan.')
        return redirect('profile:admin_setting_running_text',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_category(request):
    if request.method == 'GET':
        category = Category.objects.all().order_by('typexs','nama')
        form = CategoryForm()
        context = {
            'title' : 'Kategori - Admin',
            'categories' : category,
            'breadcrumb' : 'Kategori',
            'form': form,
        }
        
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        form = CategoryForm(data=request.POST)
        if form.is_valid():
            try:
                kat = form.save()
                x = Category.objects.get(id = kat.id)
                if 'foto' in request.FILES:
                    x.foto = request.FILES['foto']
                x.save()
                messages.success(request, 'Kategori berhasil disimpan.')
                return redirect('profile:admin_setting_category',)
            except Exception as x:
                messages.error(request, f'Kategori gagal disimpan karena {str(x)}.')
                return redirect('profile:admin_setting_category',)
            
        return redirect('profile:admin_setting_category',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_running_text_edit(request):
    if request.method == 'POST':
        running_text = RunningText.objects.get(id = request.POST.get('id'))
        form = RunningTextForm(data=request.POST, instance=running_text)
        if form.is_valid():
            form.save()
            messages.success(request, 'Running Text berhasil disimpan.')
            return redirect('profile:admin_setting_running_text',)
        messages.error(request, 'Running Text gagal disimpan.')
        return redirect('profile:admin_setting_running_text',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_category_edit(request):
    if request.method == 'POST':
        category = Category.objects.get(id = request.POST.get('id'))
        form = CategoryForm(data=request.POST, instance=category)
        foto_old = bool(category.foto)

        if foto_old : 
            path_file_lama = f"{settings.MEDIA_ROOT}/{category.foto}"

        if form.is_valid():
            kat = form.save()
            x = Category.objects.get(id = kat.id)

            if 'foto' in request.FILES:
                if foto_old : 
                    if len(request.FILES) > 0:
                        try:
                            os.remove(path_file_lama)
                        except:
                            pass
                x.foto = request.FILES['foto']
                x.save()
            messages.success(request, 'Kategori berhasil disimpan.')
            return redirect('profile:admin_setting_category',)
        messages.error(request, 'Kategori gagal disimpan.')
        return redirect('profile:admin_setting_category',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_link(request):
    if request.method == 'GET':
        links = Link.objects.all().order_by('nama')
        archives = Link.objects.filter(deleted_at__isnull=False).order_by('nama')
        form = LinkForm()
        context = {
            'title' : 'Link - Admin',
            'links' : links,
            'archives' : archives,
            'breadcrumb' : 'Link',
            'form': form,
        }
        
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        form =LinkForm(data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Link berhasil disimpan.')
            return redirect('profile:admin_setting_link',)
        messages.error(request, 'Link gagal disimpan.')
        return redirect('profile:admin_setting_link',)

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_link_edit(request):
    if request.method == 'POST':
        link = Link.objects.get(id = request.POST.get('id'))
        form = LinkForm(data=request.POST, instance=link)
        if form.is_valid():
            form.save()
            messages.success(request, 'Link berhasil disimpan.')
            return redirect('profile:admin_setting_link',)
        messages.error(request, 'Link gagal disimpan.')
        return redirect('profile:admin_setting_link',)
    
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_carabayar(request):
    form = ''

    if request.method == 'GET':
        try: 
            highlight = AppSetting.objects.get(nama = 'highlight')
        except: 
            highlight = ''

        try: 
            deskripsi = AppSetting.objects.get(nama = 'deskripsi')
            # tanggal_lahir = tanggal.tanggal.strftime('%Y-%m-%d')
        except: 
            deskripsi = ''

        try: 
            embed_maps_bayar = AppSetting.objects.get(nama = 'embed_maps_bayar')
        except: 
            embed_maps_bayar = ''

        try: 
            image_cara_bayar = AppSetting.objects.get(nama = 'image_cara_bayar')
        except: 
            image_cara_bayar = ''

        try: 
            bank = AppSetting.objects.get(nama = 'bank')
        except: 
            bank = ''

        try: 
            norek = AppSetting.objects.get(nama = 'norek')
        except: 
            norek = ''

        try: 
            an = AppSetting.objects.get(nama = 'an')
        except: 
            an = ''

        faq = FAQ.objects.all()

        # form = InfografisForm()
        context = {
            'title' : 'Cara Bayar - Admin',
            'breadcrumb' : 'Cara Bayar',
            'form' : form,
            'highlight' : highlight,
            'deskripsi' : deskripsi,
            'embed' : embed_maps_bayar,
            'image' : image_cara_bayar,
            'bank' : bank,
            'norek' : norek,
            'an' : an,
            'faq' : faq,
        }
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        infografis = ''
        jenis = request.POST.get('jenis')
        form = ''
        if jenis == 'overview':
            try:
                infografis = AppSetting.objects.get(nama='highlight')
                infografis.keterangan = request.POST.get('highlight')
                infografis.save()
            except:
                infografis = AppSetting(nama='highlight', keterangan=request.POST.get('highlight'))
                infografis.save()

            try:
                infografis = AppSetting.objects.get(nama='deskripsi')
                infografis.keterangan = request.POST.get('deskripsi')
                infografis.save()
            except:
                infografis = AppSetting(nama='deskripsi', keterangan=request.POST.get('deskripsi'))
                infografis.save()

            try:
                infografis = AppSetting.objects.get(nama='embed_maps_bayar')
                maps = request.POST.get('embed_maps_bayar')
                print(maps)
                infografis.keterangan = maps
                infografis.save()
            except:
                infografis = AppSetting(nama='embed_maps_bayar', keterangan=request.POST.get('embed_maps_bayar'))
                infografis.save()

            try: 
                image = request.FILES.get('image')
                if 'image' in request.FILES:
                    foto_diskon = AppSetting.objects.get(nama = 'image_cara_bayar')
                    foto_diskon.image = image
                    foto_diskon.save()
            except: 
                image = request.FILES.get('image')
                foto_diskon = AppSetting(nama='image_cara_bayar', image=request.FILES.get('image'))
                foto_diskon.save()

        if jenis == 'pembayaran':
            try:
                infografis = AppSetting.objects.get(nama='bank')
                infografis.keterangan = request.POST.get('bank')
                infografis.save()
            except:
                infografis = AppSetting(nama='bank', keterangan=request.POST.get('bank'))
                infografis.save()

            try:
                infografis = AppSetting.objects.get(nama='norek')
                infografis.keterangan = request.POST.get('norek')
                infografis.save()
            except:
                infografis = AppSetting(nama='norek', keterangan=request.POST.get('norek'))
                infografis.save()

            try:
                infografis = AppSetting.objects.get(nama='an')
                infografis.keterangan = request.POST.get('an')
                infografis.save()
            except:
                infografis = AppSetting(nama='an', keterangan=request.POST.get('an'))
                infografis.save()

        if jenis == 'faq':
            faqs = []
            for key in request.POST:
                if key.startswith('p') and key[1:].isdigit():
                    index = int(key[1:])
                    pertanyaan = request.POST[key]
                    jawaban = request.POST.get(f'j{index}', '')
                    highlight = request.POST.get(f'h{index}', '')
                    existing_faq = FAQ.objects.filter(highlight = highlight).first()

                    if existing_faq:
                        # Jika ada, update nilai highlight
                        existing_faq.pertanyaan = pertanyaan
                        existing_faq.jawaban = jawaban
                        existing_faq.save()
                    else:
                        # Jika tidak ada, buat objek FAQ baru
                        faq = FAQ(pertanyaan=pertanyaan, jawaban=jawaban, highlight=highlight)
                        faqs.append(faq)

            # Simpan semua FAQ baru ke database
            FAQ.objects.bulk_create(faqs)

        messages.success(request, 'Informasi berhasil disimpan.')
        return redirect('profile:admin_setting_carabayar')
    
@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
def admin_index_infografis(request):
    form = ''

    if request.method == 'GET':
        try: 
            deskripsi = RnW.objects.get(jenis = 'deskripsi')
        except: 
            deskripsi = ''
        try: 
            pertanyaan_1 = RnW.objects.get(jenis = 'pertanyaan_1')
        except: 
            pertanyaan_1 = ''
        try: 
            jawaban_1 = RnW.objects.get(jenis = 'jawaban_1')
        except: 
            jawaban_1 = ''
        try: 
            pertanyaan_2 = RnW.objects.get(jenis = 'pertanyaan_2')
        except: 
            pertanyaan_2 = ''
        try: 
            jawaban_2 = RnW.objects.get(jenis = 'jawaban_2')
        except: 
            jawaban_2 = ''
        try: 
            pertanyaan_3 = RnW.objects.get(jenis = 'pertanyaan_3')
        except: 
            pertanyaan_3 = ''
        try: 
            jawaban_3 = RnW.objects.get(jenis = 'jawaban_3')
        except: 
            jawaban_3 = ''
        try: 
            pertanyaan_4 = RnW.objects.get(jenis = 'pertanyaan_4')
        except: 
            pertanyaan_4 = ''
        try: 
            jawaban_4 = RnW.objects.get(jenis = 'jawaban_4')
        except: 
            jawaban_4 = ''
        try: 
            pertanyaan_5 = RnW.objects.get(jenis = 'pertanyaan_5')
        except: 
            pertanyaan_5 = ''
        try: 
            jawaban_5 = RnW.objects.get(jenis = 'jawaban_5')
        except: 
            jawaban_5 = ''

        try: 
            alamat = AppSetting.objects.get(nama = 'alamat')
        except: 
            alamat = ''
            
        try: 
            alamat_link = AppSetting.objects.get(nama = 'alamat_link')
        except: 
            alamat_link = ''

        try: 
            embed_maps = AppSetting.objects.get(nama = 'embed_maps')
        except: 
            embed_maps = ''

        try: 
            telepon = AppSetting.objects.get(nama = 'telepon')
        except: 
            telepon = ''

        try: 
            telepon_text = AppSetting.objects.get(nama = 'telepon_text')
        except: 
            telepon_text = ''

        try: 
            whatsapp = AppSetting.objects.get(nama = 'whatsapp')
        except: 
            whatsapp = ''

        try: 
            whatsapp_text = AppSetting.objects.get(nama = 'whatsapp_text')
        except: 
            whatsapp_text = ''

        try: 
            email = AppSetting.objects.get(nama = 'email')
        except: 
            email = ''

        try: 
            nomor_spk = AppSetting.objects.get(nama = 'nomor_spk')
        except: 
            nomor_spk = ''

        try: 
            nomor_bap = AppSetting.objects.get(nama = 'nomor_bap')
        except: 
            nomor_bap = ''

        try: 
            nomor_bast = AppSetting.objects.get(nama = 'nomor_bast')
        except: 
            nomor_bast = ''

        try: 
            nomor_bapa = AppSetting.objects.get(nama = 'nomor_bapa')
        except: 
            nomor_bapa = ''

        try: 
            nomor_faktur = AppSetting.objects.get(nama = 'nomor_faktur')
        except: 
            nomor_faktur = ''

        try: 
            nomor_dok = AppSetting.objects.get(nama = 'nomor_dok')
        except: 
            nomor_dok = ''

        try: 
            facebook = AppSetting.objects.get(nama = 'facebook')
        except: 
            facebook = ''

        try: 
            link_facebook = AppSetting.objects.get(nama = 'link_facebook')
        except: 
            link_facebook = ''

        try: 
            link_twitter = AppSetting.objects.get(nama = 'link_twitter')
        except: 
            link_twitter = ''

        try: 
            link_instagram = AppSetting.objects.get(nama = 'link_instagram')
        except: 
            link_instagram = ''

        try: 
            link_youtube = AppSetting.objects.get(nama = 'link_youtube')
        except: 
            link_youtube = ''

        try: 
            lazada = AppSetting.objects.get(nama = 'lazada')
        except: 
            lazada = ''

        try: 
            shopee = AppSetting.objects.get(nama = 'shopee')
        except: 
            shopee = ''

        try: 
            tokped = AppSetting.objects.get(nama = 'tokped')
        except: 
            tokped = ''

        try: 
            link_tiktok = AppSetting.objects.get(nama = 'link_tiktok')
        except: 
            link_tiktok = ''

        try: 
            header_layanan = AppSetting.objects.get(nama = 'header_layanan')
        except: 
            header_layanan = ''

        try: 
            header_galeri = AppSetting.objects.get(nama = 'header_galeri')
        except: 
            header_galeri = ''

        try: 
            header_kontak = AppSetting.objects.get(nama = 'header_kontak')
        except: 
            header_kontak = ''

        try: 
            foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
        except: 
            foto_diskon = ''

        try: 
            diskon = AppSetting.objects.get(nama = 'diskon')
        except: 
            diskon = ''

        try: 
            tanggal = AppSetting.objects.get(nama = 'tanggal')
            tanggal_lahir = tanggal.tanggal.strftime('%Y-%m-%d')
        except: 
            tanggal_lahir = ''

        # form = InfografisForm()
        context = {
            'title' : 'Infografis - Admin',
            'alamat' : alamat,
            'alamat_link' : alamat_link,
            'embed_maps' : embed_maps,
            'telepon' : telepon,
            'telepon_text' : telepon_text,
            'whatsapp' : whatsapp,
            'whatsapp_text' : whatsapp_text,
            'email' : email,
            'nomor_spk' : nomor_spk,
            'nomor_bap' : nomor_bap,
            'nomor_dok' : nomor_dok,
            'nomor_faktur' : nomor_faktur,
            'nomor_bapa' : nomor_bapa,
            'nomor_bast' : nomor_bast,
            'facebook' : facebook,
            'link_facebook' : link_facebook,
            'link_youtube' : link_youtube,
            'link_twitter' : link_twitter,
            'link_instagram' : link_instagram,
            'link_tiktok' : link_tiktok,
            'breadcrumb' : 'Infografis',
            'header_layanan' : header_layanan,
            'header_galeri' : header_galeri,
            'header_kontak' : header_kontak,
            'form' : form,
            'p1' : pertanyaan_1,
            'p2' : pertanyaan_2,
            'p3' : pertanyaan_3,
            'p4' : pertanyaan_4,
            'p5' : pertanyaan_5,
            'j1' : jawaban_1,
            'j2' : jawaban_2,
            'j3' : jawaban_3,
            'j4' : jawaban_4,
            'j5' : jawaban_5,
            'deskripsi' : deskripsi,
            'lazada' : lazada,
            'shopee' : shopee,
            'tokped' : tokped,
            'foto_diskon' : foto_diskon,
            'diskon' : diskon,
            'tanggal' : tanggal_lahir,
        }
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        infografis = ''
        jenis = request.POST.get('jenis')
        form = ''
        if jenis == 'overview':
            try:
                infografis = AppSetting.objects.get(nama='alamat')
                infografis.keterangan = request.POST.get('alamat')
                infografis.save()
            except:
                infografis = AppSetting(nama='alamat', keterangan=request.POST.get('alamat'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='alamat_link')
                infografis.keterangan = request.POST.get('alamat_link')
                infografis.save()
            except:
                infografis = AppSetting(nama='alamat_link', keterangan=request.POST.get('alamat_link'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='embed_maps')
                infografis.keterangan = request.POST.get('embed_maps')
                infografis.save()
            except:
                infografis = AppSetting(nama='embed_maps', keterangan=request.POST.get('embed_maps'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='telepon_text')
                infografis.keterangan = request.POST.get('telepon_text')
                infografis.save()
            except:
                infografis = AppSetting(nama='telepon_text', keterangan=request.POST.get('telepon_text'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='telepon')
                infografis.keterangan = request.POST.get('telepon')
                infografis.save()
            except:
                infografis = AppSetting(nama='telepon', keterangan=request.POST.get('telepon'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='whatsapp')
                infografis.keterangan = request.POST.get('whatsapp')
                infografis.save()
            except:
                infografis = AppSetting(nama='whatsapp', keterangan=request.POST.get('whatsapp'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='whatsapp_text')
                infografis.keterangan = request.POST.get('whatsapp_text')
                infografis.save()
            except:
                infografis = AppSetting(nama='whatsapp_text', keterangan=request.POST.get('whatsapp_text'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='email')
                infografis.keterangan = request.POST.get('email')
                infografis.save()
            except:
                infografis = AppSetting(nama='email', keterangan=request.POST.get('email'))
                infografis.save()

        if jenis == 'sosmed':
            try:
                infografis = AppSetting.objects.get(nama='facebook')
                infografis.keterangan = request.POST.get('facebook')
                infografis.save()
            except:
                infografis = AppSetting(nama='facebook', keterangan=request.POST.get('facebook'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_facebook')
                infografis.keterangan = request.POST.get('link_facebook')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_facebook', keterangan=request.POST.get('link_facebook'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_twitter')
                infografis.keterangan = request.POST.get('link_twitter')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_twitter', keterangan=request.POST.get('link_twitter'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_youtube')
                infografis.keterangan = request.POST.get('link_youtube')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_youtube', keterangan=request.POST.get('link_youtube'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_instagram')
                infografis.keterangan = request.POST.get('link_instagram')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_instagram', keterangan=request.POST.get('link_instagram'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='link_tiktok')
                infografis.keterangan = request.POST.get('link_tiktok')
                infografis.save()
            except:
                infografis = AppSetting(nama='link_tiktok', keterangan=request.POST.get('link_tiktok'))
                infografis.save()

        if jenis == 'ecomerce':
            try:
                infografis = AppSetting.objects.get(nama='lazada')
                infografis.keterangan = request.POST.get('lazada')
                infografis.save()
            except:
                infografis = AppSetting(nama='lazada', keterangan=request.POST.get('lazada'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='shopee')
                infografis.keterangan = request.POST.get('shopee')
                infografis.save()
            except:
                infografis = AppSetting(nama='shopee', keterangan=request.POST.get('shopee'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='tokped')
                infografis.keterangan = request.POST.get('tokped')
                infografis.save()
            except:
                infografis = AppSetting(nama='tokped', keterangan=request.POST.get('tokped'))
                infografis.save()

        if jenis == 'header':
            try:
                infografis = AppSetting.objects.get(nama='header_layanan')
                infografis.keterangan = request.POST.get('header_layanan')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_layanan', keterangan=request.POST.get('header_layanan'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='header_galeri')
                infografis.keterangan = request.POST.get('header_galeri')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_galeri', keterangan=request.POST.get('header_galeri'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='header_kontak')
                infografis.keterangan = request.POST.get('header_kontak')
                infografis.save()
            except:
                infografis = AppSetting(nama='header_kontak', keterangan=request.POST.get('header_kontak'))
                infografis.save()



        if jenis == 'setting_nomor':
            try:
                infografis = AppSetting.objects.get(nama='nomor_spk')
                infografis.keterangan = request.POST.get('nomor_spk')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_spk', keterangan=request.POST.get('nomor_spk'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='nomor_bap')
                infografis.keterangan = request.POST.get('nomor_bap')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_bap', keterangan=request.POST.get('nomor_bap'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='nomor_bast')
                infografis.keterangan = request.POST.get('nomor_bast')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_bast', keterangan=request.POST.get('nomor_bast'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='nomor_bapa')
                infografis.keterangan = request.POST.get('nomor_bapa')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_bapa', keterangan=request.POST.get('nomor_bapa'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='nomor_faktur')
                infografis.keterangan = request.POST.get('nomor_faktur')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_faktur', keterangan=request.POST.get('nomor_faktur'))
                infografis.save()
            try:
                infografis = AppSetting.objects.get(nama='nomor_dok')
                infografis.keterangan = request.POST.get('nomor_dok')
                infografis.save()
            except:
                infografis = AppSetting(nama='nomor_dok', keterangan=request.POST.get('nomor_dok'))
                infografis.save()

        if jenis == 'reseller':
            try: 
                image = request.FILES.get('image')
                if 'image' in request.FILES:
                    foto_diskon = AppSetting.objects.get(nama = 'foto_diskon')
                # print(image)
                # path_file_lama = f"{settings.MEDIA_ROOT}/{foto_diskon.image}"
                # os.remove(path_file_lama)
                # print(image)
                    foto_diskon.image = image
                    foto_diskon.save()
            except: 
                image = request.FILES.get('image')
                # print(image)
                foto_diskon = AppSetting(nama='foto_diskon', image=request.FILES.get('image'))
                foto_diskon.save()

            try:
                diskon = AppSetting.objects.get(nama='diskon')
                diskon.keterangan = request.POST.get('diskon')
                diskon.save()
            except:
                diskon = AppSetting(nama='diskon', keterangan=request.POST.get('diskon'))
                diskon.save()

            try:
                tanggal = AppSetting.objects.get(nama='tanggal')
                tanggal.tanggal = request.POST.get('tanggal')
                tanggal.save()
            except:
                tanggal = AppSetting(nama='tanggal', tanggal=request.POST.get('tanggal'))
                tanggal.save()

        messages.success(request, 'Informasi berhasil disimpan.')
        return redirect('profile:admin_setting_infografis')
        

@login_required
@is_verified()
@role_required(allowed_roles=['admin'])
@require_http_methods(["POST"])
def admin_index_infografis_edit(request):
    if request.method == 'POST':
        running_text = RunningText.objects.get(id = request.POST.get('id'))
        form = RunningTextForm(data=request.POST, instance=running_text)
        if form.is_valid():
            form.save()
            messages.success(request, 'Running Text berhasil disimpan.')
            return redirect('profile:admin_setting_running_text',)
        messages.error(request, 'Running Text gagal disimpan.')
        return redirect('profile:admin_setting_running_text',)




@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_running_link(request):
    template = ''
    context = {} 
    form = ''
    form_foto = ''

    if request.method == 'GET':
        form = RunningLinkForm()
        form_foto = RunningLinkFotoForm()
        context = {
            'title' : 'ADD Partner Kami',
            'form' : form,
            'form_foto' : form_foto,
            'breadcrumb' : 'Partner Kami',
        }

        template = 'profile/admin/setting/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        logo = request.FILES.get('foto')
        form = RunningLink()

        is_valid = True

        if is_valid:
            form.logo = logo
            form.save()

            messages.success(request, 'Running berhasil disimpan.')
            return redirect('profile:admin_setting_running_link',)

        messages.error(request, 'Running gagal disimpan.')
        return render(request, 'profile/admin/setting/create.html', {'title' : 'ADD Running','form' : form,'form_foto' : form_foto, 'breadcrumb' : 'Running',})



@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_running_link(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        try:
            running_link =RunningLink.objects.get(id=id)
            form =RunningLinkForm(instance=running_link)
            form_foto =RunningLinkFotoForm()
        except:
            running_link = None
            form = RunningLinkForm()
            form_foto = RunningLinkFotoForm()
        
        context = {
            'title' : 'EDIT Partner Kami',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'running_link': running_link,
            'breadcrumb': 'Partner Kami',
        }
        template = 'profile/admin/setting/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        running_link = RunningLink.objects.get(id=id)
        foto_old = bool(running_link.logo)

        if foto_old : 
            path_file_lama = f"{settings.MEDIA_ROOT}/{running_link.logo}"

        is_valid = True

        if is_valid:
            if foto_old : 
                if len(request.FILES) > 0:
                    try:
                        os.remove(path_file_lama)
                    except:
                        pass
            running_link.logo = request.FILES.get('foto')
            running_link.save()

            messages.success(request, 'Partner Kami berhasil disimpan.')
            return redirect('profile:admin_setting_running_link')

        messages.error(request, 'Partner Kami gagal disimpan.')
        return render(request, 'profile/admin/setting/create.html', {
            'title' : 'EDIT Partner Kami',
            'form' : form,
            'form_foto' : form_foto,
            'edit' : 'true',
            'running_link': running_link,
            'breadcrumb': 'Partner Kami',})



@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def admin_index_aboutme(request, section):
    if request.method == 'GET':
        try:
            headAbout = AboutMe.objects.get(section=section, jenis='header')
        except Exception as e:
            headAbout = ''
            messages.success(request, 'Data Header belum ada, silahkan diisi.')
        
        listAbout = AboutMe.objects.filter(section=section, jenis='child').order_by('id')

        context = {
            'title' : section.title()+' - Admin',
            'headAbout' : headAbout,
            'listAbout' : listAbout,
            'breadcrumb' : section.title(),
            'section': section,
        }
        
        return render(request, 'profile/admin/setting/index.html', context)

    if request.method == 'POST':
        youtube = request.POST.get('keterangan')
        images = request.FILES.get('images')
        print(images)
        try:
            ### JIKA SUDAH ADA == EDIT
            abooot = AboutMe.objects.get(section=section, jenis='header')
            abooot.judul = request.POST.get('judul')
            abooot.deskripsi = request.POST.get('deskripsi')
            abooot.youtube = youtube            
            if 'images' in request.FILES:
                abooot.images = images
                abooot.save()
            abooot.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_setting_aboutme', section=section)

        except Exception as e:
            ### JIKA BELUM ADA == ADD
            form = AboutMeForm(data=request.POST)
            if form.is_valid():
                form.save()

                messages.success(request, 'Data berhasil disimpan.')
                return redirect('profile:admin_setting_aboutme', section=section)

        messages.error(request, 'Data gagal disimpan.')
        return redirect('profile:admin_setting_aboutme', section=section)


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def create_aboutme(request, section):
    template = ''
    context = {} 
    form = '' 

    ### SPLIT DATA ICON PER LIMIT ===========================================================
    # fonticon = read_icon(section)
    # limit = 3
    # total = len(fonticon)
    # hasil = []

    # for i in range(0,total,limit):
    #     hasil.append(fonticon[i:i+limit])

    if request.method == 'GET':
        form = AboutMeForm()
        context = {
            'title' : 'ADD '+section.title(),
            'form' : form,
            'breadcrumb' : section.title(),
            'section' : section,
            # 'dticon' : hasil,
        }

        template = 'profile/admin/setting/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        form = AboutMeForm(data=request.POST)
        if form.is_valid():
            AboutMe_add = form.save(commit=False)
            AboutMe_add.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_setting_aboutme', section=section)

        context = {
            'title' : 'ADD '+section.title(),
            'form' : form,
            'breadcrumb' : section.title(),
            'section' : section,
            # 'dticon' : hasil,
        }

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/setting/create.html', context)


@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def edit_aboutme(request, section, id):
    template = ''
    context = {} 
    form = ''

    ### SPLIT DATA ICON PER LIMIT ===========================================================
    # fonticon = read_icon(section)
    # limit = 3
    # total = len(fonticon)
    # hasil = []

    # for i in range(0,total,limit):
    #     hasil.append(fonticon[i:i+limit])
    
    if request.method == 'GET':
        try:
            AboutMe_get = AboutMe.objects.get(id=id)
            form = AboutMeForm(instance=AboutMe_get)
        except:
            AboutMe_get = None
            form = AboutMeForm()
        
        context = {
            'title' : 'ADD '+section.title(),
            'form' : form,
            'breadcrumb' : section.title(),
            'section' : section,
            # 'dticon' : hasil,
            'edit' : 'true',
            'aidi' : id,
            'dataAbout' : AboutMe_get,
        }
        template = 'profile/admin/setting/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        AboutMe_get = AboutMe.objects.get(id=id)
        form = AboutMeForm(data=request.POST, instance=AboutMe_get)

        if form.is_valid():
            AboutMe_update = form.save(commit=False)
            AboutMe_update.judul = form.cleaned_data.get('judul')
            AboutMe_update.deskripsi = form.cleaned_data.get('deskripsi')
            # AboutMe_update.icon = form.cleaned_data.get('icon')
            AboutMe_update.save()

            messages.success(request, 'Data berhasil disimpan.')
            return redirect('profile:admin_setting_aboutme', section=section)

        context = {
            'title' : 'ADD '+section.title(),
            'form' : form,
            'breadcrumb' : section.title(),
            'section' : section,
            # 'dticon' : hasil,
            'edit' : 'true',
            'aidi' : id,
            'dataAbout' : AboutMe_get,
        }

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/setting/create.html', context)

@login_required
@is_verified()
# @role_required(allowed_roles=['admin'])
def delete_aboutme(request, section, id):
    message = ''

    try:
        AboutMe_del = AboutMe.objects.get(id=id)
        AboutMe_del.delete()
        message = 'success'
    except AboutMe.DoesNotExist:
        message = 'error'

    context = { 'message' : message, }
    return JsonResponse(context)