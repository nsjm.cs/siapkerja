from django.shortcuts import render, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from rizq_sanjaya_app.decorators import is_verified
from ..forms import *
from ..models import *
from django.db import connections
from ..helpers import dictfetchall

@login_required
@is_verified()
@require_http_methods(["GET"])
def index(request):
    

    # last_month = datetime.date.today() - datetime.timedelta(days=30)
    # with connections['default'].cursor() as cursor:
    #     cursor.execute("""
    #         SELECT 
    #             DISTINCT a.browser_type as browser_type,
    #             (SELECT count(b.browser_type) FROM public.rizq_sanjaya_app_logvisitor b 
    #             WHERE b.browser_type=a.browser_type and b.device_type<>'-') as total_count
    #         FROM public.rizq_sanjaya_app_logvisitor a WHERE a.device_type<>'-';""")
    #     browsers = dictfetchall(cursor)
    # browser_labels = []
    # browser_count = []
    # for browser in browsers:
    #     browser_labels.append(browser['browser_type'])
    #     browser_count.append(browser['total_count'])
    
    # with connections['default'].cursor() as cursor:
    #     cursor.execute("""
    #         SELECT 
    #             DISTINCT a.device_type as device_type,
    #             (SELECT count(b.device_type) FROM public.rizq_sanjaya_app_logvisitor b 
    #             WHERE b.device_type=a.device_type and b.device_type<>'-') as total_count
    #         FROM public.rizq_sanjaya_app_logvisitor a WHERE a.device_type<>'-';""")
    #     devices = dictfetchall(cursor)
    # device_labels = []
    # device_count = []
    # for device in devices:
    #     device_labels.append(device['device_type'])
    #     device_count.append(device['total_count'])
    
    # with connections['default'].cursor() as cursor:
    #     cursor.execute("""
    #         SELECT d::date as tanggal,
    #             (SELECT count(id) FROM public.rizq_sanjaya_app_logvisitor b 
    #             WHERE d::date=date(waktu) and b.device_type<>'-') as total_count
    #         FROM generate_series(DATE(NOW()) - INTERVAL '14 DAYS', DATE(NOW()), '1 day'::interval) d""")
    #     dates = dictfetchall(cursor)
    # date_labels = []
    # date_count = []
    # for date in dates:
    #     date_labels.append(date['tanggal'].strftime("%d/%m/%Y"))
    #     date_count.append(date['total_count'])

    context = {
        
        'title':'Dashboard',
        # 'device_labels': device_labels,
        # 'device_count': device_count,
        # 'date_labels': date_labels,
        # 'date_count': date_count,
        # 'browser_labels': browser_labels,
        # 'browser_count': browser_count,
        # 'agendas': agendas,
    }
    return render(request, 'profile/admin/index.html', context)