from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from ..models import OPD as m_skpd, MasterKegiatan as m_kegiatan, SPKRINCIAN , masterpembayaran as m_pembayaran, SPP as spp, SPK as spk
from django.utils import timezone
import os, re
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db.models import Sum, F, Q, DecimalField, Value, Prefetch
from django.db.models.functions import Coalesce
from ..support_function import format_clear as sup 
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, get_total_nilai_spk, cek_ba_spk, bast_validation_exists, cek_nomor_terbaru,ba_validation_exists
from pprint import pprint
from rizq_sanjaya_app.support_function import PagingHelper

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    
    wilayah_list = lokasi.objects.filter(status='Aktif')
    skpd_list = m_skpd.objects.filter(status='Aktif')
    perusahaan_list = perusahaan.objects.filter(status='Aktif')
    permohonan_list = SPP.objects.filter(status='Aktif')
    archives = SPK.objects.filter(status='Tidak aktif')
    pengesah = pejabatpengesah.objects.filter(status='Aktif')
    # spk_list = SPK.objects.filter(status='Aktif').annotate(total_tagihan = Sum(F('spkrincian__realisasi'))).order_by('-created_at')

    # Keperluan Datatable
    page_obj = PagingHelper.spp(request)
    # END Keperluan Datatable

    context = {
        'title' : 'Data SPK',
        'page_obj': page_obj,
        'wilayah_list' : wilayah_list,
        'skpd_list' : skpd_list,
        'perusahaan_list' : perusahaan_list,
        'permohonan_list' : permohonan_list,
        'archives' : archives,
        'pengesah' : pengesah,
        
    }
    
    return render(request, 'profile/admin/permohonan/index.html', context)

@login_required
@is_verified()
@ba_validation_exists(redirect_to = 'profile:admin_permohonan_pembayaran')
def create(request, id_spk):

    if request.method == 'GET':
        dt_spk = SPK.objects.get(id=id_spk)
        total_tagihan = get_total_nilai_spk(dt_spk.id)
        print(total_tagihan)

        statuspembayaran_list = m_pembayaran.objects.filter(status='Aktif')
        
        regex = re.compile(r'^(\d{3})/')

        spp_no = f"{1:03d}"

        all_spp = SPP.objects.all()
        if all_spp:
            nomor_urut_list = [
                int(regex.search(spp.nosurat).group(1)) 
                for spp in all_spp if regex.search(spp.nosurat)
            ]
            last_number =  max(nomor_urut_list) if nomor_urut_list else 0

            
            no_spp = last_number + 1
            spp_no = f"{no_spp:03d}"
    
        
        context = {
            'title' : 'Ubah Data SPK',
            'statuspembayaran_list' : statuspembayaran_list,
            'dt_spk' : dt_spk,
            'total_tagihan' : total_tagihan,
            'spp_no' : spp_no,
    
        }
        return render(request, 'profile/admin/permohonan/modal_form.html', context)
    
    if request.method == 'POST':
        try:
            with transaction.atomic():
                nospk = request.POST.get('nospk')
                nosurat = request.POST.get('nosurat')
                masterpembayaran = request.POST.get('masterpembayaran')
                totalpembayaran = get_total_nilai_spk(nospk)

                nosurat = cek_nomor_terbaru(SPP, 'nosurat', nomor_format = nosurat)

                if nosurat is not None:
                    insert_SPP = SPP()
                    insert_SPP.nosurat = nosurat
                    insert_SPP.SPK_id = nospk
                    insert_SPP.masterpembayaran_id = masterpembayaran
                    insert_SPP.totalpembayaran = totalpembayaran
                    insert_SPP.save()
                    messages.success(request, 'Data Permohonan Pembayaran berhasil disimpan.')
                    return redirect('profile:admin_permohonan_pembayaran')

        except Exception as e:
            print('Error insert data SPK', e)
            messages.error(request, 'Data Permohonan Pembayaran gagal disimpan.')
            return redirect('profile:admin_permohonan_pembayaran')


@login_required
@is_verified()
def editSPP(request, id_spp):
    if request.method == 'GET':
        dt_spp = SPP.objects.get(id = id_spp)
        statuspembayaran_list = m_pembayaran.objects.filter(status='Aktif')
        print(dt_spp.SPK.id)
        total_tagihan = get_total_nilai_spk(dt_spp.SPK.id)

        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'total_tagihan' : total_tagihan,
            'statuspembayaran_list' : statuspembayaran_list,
            'data_spp' : dt_spp,
    
        }
        template = 'profile/admin/permohonan/modal_form.html'
        return render(request, template, context)


    
    if request.method == 'POST':
        try:
            with transaction.atomic():
                spp_instance = get_object_or_404(SPP, id=id_spp)
                nospk = request.POST.get('nospk')
                nosurat = request.POST.get('nosurat')
                masterpembayaran = request.POST.get('masterpembayaran')
                totalpembayaran = request.POST.get('totalpembayaran')
                totalpembayaran = sup(totalpembayaran)

                total_tagihan = get_total_nilai_spk(spp_instance.SPK.id)
                
                if nospk is not None:
                    spp_instance.nosurat = nosurat
                    spp_instance.SPK_id = nospk
                    spp_instance.masterpembayaran_id = masterpembayaran
                    spp_instance.totalpembayaran = total_tagihan
                    spp_instance.save()
                    messages.success(request, 'Data Permohonan Pembayaran berhasil disimpan.')
                    return redirect('profile:admin_permohonan_pembayaran')
                else:
                    messages.error(request, 'Data gagal disimpan.')
                    return redirect('profile:admin_permohonan_pembayaran')

        except Exception as e:
            print('Error insert data SPK', e)
            messages.error(request, 'Data Permohonan Pembayaran gagal disimpan.')
            return redirect('profile:admin_permohonan_pembayaran')


@login_required
@is_verified()
def detailBA(request, id_spk):    
    template = ''
    context = {} 
    form = ''
    if request.method == 'GET':
        try:
            data_detail_spk = get_object_or_404(SPK, id=id_spk)
        except Exception as e:
            messages.error(request, 'SPK tidak ditemukan.')
            return redirect('profile:admin_spk')

        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id_spk)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id_spk)
        data_rincian = []

        data_subkegiatan = m_kegiatan.objects.get(kegiatan_id = data_detail_spk.subkegiatan.kegiatan_id).get_descendants().filter(is_sumberdana = True)

        pagu_subkegiatan = 0

        try:
            dt_spp = spp.objects.prefetch_related('SPK').get(SPK_id = id_spk)
        except Exception as e:
            dt_spp = ''

        try:
            with transaction.atomic():
                for x in data_subkegiatan:
                    for rekening in x.get_ancestors().filter(is_rekening_belanja = True):
                        data_rekening = {'kegiatan_kode': rekening.kegiatan_kode, 'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 'children': []}
                        for bagian in rekening.get_descendants().filter(is_bagian = True, kegiatan_nama = data_detail_spk.bagian.kegiatan_nama):
                            data_bagian = {'kegiatan_kode': '', 'kegiatan_nama': bagian.kegiatan_nama, 'children': []}
                            for keterangan in bagian.get_descendants().filter(is_subrekening_belanja = True):
                                pagu_per_keterangan = 0
                                data_keterangan = {'kegiatan_kode': '', 'kegiatan_nama': keterangan.kegiatan_nama, 'children': [], 'pagu_per_keterangan': pagu_per_keterangan}
                                for ssh in keterangan.get_descendants():
                                    subtotal = ssh.nilai * ssh.volume
                                    pagu_subkegiatan+=subtotal
                                    try:
                                        current_value = SPKRINCIAN.objects.get(SPK = data_detail_spk, ssh__kegiatan_id= ssh.kegiatan_id).realisasi
                                    except Exception as e:
                                        current_value = 0

                                    data_ssh = {
                                            'kegiatan_kode': '', 'kegiatan_nama': ssh.uraian_ssh, 
                                            'volume': ssh.volume,
                                            'satuan': ssh.satuan,
                                            'nilai': ssh.nilai,
                                            'subtotal': subtotal,
                                            'kegiatan_id': ssh.kegiatan_id,
                                            'sisa_pagu': round(get_sisa_pagu(data_detail_spk.tahapan, ssh.kegiatan_id, data_detail_spk.id), 2),
                                            'current_value': current_value,
                                    }

                                    data_keterangan['children'].append(data_ssh)
                                    data_keterangan['pagu_per_keterangan'] += subtotal
                                data_rekening['children'].append(data_keterangan)
                        data_rincian.append(data_rekening)
        except Exception as e:
            print('Error get detail SPK', e)
            pass

        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            "data_rincian": data_rincian,
            "pagu_subkegiatan": pagu_subkegiatan,
            'have_spp': cek_ba_spk(id_spk),
            'dt_spp': dt_spp,
           
        }
        template = 'profile/admin/permohonan/detail.html'
        return render(request, template, context)



@login_required
@is_verified()
def print_permohonan(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        context = {
            'dt_spk':dt_spk,
        }

        template = 'profile/admin/permohonan/modal_print.html'
        return render(request, template, context)


@login_required
@is_verified()
def cetakBA(request, id):
    if request.method == 'GET':
        template = ''
        context = {} 
        form = ''
        
        try:
            dt_spk = SPK.objects.get(id=id)
            total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']
        except Exception:
            dt_spk = {}
            dt_spp = {}

        try:
            dt_spp = SPP.objects.get(SPK_id = id)
        except Exception:
            dt_spp = {}
            dt_spk = {}


        
        get_tgl = request.GET.get('tanggal')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')
        # dt_sppsubrincian = SPKSUBRINCIAN.objects.get(id = id)
        # dt_spprincian = SPKSUBRINCIAN.objects.get(id = id)
        # dt_spp = SPKRINCIAN.objects.get(id = id)

        context = {
            'title' : 'Laporan Surat Permohonan Pembayaran',
            'form' : form,
            'edit' : 'true',
            'data_spk_' : dt_spk,
            'data_spp_' : dt_spp,
            'tgl_print' : tgl_print,
            'total_tagihan' : total_tagihan,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    
        }

        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/permohonan/print.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')

        
@login_required
@is_verified()
def cetakKwitansi(request, id):
    if request.method == 'GET':
        try:
            dt_spk = SPK.objects.get(id=id)
            total_tagihan = dt_spk.spkrincian_set.aggregate(total_tagihan=Sum('realisasi'))['total_tagihan']
        except Exception:
            dt_spk = {}
            dt_spp = {}

        try:
            dt_spp = SPP.objects.get(SPK_id = id)
        except Exception:
            dt_spp = {}
            dt_spk = {}

        get_tgl = request.GET.get('tanggal')
        tgl_print = datetime.strptime(get_tgl, '%Y-%m-%d')
        id_bendahara = request.GET.get('bendahara')
        dt_bendahara = get_object_or_404(pejabatpengesah, id=id_bendahara)
        id_pptk = request.GET.get('pptk')
        dt_pptk = get_object_or_404(pejabatpengesah, id=id_pptk)
        id_pengguna = request.GET.get('pengguna')
        dt_pengguna = get_object_or_404(pejabatpengesah, id=id_pengguna)
        print(tgl_print)
        context = {
            'title' : 'Kwitansi',
            'edit' : 'true',
            'data_spk_' : dt_spk,
            'data_spp_' : dt_spp,
            'tgl_print' : tgl_print,
            'dt_bendahara' : dt_bendahara,
            'total_tagihan' : total_tagihan,
            'dt_pptk' : dt_pptk,
            'dt_pengguna' : dt_pengguna,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    
        }

        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/permohonan/kwitansi.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')



    
    

@login_required
@is_verified()
def printkwitansi(request, id):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        pengesah = pejabatpengesah.objects.filter(status='Aktif')
        context = {
            'dt_spk':dt_spk,
            'pengesah':pengesah,
        }

        template = 'profile/admin/permohonan/print_kwitansi.html'
        return render(request, template, context)
    



@login_required
@is_verified()
def edit(request, id):
    if request.method == 'GET':
        dt_spk = SPK.objects.get(id = id)
        
        context = {
            'title' : 'Ubah Data SPK',
            'edit' : 'true',
            'data_spk_' : dt_spk,
    
        }
        template = 'profile/admin/permohonan/modal_form.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_spk = SPK.objects.get(id = id)
        nospk = request.POST.get('nospk')
        uraipekerjaan = request.POST.get('uraipekerjaan')
        perusahaan = request.POST.get('perusahaan')
        tglspk = request.POST.get('tglspk')
        sumberdana = request.POST.get('sumberdana')
        waktupelaksanaan = request.POST.get('waktupelaksanaan')
        opd = request.POST.get('opd')
        perusahaan = request.POST.get('perusahaan')

        if data_spk is not None:
            
            data_spk.nospk = nospk
            data_spk.tglspk = tglspk
            data_spk.uraipekerjaan = uraipekerjaan
            data_spk.sumberdana = sumberdana
            data_spk.waktupelaksanaan = waktupelaksanaan
            data_spk.opd_id = opd
            data_spk.perusahaan_id = perusahaan
         
            data_spk.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_spk')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/trans_spk/index.html', {'form': form,})


@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        sekarang = timezone.now()
        doc = SPK.objects.get(id=id)
        doc.deleted_at = sekarang
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except SPK.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        
        doc = SPK.objects.get(id=id)
        doc.deleted_at = None
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except SPK.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

# # # # # # # # # # #DETAIL SPK # # # # # # # # # # # # # # # # 
@login_required
@is_verified()
def detailSPK(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_detail_spk = get_object_or_404(SPK, id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            
           
        }
        template = 'profile/admin/spk/detail_spk.html'
        return render(request, template, context)
    
@login_required
@is_verified()
def createRincianSPK(request, id):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = SPKRINCIAN.objects.all()
        data_rincianspk = SPKRINCIAN.objects.all()
        context = {
            'title' : 'Data Rincian SPK',
            'form' : form,
            'data_rincianspk':data_rincianspk,
        }

        template = 'profile/admin/spk/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        uraianbelanja = request.POST.get('uraianbelanja')
        
       
        if uraianbelanja is not None:
            insert_SPKRINCIAN = SPKRINCIAN()
            insert_SPKRINCIAN.uraianbelanja = uraianbelanja
            insert_SPKRINCIAN.SPK_id = id

            insert_SPKRINCIAN.save()
            messages.success(request, 'Data spk berhasil disimpan.')
            return redirect('profile:admin_spk_detail',id)
        messages.error(request, 'Data spk gagal disimpan.')
        return render(request, 'profile/admin/spk/index.html', {'form': form,})
    
# insertsubrian
@login_required
@is_verified()
def insertsubrian(request, id):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = SPKSUBRINCIAN.objects.all()
        data_rincianspk = SPKSUBRINCIAN.objects.all()
        context = {
            'title' : 'Data Sub Rincian SPK',
            'form' : form,
            'data_rincianspk':data_rincianspk,
        }

        template = 'profile/admin/spk/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        spkrincian_id = request.POST.get('spk_idspkrincian_id')
        suburaianbelanja = request.POST.get('suburaianbelanja')
        qty = request.POST.get('qty')
        harga = request.POST.get('harga')
        satuan = request.POST.get('satuan')
        
       
        if spkrincian_id is not None:
            insert_SPKSUBRINCIAN = SPKSUBRINCIAN()
            insert_SPKSUBRINCIAN.SPKRINCIAN_id = spkrincian_id
            insert_SPKSUBRINCIAN.suburaianbelanja = suburaianbelanja
            insert_SPKSUBRINCIAN.qty = qty
            insert_SPKSUBRINCIAN.satuan = satuan
            insert_SPKSUBRINCIAN.harga = harga
            insert_SPKSUBRINCIAN.save()
            messages.success(request, 'Data spk berhasil disimpan.')
            return redirect('profile:admin_spk_detail',id)
        messages.error(request, 'Data spk gagal disimpan.')
        return render(request, 'profile/admin/spk/index.html', {'form': form,})
    
# CETAK DOKUMEN
@login_required
@is_verified()
def cetakSPK(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        data_detail_spk = get_object_or_404(SPK, id=id)
        data_rincian_spk = SPKRINCIAN.objects.filter(SPK_id=id)
        data_subrincian_spk = SPKSUBRINCIAN.objects.select_related('SPKRINCIAN').filter(SPKRINCIAN__SPK_id=id)
        context = {
            'title' : 'Detail SPK',
            'form' : form,
            'edit' : 'true',
            'data_spk' : data_detail_spk,
            'data_rincianSPK' : data_rincian_spk,
            'data_subrincian_spk' :data_subrincian_spk,
            
           
        }
        template = 'profile/admin/spk/detail_spk.html'
        return render(request, template, context)
    
 