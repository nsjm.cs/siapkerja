from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
   
    pajak_list = masterpajak.objects.filter(status='Aktif')
    archives = masterpajak.objects.filter(status='Tidak aktif')
   
    context = {
        'title' : 'Master Pajak',
        'pajak_list' : pajak_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/masterpajak/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = masterpajak.objects.all()
        data_pajak = masterpajak.objects.all()
        context = {
            'title' : 'Data Master Pajak',
            'form' : form,
            'data_pajak':data_pajak,
        }

        template = 'profile/admin/masterpajak/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        uraipajak = request.POST.get('uraipajak')
        persen = request.POST.get('persen')
        ket = request.POST.get('ket')
      

        if uraipajak is not None:
            insert_pajak = masterpajak()
            insert_pajak.uraipajak = uraipajak
            insert_pajak.persen = persen
            insert_pajak.ket = ket

            insert_pajak.save()
            messages.success(request, 'Data master pajak berhasil disimpan.')
            return redirect('profile:admin_pajak')
        messages.error(request, 'Data SKPD gagal disimpan.')
        return render(request, 'profile/admin/pajak/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_pajak = masterpajak.objects.get(id = id)

        context = {
            'title' : 'Ubah Data Master Pajak',
            'form' : form,
            'edit' : 'true',
            'data_pajak_' : dt_pajak,
        }
        template = 'profile/admin/masterpajak/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        insert_data_pajak = masterpajak.objects.get(id = id)
        uraipajak = request.POST.get('uraipajak')
        persen = request.POST.get('persen')
        ket = request.POST.get('ket')

        if insert_data_pajak is not None:
            
            insert_data_pajak.uraipajak = uraipajak
            insert_data_pajak.persen = persen
            insert_data_pajak.ket = ket
            insert_data_pajak.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_pajak')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/masterpajak/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = masterpajak.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except masterpajak.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = masterpajak.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except masterpajak.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)