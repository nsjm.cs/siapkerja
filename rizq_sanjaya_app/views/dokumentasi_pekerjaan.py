from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os, re
from django.db.models import Sum, F, Q, DecimalField, Value
from django.db.models.functions import Coalesce
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.forms.models import model_to_dict
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, get_total_nilai_spk, cek_ba_spk, bast_validation_exists
from rizq_sanjaya_app.support_function import PagingHelper

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):

    # doc_pekerjaan_list = DokumentasiPekerjaan.objects.filter(status='Aktif')
    doc_pekerjaan_list = DokumentasiPekerjaan.objects.select_related('SPK').filter(status='Aktif')
    archives = DokumentasiPekerjaan.objects.filter(status='Tidak aktif')
    
    SPK_list = SPK.objects.all()

    # Keperluan DatatableDokumentasiPekerjaan.objects.select_related('SPK').filter(Q(**kwargs_filter, _connector=Q.OR))
    page_obj = PagingHelper.dokumentasipekerjaan(request)
    # END Keperluan Datatable

    spk_ids_dokumentasi = doc_pekerjaan_list.values_list('SPK', flat=True)
    spk_belum_dokumentasi = SPK.objects.exclude(id__in=spk_ids_dokumentasi)

    regex = re.compile(r'^(\d{3})/')

    dok_no = f"{1:03d}"

    all_dok = DokumentasiPekerjaan.objects.all()
    if all_dok:
        nomor_urut_list = [
            int(regex.search(dok.nodoc).group(1)) 
            for dok in all_dok if regex.search(dok.nodoc)
        ]
        last_number =  max(nomor_urut_list) if nomor_urut_list else 0

        
        no_dok = last_number + 1
        dok_no = f"{no_dok:03d}"


    context = {
        'title' : 'Dokumentasi Pekerjaan',
        'page_obj' : page_obj,
        'SPK_list' : spk_belum_dokumentasi,
        
        'archives' : archives,
        'dok_no' : dok_no,
    }
    
    return render(request, 'profile/admin/dokumentasi/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = perusahaan.objects.all()
        data_perusahaan = perusahaan.objects.all()
        context = {
            'title' : 'Data Perusahaan',
            'form' : form,
            'data_perusahaan':data_perusahaan,
        }

        template = 'profile/admin/perusahaan/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
      
        nodoc = request.POST.get('nodoc')
        tgldoc = request.POST.get('tgldoc')
        uraidoc = request.POST.get('uraidoc')
        SPK_id = request.POST.get('SPK_id')
        uraibukti1 = request.POST.get('uraibukti1')
        uraibukti2 = request.POST.get('uraibukti2')
        uraibukti3 = request.POST.get('uraibukti3')
        uraibukti4 = request.POST.get('uraibukti4')
        uraibukti5 = request.POST.get('uraibukti5')
        uraibukti6 = request.POST.get('uraibukti6')
        bukti1 = request.FILES['bukti1']
        bukti2 = request.FILES['bukti2']
        bukti3 = request.FILES['bukti3']
        bukti4 = request.FILES['bukti4']
        bukti5 = request.FILES['bukti5']
        bukti6 = request.FILES['bukti6']
        
        nodoc = cek_nomor_terbaru(DokumentasiPekerjaan, 'nodoc', nomor_format = nodoc)
        
        if nodoc is not None:
            insert_dokumentasi_pekerjaan = DokumentasiPekerjaan()
            insert_dokumentasi_pekerjaan.nodoc = nodoc
            insert_dokumentasi_pekerjaan.tgldoc = tgldoc
            insert_dokumentasi_pekerjaan.uraidoc = uraidoc
            insert_dokumentasi_pekerjaan.SPK_id = SPK_id
            insert_dokumentasi_pekerjaan.uraibukti1 = uraibukti1
            insert_dokumentasi_pekerjaan.uraibukti2 = uraibukti2
            insert_dokumentasi_pekerjaan.uraibukti3 = uraibukti3
            insert_dokumentasi_pekerjaan.uraibukti4 = uraibukti4
            insert_dokumentasi_pekerjaan.uraibukti5 = uraibukti5
            insert_dokumentasi_pekerjaan.uraibukti6 = uraibukti6
            insert_dokumentasi_pekerjaan.bukti1 = bukti1
            insert_dokumentasi_pekerjaan.bukti2 = bukti2
            insert_dokumentasi_pekerjaan.bukti3 = bukti3
            insert_dokumentasi_pekerjaan.bukti4 = bukti4
            insert_dokumentasi_pekerjaan.bukti5 = bukti5
            insert_dokumentasi_pekerjaan.bukti6 = bukti6
            insert_dokumentasi_pekerjaan.save()
            messages.success(request, 'Data Dokumentasi Pekerjaan berhasil disimpan.')
            return redirect('profile:admin_dokumentasi_pekerjaan')
        messages.error(request, 'Data perusahaan gagal disimpan.')
        return render(request, 'profile/admin/dokumentasi/index.html', {'form': form,})

@login_required
@is_verified()
def printdoc(request, id):
    template = ''
    context = {} 
    
    if request.method == 'GET':
        dt_doc = DokumentasiPekerjaan.objects.filter(id=id)
        print(dt_doc)

        
        context = {
            'title' : 'Print Dokumentasi',
            'dt_doc' : dt_doc,
            'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    
        }
        pdf_format = {
            'format': 'A4',
            # 'width': "21cm",
            # "height": "21.97cm",
            'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
            'display_header_footer':True,
            'header_template':"<div></div>",
            'footer_template':"<div></div>",
        }

        pdf_file = generate_report('profile/admin/dokumentasi/print.html', context, pdf_format)

        return HttpResponse(pdf_file, content_type='application/pdf')

@login_required
@is_verified()
def getUrai(request, id):
    # Memastikan hanya POST request yang diproses
    if request.method == 'POST':
        data = {}
        try:
            # Mencoba mengambil data SPK berdasarkan ID
            data_rincian = SPK.objects.get(id=id)
            data_rincian_dict = model_to_dict(data_rincian)

            # Menyiapkan response data
            data['status'] = 'success'
            data['message'] = 'OK'
            data['data_rincian'] = data_rincian_dict  # Bisa diproses lebih lanjut jika perlu serialisasi

            return JsonResponse(data, status=200)
        
        except SPK.DoesNotExist:
            # Jika SPK dengan ID tidak ditemukan
            data['status'] = 'error'
            data['message'] = f'SPK dengan ID {id} tidak ditemukan.'
            return JsonResponse(data, status=404)

        except Exception as e:
            # Menangani kesalahan lain
            data['status'] = 'error'
            data['message'] = f'Terjadi kesalahan server: {str(e)}'
            return JsonResponse(data, status=500)

    # Jika method bukan POST, kembalikan error
    data = {'status': 'error', 'message': 'Method tidak diizinkan'}
    return JsonResponse(data, status=405)