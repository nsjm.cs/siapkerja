from warnings import catch_warnings
from django.shortcuts import render, HttpResponse, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from ..models import *
from django.contrib import messages
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from ..decorators import *
import os
from django.core.exceptions import PermissionDenied
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm
from django.core.mail import send_mail, BadHeaderError
from django.template.loader import render_to_string
from django.db.models.query_utils import Q
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode


# @require_http_methods(["GET"])
# def index(request):
#     user = User.objects.filter(is_staff = 'f').order_by('-created_at')
#     context = {
#         'title' : 'User',
#         'users' : user,
#     }
#     return render(request, 'profile/user/index.html', context)

@login_required
@is_verified()
@require_http_methods(["GET"])
@role_required(allowed_roles = ['admin'])
def admin_index(request):
    if request.user.is_staff:
        user = Account.objects.filter(is_staff='f').order_by('-date_joined')
    else:
        user = Account.objects.filter(role='posting').order_by('-date_joined')
    form = RoleForm()
    archives = Account.objects.filter(is_active='f').order_by('-date_joined')
    context = {
        'title' : 'User - Admin',
        'users' : user,
        'archives' : archives,
        'form' : form,
    }
    
    return render(request, 'profile/admin/user/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {}
    form_user = ''

    if request.method == 'GET':
        form_user = CustomUserCreationForm()
        skpd_list = OPD.objects.filter(status='Aktif', )
        context = {
            'title' : 'ADD Pengguna',
            'form_user' : form_user,
            'skpd_list' : skpd_list,
        }

        template = 'profile/admin/user/create.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        domain = '127.0.0.1:8000'
        skpd_list = OPD.objects.filter(status='Aktif', )
        try:
            domain = request.get_host()
        except:
            pass
        user_form = CustomUserCreationForm(data=request.POST)

        if user_form.is_valid() :
            try:
                pengguna = user_form.save(commit=False)
                pengguna.save()
                pengguna.is_verified = False
                pengguna.save()
                token = default_token_generator.make_token(pengguna)
                subject = "Verifikasi Email"
                message = get_template("profile/email/email_verification.html").render({
                    "email":pengguna.email,
                    'domain':domain,
                    'site_name': 'Website Merapi Trip',
                    "uid": urlsafe_base64_encode(force_bytes(pengguna.pk)),
                    "user": pengguna,
                    'token': token,
                    'protocol': 'http',
                })
                try:
                    # mail = EmailMessage(
                    #     subject=subject,
                    #     body=message,
                    #     from_email=settings.EMAIL_HOST_USER,
                    #     to=[
                    #         pengguna.email
                    #     ],
                    # )
                    
                    # mail.content_subtype = "html"
                    # mail.send()
                    pengguna.is_verified = True
                    pengguna.email_verification_token = token
                    pengguna.save()
                except BadHeaderError:
                    messages.error(request, 'Pengguna gagal ditambahkan. Email verifikasi gagal dikirim. Invalid header.')
                    return redirect('profile:admin_user',)
            except:
                messages.error(request, 'Pengguna gagal ditambahkan. Email verifikasi gagal dikirim.')
                return redirect('profile:admin_user',)

            messages.success(request, 'Pengguna berhasil ditambahkan.')
            return redirect('profile:admin_user',)
        messages.error(request, 'Pengguna gagal ditambahkan.')
        return render(request, 'profile/admin/user/create.html', {'form_user' : user_form, 'skpd_list': skpd_list })
    
@login_required
@is_verified()
def edit_avatar(request, username):
    form = ''
    if request.method == 'POST':
        try:
            if request.user.avatar: path_file_lama = f"{settings.MEDIA_ROOT}/{request.user.avatar}"
        except:
            pass
        account = Account.objects.get(username=username)
        avatar = request.FILES.get('avatar')
        is_valid = True
        if is_valid:
            try:
                if request.user.avatar: os.remove(path_file_lama)
            except:
                pass
            account.avatar = avatar
            account.save()
            messages.success(request, 'Avatar berhasil disimpan.')
            return redirect('profile:admin_user_detail', )

        messages.error(request, 'Avatar gagal disimpan.')
        return render(request, 'profile/admin/user/detail.html', {'form': form})

@login_required
@is_verified()
def edit_profile(request, username):
    form = ''
    if username != request.user.username:
        raise PermissionDenied

    if request.method == 'GET':
        pengguna = Account.objects.get(username=username)
        form = AccountForm(instance=pengguna)
        context = {
            'title' : 'EDIT Profile',
            'form' : form,
            'username' : username
        }
        template = 'profile/admin/user/edit_profile.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        pengguna = Account.objects.get(username=username)
        form = AccountForm(data=request.POST, instance=pengguna)
        if form.is_valid():
            form.save()

            messages.success(request, 'Account berhasil disimpan.')
            if(username == request.user.username):
                return redirect('profile:admin_user_detail')
            return redirect('profile:admin_user')

        messages.error(request, 'Account gagal disimpan.')
        return render(request, 'profile/admin/user/edit_profile.html', {'form': form, 'username' : username})

@login_required
def non_aktif(request, id):
    user = Account.objects.get(id = id)
    user.is_active = False
    user.save()
    return redirect('profile:admin_user')

@login_required
def re_aktif(request, id):
    user = Account.objects.get(id = id)
    user.is_active = True
    user.save()
    return redirect('profile:admin_user')

@login_required
@is_verified()
def edit_role(request):
    if request.method == 'POST':
        try:
            pengguna = Account.objects.get(username=request.POST.get('username'))
            foto = request.FILES.get('foto')
            email = request.POST.get('email')
            first = request.POST.get('first')
            last = request.POST.get('last')
            phone = request.POST.get('phone')
            role = request.POST.get('role')
            print(foto)
            is_valid = True
            if is_valid:
                pengguna = Account.objects.get(username=request.POST.get('username'))
                pengguna.avatar = foto
                pengguna.email = email
                pengguna.first_name = first
                pengguna.last_name = last
                pengguna.phone = phone
                pengguna.role = role
                pengguna.save()

                messages.success(request, 'Data berhasil diganti.')
                return redirect('profile:admin_user')
            messages.error(request, 'Role gagal diganti')
            return redirect('profile:admin_user')
        except:
            messages.error(request, 'Role gagal diganti.')
            return redirect('profile:admin_user')

@login_required
@is_verified()
def edit_password(request):
    form = ''

    if request.method == 'GET':
        # form = PasswordForm()
        form = PasswordChangeForm(user=request.user)
        context = {
            'title' : 'EDIT Password',
            'form' : form,
        }
        return render(request, 'profile/admin/user/edit_password.html', context)
    
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)

            messages.success(request, 'Password berhasil disimpan.')
            return redirect('profile:admin_user_detail')

        else: 
            messages.error(request, 'Password gagal diperbarui.')
            return render(request, 'profile/admin/user/edit_password.html', {'form': form})

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_detail(request):
    template = 'profile/admin/user/detail.html'
    form = AvatarForm()
    # news = News.objects.filter(created_by_id=request.user.id, deleted_at=None).count()
    # news_total = News.objects.filter(deleted_at=None).count()
    # document = Document.objects.filter(created_by_id=request.user.id, deleted_at=None).count()
    # document_total = Document.objects.filter(deleted_at=None).count()
    context = {
        'title' : 'PROFILE',
        'form' : form,
        # 'news' : news,
        # 'news_total' : news_total,
        # 'document' : document,
        # 'document_total' : document_total,
        'labels' : ['chrome', 'firefox', 'edge', 'opera'],
        'data1' : [10, 5, 8, 2],
    }
    return render(request, template, context)

@login_required
@is_verified()
@role_required(allowed_roles = ['admin'])
def softDelete(request, username):
    message = ''
    try:
        user = Account.objects.get(username=username)
        user.is_active = False
        user.save()
        message = 'success'
    except Account.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
@role_required(allowed_roles = ['admin'])
def permanentDelete(request, slug):
    message = ''
    try:
        user = Account.objects.get(slug=slug)
        user.delete()
        message = 'success'
    except Account.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
    
@login_required
@is_verified()
@role_required(allowed_roles = ['admin'])
def restore(request, username):
    message = ''
    try:
        user = Account.objects.get(username=username)
        user.is_active = True
        user.save()
        message = 'success'
    except Account.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

def password_reset_request(request):
    domain = '127.0.0.1:8000'
    try:
        domain = request.get_host()
    except:
        pass
	
    if request.method == "POST":
        password_reset_form = PasswordResetForm(request.POST)
        if password_reset_form.is_valid():
            data = password_reset_form.cleaned_data['email']
            associated_users = Account.objects.filter(Q(email=data))
            if associated_users.exists():
                for user in associated_users:
                    subject = "Permintaan Reset Password"
                    message = get_template("profile/email/password_reset.html").render({
                        "email":user.email,
                        'domain':domain,
                        'site_name': 'Website Merapi Trip',
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "user": user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                        })
                    # try:
                    #     mail = EmailMessage(
                    #         subject=subject,
                    #         body=message,
                    #         from_email=settings.EMAIL_HOST_USER,
                    #         to=[
                    #             user.email
                    #         ],
                    #     )
                        
                    #     mail.content_subtype = "html"
                    #     mail.send()
                    # except BadHeaderError:
                    #     return HttpResponse('Invalid header found.')
                    return redirect ("profile:password_reset_done")
    password_reset_form = PasswordResetForm()
    return render(request, "profile/admin/user/password/password_reset.html", {"password_reset_form":password_reset_form})

@login_required
def verification(request):
    if request.user.is_verified:
        return redirect('profile:admin_home')
    form = ''

    if request.method == 'GET':
        context = {
            'title' : 'Verifikasi Email Anda',
            'form' : form,
        }
        return render(request, 'profile/admin/user/verifikasi_email.html', context)


@login_required
def send_verification(request):
    if request.user.is_verified:
        return redirect('profile:admin_home')
    subject = "Verifikasi Email"
    domain = '127.0.0.1:8000'
    try:
        domain = request.get_host()
    except:
        pass
    token = default_token_generator.make_token(request.user)
    pengguna = Account.objects.get(id=request.user.id)
    pengguna.email_verification_token = token
    pengguna.save()
    message = get_template("profile/email/email_verification.html").render({
        "email":request.user.email,
        'domain':domain,
        'site_name': 'Website Merapi Trip',
        "uid": urlsafe_base64_encode(force_bytes(request.user.pk)),
        "user": request.user,
        'token': token,
        'protocol': 'http',
    })
    # try:
    #     mail = EmailMessage(
    #         subject=subject,
    #         body=message,
    #         from_email=settings.EMAIL_HOST_USER,
    #         to=[
    #             request.user.email
    #         ],
    #     )
        
    #     mail.content_subtype = "html"
    #     mail.send()
    # except BadHeaderError:
    #     return HttpResponse('Invalid header found.')
    context = {
        'title' : 'Verifikasi Email Anda',
        'pesan' : 'terkirim',
    }
    return render(request, 'profile/admin/user/verifikasi_email.html', context)

def email_verify(request, uidb64, token):
    try:
        if request.user.is_verified: return redirect('profile:admin_home')
    except:
        pass

    status = ''
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None
    if user is not None and user.email_verification_token == token:
        user.is_verified = True
        user.save()
        status = 'sukses'
    
    context = {
        'title' : 'Verifikasi Email',
        'status' : status,
    }
    return render(request, 'profile/admin/user/verifikasi_email_complete.html', context)