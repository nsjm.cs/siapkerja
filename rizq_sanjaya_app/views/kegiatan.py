from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.urls import reverse
from django.contrib import messages
from ..models import OPD as m_skpd, MasterKegiatan as m_kegiatan
from django.utils import timezone
import os, re, traceback, json
from django.db import transaction
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.views import View

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest

from collections import defaultdict
from django.utils.formats import number_format
from rizq_sanjaya_app.context_processors import global_variables

# For Excel Operation
import io
from io import BytesIO
from openpyxl import load_workbook
from xlsxwriter.workbook import Workbook

@login_required
@is_verified()
# @require_http_methods(["GET"])
def indexKegiatan(request):
    if request.method == 'GET':
        # page = request.GET.get('page', 1)
        # kegiatan_list = MasterKegiatan.objects.filter(deleted_at=None).order_by('-created_at')
        # archives = MasterKegiatan.objects.filter(deleted_at__isnull=False)
        # paginator = Paginator(kegiatan_list, 15)

        # try:
        #     kegiatan_list = paginator.page(page)
        # except PageNotAnInteger:
        #     kegiatan_list = paginator.page(1)
        # except EmptyPage:
        #     kegiatan_list = paginator.page(paginator.num_pages)

        try:
            with transaction.atomic():
                m_kegiatan.objects.rebuild()
        except Exception as e:
            pass

        context = {
            'title' : 'Master Kegiatan',
            # 'kegiatan_list' : kegiatan_list,
            # 'archives' : archives,
        }
        
        return render(request, 'profile/admin/master_kegiatan/index.html', context)
    elif request.method == 'POST':
        data = {
            "array_kegiatan": []
        }

        table_format = """
                        <table class="table table-rounded table-row-dashed border table-flush" width="100%">
                            <thead style="background-color: #015699 !important;">
                                <tr class="fw-bold fs-6 text-white py-4">
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start" width="60%">URAIAN SSH</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Harga Satuan</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Volume</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Satuan</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody class="" id="table_body">
                                {replace_tr}
                            </tbody>
                        </table>
                    """

        data_skpd = m_kegiatan.objects.all().values('skpd_id').order_by('skpd').distinct('skpd')
        array_kegiatan = []
        
        # for skpd in data_skpd:
        #     dt_skpd = m_skpd.objects.get(skpd_id = skpd['skpd_id'])
        #     programs = m_kegiatan.objects.filter(skpd = dt_skpd, is_program = True)
        #     skpd_entry = {"text": f"<b>{dt_skpd.skpd_kode} - {dt_skpd.skpd_nama}</b>", "kode":dt_skpd.skpd_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
        #     for program in programs:
        #         program_entry = {"text": f"<b>{program.kegiatan_kode} - {program.kegiatan_nama}</b>", "kode":program.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
        #         for kegiatan in program.get_descendants().filter(is_kegiatan = True):
        #             kegiatan_entry = {"text": f"<b>{kegiatan.kegiatan_kode} - {kegiatan.kegiatan_nama}</b>", "kode":kegiatan.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
        #             for subkegiatan in kegiatan.get_descendants().filter(is_subkegiatan = True):
        #                 subkegiatan_entry = {"text": f"<b>{subkegiatan.kegiatan_kode} - {subkegiatan.kegiatan_nama}</b>", "kode":subkegiatan.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
        #                 for rekening_belanja in subkegiatan.get_descendants().filter(is_rekening_belanja = True):
        #                     rekening_belanja_entry = {"text": f"<b>{rekening_belanja.kegiatan_kode} - {rekening_belanja.kegiatan_nama}</b>", "kode":rekening_belanja.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
        #                     for keterangan in rekening_belanja.get_descendants().filter(is_subrekening_belanja = True):

        #                         tr_add = ''
        #                         penerimas = keterangan.get_descendants().filter(is_ssh = True)

        #                         for x in penerimas:
        #                             tr_add += f"""
        #                                         <tr>
        #                                             <td class="align-middle p-2">{x.kabupaten}</td>
        #                                             <td class="align-middle p-2">{x.distrik}</td>
        #                                             <td class="align-middle p-2">{x.kampung}</td>
        #                                             <td class="align-middle p-2">{x.alamat}</td>
        #                                             <td class="align-middle p-2">{x.nama_penerima}</td>
        #                                             <td class="align-middle p-2 text-center">{x.volume}</td>
        #                                             <td class="align-middle p-2">{x.satuan}</td>
        #                                             <td class="p-2 text-end align-middle">Rp {number_format(x.nilai, decimal_pos = 2, use_l10n = True)}</td>
        #                                         </tr>
        #                                     """

        #                         rendered_table = re.sub(r"\s+", " ", table_format.format(replace_tr = tr_add)).strip()

        #                         keterangan_entry = {"text": f"<b># {keterangan.kegiatan_nama}</b>", "kode":keterangan.kegiatan_nama, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "penerima":[], "children": [{"text": rendered_table, "icon": False, "state": {"opened": True, "disabled": True}, "a_attr": {"class": "height-100 wrap"} }]}
        #                         rekening_belanja_entry['children'].append(keterangan_entry)
        #                     subkegiatan_entry['children'].append(rekening_belanja_entry)
        #                 kegiatan_entry["children"].append(subkegiatan_entry)
        #             program_entry["children"].append(kegiatan_entry)
        #         skpd_entry["children"].append(program_entry)

        #     data['array_kegiatan'].append(skpd_entry)

        for skpd in data_skpd:
            dt_skpd = m_skpd.objects.get(id = skpd['skpd_id'])
            programs = m_kegiatan.objects.filter(skpd = dt_skpd, is_program = True)
            skpd_entry = {"text": f"<b>{dt_skpd.kodeopd} - {dt_skpd.namaopd}</b>", "kode":dt_skpd.kodeopd, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
            for program in programs:
                program_entry = {"text": f"<b>{program.kegiatan_kode} - {program.kegiatan_nama}</b>", "kode":program.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                for kegiatan in program.get_descendants().filter(is_kegiatan = True):
                    kegiatan_entry = {"text": f"<b>{kegiatan.kegiatan_kode} - {kegiatan.kegiatan_nama}</b>", "kode":kegiatan.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                    for subkegiatan in kegiatan.get_descendants().filter(is_subkegiatan = True):
                        subkegiatan_entry = {"text": f"<b>{subkegiatan.kegiatan_kode} - {subkegiatan.kegiatan_nama}</b>", "kode":subkegiatan.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                        for rekening_belanja in subkegiatan.get_descendants().filter(is_rekening_belanja = True):
                            rekening_belanja_entry = {"text": f"<b>{rekening_belanja.kegiatan_kode} - {rekening_belanja.kegiatan_nama}</b>", "kode":rekening_belanja.kegiatan_kode, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}

                            for sumberdana in rekening_belanja.get_descendants().filter(is_sumberdana = True):
                                sumberdana_entry = {"text": f"<b>{sumberdana.kegiatan_nama}</b>", "kode":sumberdana.kegiatan_nama, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                for bagian in sumberdana.get_descendants().filter(is_bagian = True):
                                    bagian_entry = {"text": f"<b>{bagian.kegiatan_nama}</b>", "kode":bagian.kegiatan_nama, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                    for keterangan in bagian.get_descendants().filter(is_subrekening_belanja = True):
                                        tr_add = ''
                                        
                                        ssh = keterangan.get_descendants().filter(is_ssh = True)

                                        for x in ssh:
                                            tr_add += f"""
                                                        <tr>
                                                            <td class="align-middle p-2">{x.uraian_ssh}</td>
                                                            <td class="text-right p-2">Rp. {number_format(x.nilai)}</td>
                                                            <td class="text-center p-2">{x.volume}</td>
                                                            <td class="text-center p-2">{x.satuan}</td>
                                                            <td class="text-right p-2">Rp. {number_format(x.volume * x.nilai, decimal_pos = 2, use_l10n = True)}</td>
                                                        </tr>
                                                    """

                                            rendered_table = re.sub(r"\s+", " ", table_format.format(replace_tr = tr_add)).strip()

                                            keterangan_entry = {"text": f"<b># {keterangan.kegiatan_nama}</b>", "kode":keterangan.kegiatan_nama, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "ssh":[], "children": [{"text": rendered_table, "icon": False, "state": {"opened": True, "disabled": True}, "a_attr": {"class": "height-100 wrap"} }]}
                                        bagian_entry['children'].append(keterangan_entry)
                                    sumberdana_entry['children'].append(bagian_entry)
                                rekening_belanja_entry['children'].append(sumberdana_entry)
                            subkegiatan_entry['children'].append(rekening_belanja_entry)
                        kegiatan_entry["children"].append(subkegiatan_entry)
                    program_entry["children"].append(kegiatan_entry)
                skpd_entry["children"].append(program_entry)
            data['array_kegiatan'].append(skpd_entry)

        return JsonResponse(data)

def input_kegiatan(kwargs):
    data_kegiatan = m_kegiatan(**kwargs)
    # print('kegiatan baru', data_kegiatan, data_kegiatan.is_program, data_kegiatan.is_kegiatan, data_kegiatan.is_subkegiatan, data_kegiatan.is_rekening_belanja, data_kegiatan.is_subrekening_belanja, data_kegiatan.is_ssh)
    return data_kegiatan.save()

@login_required
@is_verified()
def create(request):
    if request.method == 'GET':
        skpd = OPD.objects.filter(status='Aktif')
        tahapan = MasterTahapan.objects.get(status=True)

        context = {
            'title' : 'Tambah Master Kegiatan',
            'skpd' : skpd,
            'tahapan' : tahapan,
        }
        
        return render(request, 'profile/admin/master_kegiatan/form.html', context)


    if request.method == 'POST':
        data = {
            'status': 'error',
            'message': 'Terjadi kesalahan server.'
        }

        frm_tahapan = request.POST.get('frm_tahapan')
        data_kegiatan = json.loads(request.POST.get('data_kegiatan', []))

        kwargs_input = {}

        try:
            with transaction.atomic():
                for skpd in data_kegiatan:
                    skpd_ = skpd['kode'].split(' ')
                    kode_skpd = skpd_[0]
                    nama_skpd = re.sub(r'[^a-zA-Z\s]', '', ' '.join(skpd_[1:len(skpd_)])).strip()
                    print(f'{kode_skpd}-{nama_skpd}-')
                    dt_skpd = m_skpd.objects.get(kodeopd = kode_skpd, namaopd = nama_skpd)

                    for program in skpd['children']:
                        program_ = program['kode'].split(' ')
                        kode_program = program_[0]
                        nama_program = ' '.join(program_[1:len(program_)])

                        kwargs_input = {
                             "parent": None,
                             "kegiatan_kode": kode_program.strip(),
                             "kegiatan_nama": nama_program.strip(),
                             "is_program": True,
                             "tahapan": global_variables(request)['tahapan_aktif'],
                             "skpd": dt_skpd,
                        }
                        dt_program = input_kegiatan(kwargs_input)
                        print('Program', dt_program)
                        for kegiatan in program['children']:
                            kegiatan_ = kegiatan['kode'].split(' ')
                            kode_kegiatan = kegiatan_[0]
                            nama_kegiatan = ' '.join(kegiatan_[1:len(kegiatan_)])
                            kwargs_input = {
                                 "parent": dt_program,
                                 "kegiatan_kode": kode_kegiatan.strip(),
                                 "kegiatan_nama": nama_kegiatan.strip(),
                                 "is_kegiatan": True,
                                 "tahapan": dt_program.tahapan,
                                 "skpd": dt_program.skpd,
                            }
                            dt_kegiatan = input_kegiatan(kwargs_input)

                            for subkegiatan in kegiatan['children']:
                                subkegiatan_ = subkegiatan['kode'].split(' ')
                                kode_subkegiatan = subkegiatan_[0]
                                nama_subkegiatan = ' '.join(subkegiatan_[1:len(subkegiatan_)])
                                kwargs_input = {
                                     "parent": dt_kegiatan,
                                     "kegiatan_kode": kode_subkegiatan.strip(),
                                     "kegiatan_nama": nama_subkegiatan.strip(),
                                     "is_subkegiatan": True,
                                     "tahapan": dt_program.tahapan,
                                     "skpd": dt_program.skpd,
                                }
                                dt_subkegiatan = input_kegiatan(kwargs_input)

                                for rekening in subkegiatan['children']:
                                    rekening_ = rekening['kode'].split(' ')
                                    kode_rekening = rekening_[0]
                                    nama_rekening = ' '.join(rekening_[1:len(rekening_)])
                                    kwargs_input = {
                                         "parent": dt_subkegiatan,
                                         "kegiatan_kode": kode_rekening.strip(),
                                         "kegiatan_nama": nama_rekening.strip(),
                                         "is_rekening_belanja": True,
                                         "tahapan": dt_program.tahapan,
                                         "skpd": dt_program.skpd,
                                    }
                                    dt_rekening = input_kegiatan(kwargs_input)

                                    for sumberdana in rekening['children']:
                                        nama_sumberdana = sumberdana['kode']
                                        kwargs_input = {
                                            "parent": dt_rekening,
                                            "kegiatan_kode": None,
                                            "kegiatan_nama": nama_sumberdana.strip(),
                                            "is_sumberdana": True,
                                            "tahapan": dt_program.tahapan,
                                            "skpd": dt_program.skpd,
                                        }
                                        dt_sumberdana = input_kegiatan(kwargs_input)

                                        for bagian in sumberdana['children']:
                                            nama_bagian = bagian['kode']
                                            kwargs_input = {
                                                 "parent": dt_sumberdana,
                                                 "kegiatan_kode": None,
                                                 "kegiatan_nama": nama_bagian.strip(),
                                                 "is_bagian": True,
                                                 "tahapan": dt_program.tahapan,
                                                 "skpd": dt_program.skpd,
                                            }
                                            dt_bagian = input_kegiatan(kwargs_input)
                                            print('Bagian', dt_bagian)

                                            for keterangan in bagian['children']:
                                                nama_keterangan = keterangan['kode']
                                                kwargs_input = {
                                                     "parent": dt_bagian,
                                                     "kegiatan_kode": None,
                                                     "kegiatan_nama": nama_keterangan.strip(),
                                                     "is_subrekening_belanja": True,
                                                     "tahapan": dt_program.tahapan,
                                                     "skpd": dt_program.skpd,
                                                }
                                                dt_keterangan = input_kegiatan(kwargs_input)
                                                
                                                for ssh in keterangan['ssh']:
                                                    uraianssh = ssh['uraianssh']
                                                    volume = ssh['volume']
                                                    satuan = ssh['satuan']
                                                    hargasatuan = ssh['hargasatuan']

                                                    kwargs_input = {
                                                         "parent": dt_keterangan,
                                                         "kegiatan_kode": None,
                                                         "kegiatan_nama": None,
                                                         "uraian_ssh": uraianssh.strip(),
                                                         "volume": volume,
                                                         "satuan": satuan.strip(),
                                                         "nilai": hargasatuan,
                                                         "subtotal": hargasatuan * volume,
                                                         "is_ssh": True,
                                                         "tahapan": dt_program.tahapan,
                                                         "skpd": dt_program.skpd,
                                                    }
                                                    dt_penerima = input_kegiatan(kwargs_input)
                                                
                    #                             print('Keterangan', nama_keterangan)
                    #                         print('bagian', nama_bagian)
                    #                     print('sumberdana', nama_sumberdana)
                    #                 print('rekening', kode_rekening, nama_rekening)
                    #             print('subkegiatan', kode_subkegiatan, nama_subkegiatan)
                    #         print('kegiatan', kode_kegiatan, nama_kegiatan)
                    #     print('program', kode_program, nama_program)
                    # print('skpd', kode_skpd, nama_skpd)

            data['status'] = 'success'
            data['message'] = 'Input Kegiatan berhasil.'

            try:
              with transaction.atomic():
                  m_kegiatan.objects.rebuild()
            except Exception as e:
                pass

            return JsonResponse(data, status = 201)
        except Exception as e:
            traceback.print_exc()
            print('error input kegiatan', e)
            data['message'] = f'Input Kegiatan gagal. {e}'
        return JsonResponse(data, status = 400)
        # id_tahapan = request.POST.get('id_tahapan')
        # tahun = request.POST.get('tahun')
        # skpd = request.POST.get('skpd')
        # bagian = request.POST.get('bagian')
        # program = request.POST.get('program')
        # kegiatan = request.POST.get('kegiatan')
        # subkegiatan = request.POST.get('subkegiatan')
        # print(id_tahapan)

        # tahapan = MasterTahapan.objects.get(id_tahapan = id_tahapan)
        # tahapan_id =  tahapan.id_tahapan
        # id_tahun = tahapan.tahun.id
        # try:

        #     with transaction.atomic():
        #         insert_kegiatan = MasterKegiatan()
        #         insert_kegiatan.tahun_id = id_tahun
        #         insert_kegiatan.tahapan_id = tahapan_id
        #         insert_kegiatan.bagian = bagian
        #         insert_kegiatan.skpd_id = skpd
        #         insert_kegiatan.program = program
        #         insert_kegiatan.kegiatan = kegiatan
        #         insert_kegiatan.sub_kegiatan = subkegiatan
        #         insert_kegiatan.save()

        #     messages.success(request, f'Data kegiatan {insert_kegiatan.kegiatan} berhasil disimpan.')
        #     return redirect('profile:index_kegiatan')
                
        # except Exception as e:
        #     print('Error akun', e)
        #     messages.error(request, 'Data gagal disimpan.')
        #     return redirect('profile:admin_kegiatan_create')


@login_required
@is_verified()
def edit(request, id_kegiatan):
    if request.method == 'GET':
        skpd = OPD.objects.filter(status='Aktif')
        tahapan = MasterTahapan.objects.get(status=True)
        kegiatan = MasterKegiatan.objects.get(id_kegiatan = id_kegiatan)

        context = {
            'title' : 'Tambah Master Kegiatan',
            'skpd' : skpd,
            'tahapan' : tahapan,
            'kegiatan' : kegiatan,
            'edit' : True,
        }
        
        return render(request, 'profile/admin/master_kegiatan/form.html', context)

    if request.method == 'POST':
        id_tahapan = request.POST.get('id_tahapan')
        tahun = request.POST.get('tahun')
        skpd = request.POST.get('skpd')
        bagian = request.POST.get('bagian')
        program = request.POST.get('program')
        kegiatan = request.POST.get('kegiatan')
        subkegiatan = request.POST.get('subkegiatan')
        print(id_tahapan)

        tahapan = MasterTahapan.objects.get(id_tahapan = id_tahapan)
        tahapan_id =  tahapan.id_tahapan
        id_tahun = tahapan.tahun.id
        try:


            with transaction.atomic():
                insert_kegiatan = MasterKegiatan.objects.get(id_kegiatan = id_kegiatan)
                insert_kegiatan.tahun_id = id_tahun
                insert_kegiatan.tahapan_id = tahapan_id
                insert_kegiatan.skpd_id = skpd
                insert_kegiatan.bagian = bagian
                insert_kegiatan.program = program
                insert_kegiatan.kegiatan = kegiatan
                insert_kegiatan.sub_kegiatan = subkegiatan
                insert_kegiatan.save()

            messages.success(request, f'Data kegiatan {insert_kegiatan.kegiatan} berhasil disimpan.')
            return redirect('profile:index_kegiatan')
                
        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal diubah.')
            return redirect('profile:index_tahapan')



@login_required
@is_verified()
def softDelete(request, id_kegiatan):
    message = ''
    try:
        sekarang = timezone.now()
        doc = MasterKegiatan.objects.get(id_kegiatan=id_kegiatan)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except MasterKegiatan.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restoreKegiatan(request, id_kegiatan):
    message = ''
    try:
        doc = MasterKegiatan.objects.get(id_kegiatan=id_kegiatan)
        print(doc, 'eshafoiiesiofaeios')
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except MasterKegiatan.DoesNotExist:
        print(doc, 'eshafoiiesiofaeios')
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

# @login_required
# @is_verified()
def formated_kode_kegiatan(text):
    print(text)
    match = re.match(r'^([\d.]+)\s*(.*)', text)
    if match is not None:
        text2 = re.sub(r'^[^a-zA-Z]+', '', match.group(2).strip())
        return f"{match.group(1)} {text2}".strip()
    else:
        return text

@method_decorator(login_required(), name='dispatch')
class ParseKegiatanExcelViews(View):
    def post(self, request):
        excel = request.FILES.get('frm_file_excel')
        pesan = ''
        # row_error = 0
        additional_error = ''
        array_kegiatan = []
        data_gagal = []
        data_response = []

        total_anggaran = 0

        header_keys = ['NO',
        'SKPD',
        'UNIT SKPD',
        'PROGRAM',
        'KEGIATAN',
        'SUB KEGIATAN',
        'SUMBERDANA',
        'BAGIAN',
        'REKENING BELANJA',
        'URAIANSSH',
        'HARGASATUAN',
        'SATUAN',
        'JUMLAH',
        'SUBTOTAL',
        'PAGU PER REKENING BELANJA',
        'PAGU SUB KEGIATAN APBD',
        'TAHAPAN',
        'KETERANGAN']

        data = {
            'pesan':'Import data gagal.', 
            'data_gagal': 0,
        }



        data['data_response'] = data_response
        
        if excel:
            try:
                with transaction.atomic():
                    wb = load_workbook(filename=BytesIO(excel.read()))
                    sheet_anggota = wb['Sheet1']

                    for cell in sheet_anggota[1]:
                        kolom = cell.value
                        if kolom is not None:
                            if kolom not in header_keys:
                                raise Exception(f'Header <b>{kolom.upper()}</b> tidak ada dalam keys, mohon tidak merubah header excel template.')

                    skpd_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))))))

                    for row in sheet_anggota.iter_rows(min_row=2):
                        kolom_NO = row[0].value
                        kolom_SKPD = formated_kode_kegiatan(row[1].value.strip())
                        kolom_UNITSKPD = formated_kode_kegiatan(row[2].value.strip())
                        kolom_PROGRAM = formated_kode_kegiatan(row[3].value.strip())
                        kolom_KEGIATAN = formated_kode_kegiatan(row[4].value.strip())
                        kolom_SUBKEGIATAN = formated_kode_kegiatan(row[5].value.strip())
                        kolom_SUMBERDANA = row[6].value.strip()
                        kolom_BAGIAN = row[7].value.strip()
                        kolom_REKENING_BELANJA = formated_kode_kegiatan(row[8].value.strip())
                        kolom_URAIANSSH = row[9].value.strip()
                        kolom_HARGASATUAN = row[10].value
                        kolom_SATUAN = row[11].value.strip()
                        kolom_JUMLAH = row[12].value
                        kolom_KETERANGAN = '' if row[17].value is None else row[17].value.strip()
                        # kolom_TAHUN = str(row[1].value).strip()

                        # print(kolom_NO, kolom_SKPD, kolom_UNITSKPD, kolom_PROGRAM, kolom_KEGIATAN, kolom_SUBKEGIATAN, kolom_SUMBERDANA, kolom_BAGIAN, kolom_REKENING_BELANJA, kolom_URAIANSSH, kolom_HARGASATUAN, kolom_SATUAN, kolom_JUMLAH, kolom_KETERANGAN)

                        # print('regroup', )

                        try:
                            dt_skpd = m_skpd.objects.get(kodeopd = kolom_UNITSKPD.split(' ')[0])
                        except Exception as e:
                            print('Error skpd', e)
                            row_error = row[2].row
                            additional_error = f'SKPD <b>{kolom_UNITSKPD}</b> tidak ada dalam database, silahkan sesuaikan berdasarkan <b>Master SKPD</b>'
                            raise Exception('SKPD tidak ditemukan.')

                        try:
                            total_anggaran += float(kolom_HARGASATUAN) * float(kolom_JUMLAH)
                        except Exception as e:
                            row_error = row[16].row
                            additional_error = f'Pastikan <b>Harga Satuan</b> dan <b>Jumlah</b> adalah angka dan tidak ada spasi didalamnya.'
                            raise Exception('Total Error.')
                        
                        # match = re.match(r'^(\d+)\s*(.*)', kolom_SATUAN)

                        ssh = {
                                "uraianssh": kolom_URAIANSSH,
                                "hargasatuan": kolom_HARGASATUAN,
                                "volume": kolom_JUMLAH,
                                "satuan": kolom_SATUAN,
                            }

                        skpd_dict[kolom_UNITSKPD][kolom_PROGRAM][kolom_KEGIATAN][kolom_SUBKEGIATAN][kolom_REKENING_BELANJA][kolom_SUMBERDANA][kolom_BAGIAN][kolom_KETERANGAN].append(ssh)

                    table_format = """
                        <table class="table table-rounded table-row-dashed border table-flush" width="100%">
                            <thead style="background-color: #015699 !important;">
                                <tr class="fw-bold fs-6 text-white py-4">
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start" width="60%">URAIAN SSH</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Harga Satuan</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Volume</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Satuan</th>
                                    <th class="p-2 text-center text-white border-end border-top border-bottom border-start">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody class="" id="table_body">
                                {replace_tr}
                            </tbody>
                        </table>
                    """

                    for skpd, programs in skpd_dict.items():
                        skpd_entry = {"text": f"<b>{skpd}</b>", "kode":skpd, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                        for program, kegiatans in programs.items():
                            program_entry = {"text": f"<b>{program}</b>", "kode":program, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                            for kegiatan, subkegiatans in kegiatans.items():
                                kegiatan_entry = {"text": f"<b>{kegiatan}</b>", "kode":kegiatan, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                for subkegiatan, rekening_belanjas in subkegiatans.items():
                                    subkegiatan_entry = {"text": f"<b>{subkegiatan}</b>", "kode":subkegiatan, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                    for rekening_belanja, sumberdanas in rekening_belanjas.items():
                                        rekening_belanja_entry = {"text": f"<b>{rekening_belanja}</b>", "kode":rekening_belanja, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                        for sumberdana, bagians in sumberdanas.items():
                                            sumberdana_entry = {"text": f"<b>{sumberdana}</b>", "kode":sumberdana, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                            for bagian, keterangans in bagians.items():
                                                bagian_entry = {"text": f"<b>{bagian}</b>", "kode":bagian, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "children": []}
                                                for keterangan, ssh in keterangans.items():
                                                    tr_add = ''

                                                    for x in ssh:
                                                        tr_add += f"""
                                                                    <tr>
                                                                        <td class="align-middle p-2">{x['uraianssh']}</td>
                                                                        <td class="text-right p-2">Rp. {number_format(x['hargasatuan'])}</td>
                                                                        <td class="text-center p-2">{x['volume']}</td>
                                                                        <td class="text-center p-2">{x['satuan']}</td>
                                                                        <td class="text-right p-2">Rp. {number_format(x['volume'] * x['hargasatuan'], decimal_pos = 2, use_l10n = True)}</td>
                                                                    </tr>
                                                                """

                                                        rendered_table = table_format.format(replace_tr = tr_add)

                                                        keterangan_entry = {"text": f"<b># {keterangan}</b>", "kode":keterangan, "icon": "ki-outline ki-folder", "state" : {"opened": True, "disabled"  : False}, "ssh":ssh, "children": [{"text": rendered_table, "icon": False, "state": {"opened": True, "disabled": True}, "a_attr": {"class": "height-100 wrap"} }]}
                                                    bagian_entry['children'].append(keterangan_entry)
                                                sumberdana_entry['children'].append(bagian_entry)
                                            rekening_belanja_entry['children'].append(sumberdana_entry)
                                        subkegiatan_entry['children'].append(rekening_belanja_entry)
                                    kegiatan_entry["children"].append(subkegiatan_entry)
                                program_entry["children"].append(kegiatan_entry)
                            skpd_entry["children"].append(program_entry)

                        array_kegiatan.append(skpd_entry)

                    # pprint.pprint(array_kegiatan)
                    # additional_error = f'{additional_error} Kode mata uang <b>{frm_kode_mata_uang}</b> tidak ada dalam database, silahkan sesuaikan berdasarkan sheet <b>Referensi Mata Uang</b>'
        #             raise Exception('Kode mata uang tidak cocok.')

                    data['data_response'] = array_kegiatan
                    data['total_anggaran'] = total_anggaran
                return JsonResponse(data, status = 201)            

            except Exception as e:
                traceback.print_exc()
                print('Error insert row import anggota', e)
                try:
                    data['pesan'] = f'Import Gagal Excel Baris Ke {row_error},<br> {additional_error},  {e}'
                except Exception as err:
                    data['pesan'] = f'{e}<br>{additional_error}'
                return JsonResponse(data, status = 400)

        return JsonResponse(data, status = 400)