from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    SKPD_list = OPD.objects.filter(status='Aktif')
    wilayah_list = lokasi.objects.filter(status='Aktif')
    archives = OPD.objects.filter(status='Tidak aktif')
    paginator = Paginator(SKPD_list, 15)
    tahun = Tahun.objects.filter(deleted_at=None)
    try:
        SKPD_list = paginator.page(page)
    except PageNotAnInteger:
        SKPD_list = paginator.page(1)
    except EmptyPage:
        SKPD_list = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master SKPD',
        'SKPD_list' : SKPD_list,
        'wilayah_list' :wilayah_list,
        'tahun' :tahun,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/opd/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = OPD.objects.all()
        data_opd = OPD.objects.all()
        context = {
            'title' : 'Data SKPD',
            'form' : form,
            'data_opd':data_opd,
        }

        template = 'profile/admin/opd/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        tahun = request.POST.get('tahun')
        kodeopd = request.POST.get('kodeopd')
        namaopd = request.POST.get('namaopd')
        alamatopd = request.POST.get('alamatopd')
        kontakopd = request.POST.get('kontakopd')
        emailopd = request.POST.get('emailopd')
        lokasi = request.POST.get('lokasi')

        if tahun is not None:
            insert_skpd = OPD()
            insert_skpd.tahun_id = tahun
            insert_skpd.kodeopd = kodeopd
            insert_skpd.namaopd = namaopd
            insert_skpd.alamatopd = alamatopd
            insert_skpd.kontakopd = kontakopd
            insert_skpd.emailopd = emailopd
            insert_skpd.lokasi_id = lokasi

            insert_skpd.save()
            messages.success(request, 'Data SKPD berhasil disimpan.')
            return redirect('profile:admin_skpd')
        messages.error(request, 'Data SKPD gagal disimpan.')
        return render(request, 'profile/admin/opd/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_skpd = OPD.objects.get(id = id)

        context = {
            'title' : 'Ubah Data SKPD',
            'form' : form,
            'edit' : 'true',
            'data_skpd_' : dt_skpd,
        }
        template = 'profile/admin/opd/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_skpd = OPD.objects.get(id = id)
        tahun = request.POST.get('tahun')
        kodeopd = request.POST.get('kodeopd')
        namaopd = request.POST.get('namaopd')
        alamatopd = request.POST.get('alamatopd')
        kontakopd = request.POST.get('kontakopd')
        emailopd = request.POST.get('emailopd')
        lokasi = request.POST.get('lokasi')

        if data_skpd is not None:
            
            data_skpd.tahun_id = tahun
            data_skpd.kodeopd = kodeopd
            data_skpd.namaopd = namaopd
            data_skpd.alamatopd = alamatopd
            data_skpd.kontakopd = kontakopd
            data_skpd.emailopd = emailopd
            data_skpd.lokasi_id = lokasi
            data_skpd.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_skpd')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/opd/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = OPD.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except OPD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = OPD.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except OPD.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)