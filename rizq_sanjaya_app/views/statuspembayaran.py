from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
   
    masterpembayaran_list = masterpembayaran.objects.filter(status='Aktif')
    archives = masterpembayaran.objects.filter(status='Tidak aktif')
   
    context = {
        'title' : 'Master Pembayaran',
        'masterpembayaran_list' : masterpembayaran_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/masterpembayaran/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = masterpembayaran.objects.all()
        data_status_pembayaran = masterpembayaran.objects.all()
        context = {
            'title' : 'Data Master Pembayaran',
            'form' : form,
            'data_status_pembayaran':data_status_pembayaran,
        }

        template = 'profile/admin/masterpembayaran/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
       
        urai = request.POST.get('urai')
       
      

        if urai is not None:
            insert_status_pembayaran = masterpembayaran()
            insert_status_pembayaran.urai = urai
            insert_status_pembayaran.save()
            messages.success(request, 'Data master pembayaran berhasil disimpan.')
            return redirect('profile:admin_status_pembayaran')
        messages.error(request, 'Data SKPD gagal disimpan.')
        return render(request, 'profile/admin/masterpermbayaran/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_status_pembayaran = masterpembayaran.objects.get(id = id)

        context = {
            'title' : 'Ubah Data Master Pembayaran',
            'form' : form,
            'edit' : 'true',
            'data_status_pembayaran_' : dt_status_pembayaran,
        }
        template = 'profile/admin/masterpembayaran/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        insert_data_status_pembayaran = masterpembayaran.objects.get(id = id)
        urai = request.POST.get('urai')
       

        if insert_data_status_pembayaran is not None:
            
            insert_data_status_pembayaran.urai = urai
           
            insert_data_status_pembayaran.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_status_pembayaran')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/masterpembayaran/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = masterpembayaran.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except masterpembayaran.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = masterpembayaran.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except masterpembayaran.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)