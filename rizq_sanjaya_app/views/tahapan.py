from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.urls import reverse
from django.contrib import messages
from django.utils import timezone
import os
from django.db import transaction
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.core.exceptions import ValidationError

@login_required
@is_verified()
@require_http_methods(["GET"])
def indexTahapan(request):
    page = request.GET.get('page', 1)
    tahun_list = Tahun.objects.filter(deleted_at=None)
    tahapan_list = MasterTahapan.objects.filter(deleted_at=None).order_by('-created_at')
    archives = MasterTahapan.objects.filter(deleted_at__isnull=False)
    paginator = Paginator(tahapan_list, 15)
    print(tahun_list)
    try:
        tahapan_list = paginator.page(page)
    except PageNotAnInteger:
        tahapan_list = paginator.page(1)
    except EmptyPage:
        tahapan_list = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master Tahun',
        'tahapan_list' : tahapan_list,
        'tahun_list' : tahun_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/master_tahapan/index.html', context)

@login_required
@is_verified()
def create(request):
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        tahapan = request.POST.get('tahapan')
        status = request.POST.get('status')
        try:

            with transaction.atomic():
                insert_tahapan = MasterTahapan()
                insert_tahapan.tahun_id = tahun
                insert_tahapan.tahapan = tahapan
                # insert_tahapan.status = status
                insert_tahapan.save()

            messages.success(request, 'Data BAST berhasil disimpan.')
            return redirect('profile:index_tahapan')
                
        except ValidationError as ve:
            # Tampilkan pesan validasi model
            messages.error(request, f"Validasi gagal: {ve.messages}")
            return redirect('profile:index_tahapan')

        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal disimpan.')
            return redirect('profile:index_tahapan')


@login_required
@is_verified()
def edit(request, id_tahapan):
    if request.method == 'POST':
        tahun = request.POST.get('tahun')
        tahapan = request.POST.get('tahapan')
        status = request.POST.get('status')
        try:

            with transaction.atomic():
                insert_tahapan = MasterTahapan.objects.get(id_tahapan = id_tahapan)
                insert_tahapan.tahun_id = tahun
                insert_tahapan.tahapan = tahapan
                # insert_tahapan.status = status
                insert_tahapan.save()

            messages.success(request, 'Data Tahapan berhasil diubah.')
            return redirect('profile:index_tahapan')

        except ValidationError as ve:
            # Tampilkan pesan validasi ke pengguna
            error_message = ', '.join(ve.messages)  # Gabungkan pesan error
            messages.error(request, f"{error_message}")
            return redirect('profile:index_tahapan')
                
        except Exception as e:
            print('Error akun', e)
            messages.error(request, 'Data gagal diubah.')
            return redirect('profile:index_tahapan')



@login_required
@is_verified()
def softDelete(request, id_tahapan):
    message = ''
    try:
        sekarang = timezone.now()
        doc = MasterTahapan.objects.get(id_tahapan=id_tahapan)
        doc.deleted_at = sekarang
        doc.save()
        message = 'success'
    except Tahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id_tahapan):
    message = ''
    try:
        doc = MasterTahapan.objects.get(id_tahapan=id_tahapan)
        doc.deleted_at = None
        doc.save()
        message = 'success'
    except Tahun.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)
