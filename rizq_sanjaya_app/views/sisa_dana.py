from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.http import Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from ..models import FakturPajak, FakturPajakRincian, MasterKegiatan as m_kegiatan, MasterTahapan
from django.utils import timezone
import os
from django.urls import reverse
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category
from django.db import transaction
import re, pprint, json, math
from rizq_sanjaya_app.context_processors import global_variables
from rizq_sanjaya_app.views.utility_views import get_sisa_pagu, generate_report, spk_validation, cek_ba_spk, get_sisa_pagu_laporan, get_total_realisasi
from decimal import Decimal
from django.db.models import Sum, F, Value, DecimalField, Q, F
from django.db.models.functions import Coalesce
from datetime import datetime
from collections import defaultdict

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    data_subkegiatan = m_kegiatan.objects.all().get_descendants().filter(is_sumberdana = True)
    data_rincian = []
    data_keterangan_entry = []

    pagu_subkegiatan = 0
    total_keseluruhan_sisa_pagu = 0
    total_keseluruhan_subtotal_ssh = 0
    total_keseluruhan_pagu_per_keterangan = 0

    try: 
        with transaction.atomic():
            for x in data_subkegiatan:
                for rekening in x.get_ancestors().filter(is_rekening_belanja=True):
                    data_rekening = {
                        'kegiatan_kode': rekening.kegiatan_kode, 
                        'skpd': rekening.skpd.namaopd, 
                        'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                        'children': [], 
                        'total_sisa_pagu': 0  
                    }
                    
                    for bagian in rekening.get_descendants().filter(is_bagian=True):
                        data_bagian = {
                            'kegiatan_kode': '', 
                            'kegiatan_nama': bagian.kegiatan_nama, 
                            'children': []
                        }
                        
                        for keterangan in bagian.get_descendants().filter(is_subrekening_belanja=True):
                            pagu_per_keterangan = 0
                            total_sisa_pagu_keterangan = 0  
                            subtotal_ssh_keterangan = 0
                            
                            data_keterangan = {
                                'kegiatan_kode': '', 
                                'kegiatan_nama': keterangan.kegiatan_nama, 
                                'skpd': rekening.skpd.namaopd, 
                                'children': [], 
                                'pagu_per_keterangan': pagu_per_keterangan,
                                'total_sisa_pagu': 0 ,
                                'bagian_nama': bagian.kegiatan_nama,
                                'subtotal_ssh': 0,

                            }
                            
                            for ssh in keterangan.get_descendants():
                                subtotal = ssh.nilai * ssh.volume
                                pagu_subkegiatan += subtotal
                                
                                tahapan = MasterTahapan.objects.get(status=True)
                                sisa_pagu = round(get_sisa_pagu_laporan(tahapan, ssh.kegiatan_id))  
                                total_ralisasi = round(get_total_realisasi(tahapan, ssh.kegiatan_id))  
                                
                                data_ssh = {
                                    'kegiatan_kode': '', 
                                    'kegiatan_nama': ssh.uraian_ssh, 
                                    'volume': ssh.volume,
                                    'satuan': ssh.satuan,
                                    'nilai': ssh.nilai,
                                    'subtotal': subtotal,
                                    'kegiatan_id': ssh.kegiatan_id,
                                    'sisa_pagu': sisa_pagu, 
                                }

                                
                                data_keterangan['children'].append(data_ssh)
                                data_keterangan['pagu_per_keterangan'] += subtotal
                                total_sisa_pagu_keterangan += sisa_pagu  
                                subtotal_ssh_keterangan += total_ralisasi 
                            
                            data_keterangan['total_sisa_pagu'] = total_sisa_pagu_keterangan
                            data_keterangan['subtotal_ssh'] = subtotal_ssh_keterangan
                            
                            data_rekening['children'].append(data_keterangan)
                            data_rekening['total_sisa_pagu'] += total_sisa_pagu_keterangan  

                            total_keseluruhan_sisa_pagu += total_sisa_pagu_keterangan
                            total_keseluruhan_subtotal_ssh += subtotal_ssh_keterangan
                            total_keseluruhan_pagu_per_keterangan += data_keterangan['pagu_per_keterangan'] 

                            data_keterangan_entry.append(data_keterangan)
                            
                    
                    data_rincian.append(data_rekening)

    except Exception as e:
        print('Error get detail SPK', e)
        pass


    grouped_data = defaultdict(lambda: {'skpd': '', 'pagu_per_keterangan': 0, 'subtotal_ssh': 0, 'total_sisa_pagu': 0, 'entries': []})

    for data in data_keterangan_entry:
        bagian_nama = data['bagian_nama']
        

        if not grouped_data[bagian_nama]['skpd']:
            grouped_data[bagian_nama]['skpd'] = data['skpd']

        grouped_data[bagian_nama]['pagu_per_keterangan'] += data['pagu_per_keterangan']
        grouped_data[bagian_nama]['subtotal_ssh'] += data['subtotal_ssh']
        grouped_data[bagian_nama]['total_sisa_pagu'] += data['total_sisa_pagu']
        grouped_data[bagian_nama]['entries'].append(data)


    data_keterangan_entry_grouped = [
        {
            'skpd': value['skpd'],
            'bagian_nama': key,
            'pagu_per_keterangan': value['pagu_per_keterangan'],
            'subtotal_ssh': value['subtotal_ssh'],
            'total_sisa_pagu': value['total_sisa_pagu'],
        }
        for key, value in grouped_data.items()
    ]



    context = {
        'title' : 'Realisasi Kegiatan',
        'data_rincian' : data_rincian,
        'data_keterangan_entry' : data_keterangan_entry_grouped,
        'total_keseluruhan_sisa_pagu' : total_keseluruhan_sisa_pagu,
        'total_keseluruhan_subtotal_ssh' : total_keseluruhan_subtotal_ssh,
        'total_keseluruhan_pagu_per_keterangan' : total_keseluruhan_pagu_per_keterangan,
    }
    
    return render(request, 'profile/admin/laporan/sisa_dana.html', context)

@login_required
@is_verified()
def CetakLaporanTagihan(request):
    data_subkegiatan = m_kegiatan.objects.all().get_descendants().filter(is_sumberdana = True)
    data_rincian = []
    data_keterangan_entry = []

    pagu_subkegiatan = 0
    total_keseluruhan_sisa_pagu = 0
    total_keseluruhan_subtotal_ssh = 0
    total_keseluruhan_pagu_per_keterangan = 0

    try: 
        with transaction.atomic():
            for x in data_subkegiatan:
                for rekening in x.get_ancestors().filter(is_rekening_belanja=True):
                    data_rekening = {
                        'kegiatan_kode': rekening.kegiatan_kode, 
                        'skpd': rekening.skpd.namaopd, 
                        'kegiatan_nama': f'{rekening.kegiatan_nama} - {x.kegiatan_nama}', 
                        'children': [], 
                        'total_sisa_pagu': 0  
                    }
                    
                    for bagian in rekening.get_descendants().filter(is_bagian=True):
                        data_bagian = {
                            'kegiatan_kode': '', 
                            'kegiatan_nama': bagian.kegiatan_nama, 
                            'children': []
                        }
                        
                        for keterangan in bagian.get_descendants().filter(is_subrekening_belanja=True):
                            pagu_per_keterangan = 0
                            total_sisa_pagu_keterangan = 0  
                            subtotal_ssh_keterangan = 0
                            
                            data_keterangan = {
                                'kegiatan_kode': '', 
                                'kegiatan_nama': keterangan.kegiatan_nama, 
                                'skpd': rekening.skpd.namaopd, 
                                'children': [], 
                                'pagu_per_keterangan': pagu_per_keterangan,
                                'total_sisa_pagu': 0 ,
                                'bagian_nama': bagian.kegiatan_nama,
                                'subtotal_ssh': 0,

                            }
                            
                            for ssh in keterangan.get_descendants():
                                subtotal = ssh.nilai * ssh.volume
                                pagu_subkegiatan += subtotal
                                
                                tahapan = MasterTahapan.objects.get(status=True)
                                sisa_pagu = round(get_sisa_pagu_laporan(tahapan, ssh.kegiatan_id))  
                                total_ralisasi = round(get_total_realisasi(tahapan, ssh.kegiatan_id))  
                                
                                data_ssh = {
                                    'kegiatan_kode': '', 
                                    'kegiatan_nama': ssh.uraian_ssh, 
                                    'volume': ssh.volume,
                                    'satuan': ssh.satuan,
                                    'nilai': ssh.nilai,
                                    'subtotal': subtotal,
                                    'kegiatan_id': ssh.kegiatan_id,
                                    'sisa_pagu': sisa_pagu, 
                                }

                                
                                data_keterangan['children'].append(data_ssh)
                                data_keterangan['pagu_per_keterangan'] += subtotal
                                total_sisa_pagu_keterangan += sisa_pagu  
                                subtotal_ssh_keterangan += total_ralisasi 
                            
                            data_keterangan['total_sisa_pagu'] = total_sisa_pagu_keterangan
                            data_keterangan['subtotal_ssh'] = subtotal_ssh_keterangan
                            
                            data_rekening['children'].append(data_keterangan)
                            data_rekening['total_sisa_pagu'] += total_sisa_pagu_keterangan  

                            total_keseluruhan_sisa_pagu += total_sisa_pagu_keterangan
                            total_keseluruhan_subtotal_ssh += subtotal_ssh_keterangan
                            total_keseluruhan_pagu_per_keterangan += data_keterangan['pagu_per_keterangan'] 

                            data_keterangan_entry.append(data_keterangan)
                            
                    
                    data_rincian.append(data_rekening)

    except Exception as e:
        print('Error get detail SPK', e)
        pass


    grouped_data = defaultdict(lambda: {'skpd': '', 'pagu_per_keterangan': 0, 'subtotal_ssh': 0, 'total_sisa_pagu': 0, 'entries': []})

    for data in data_keterangan_entry:
        bagian_nama = data['bagian_nama']
        
        
        if not grouped_data[bagian_nama]['skpd']:
            grouped_data[bagian_nama]['skpd'] = data['skpd']

        grouped_data[bagian_nama]['pagu_per_keterangan'] += data['pagu_per_keterangan']
        grouped_data[bagian_nama]['subtotal_ssh'] += data['subtotal_ssh']
        grouped_data[bagian_nama]['total_sisa_pagu'] += data['total_sisa_pagu']
        grouped_data[bagian_nama]['entries'].append(data)


    data_keterangan_entry_grouped = [
        {
            'skpd': value['skpd'],
            'bagian_nama': key,
            'pagu_per_keterangan': value['pagu_per_keterangan'],
            'subtotal_ssh': value['subtotal_ssh'],
            'total_sisa_pagu': value['total_sisa_pagu'],
        }
        for key, value in grouped_data.items()
    ]

    context = {
        'title' : 'Realisasi Kegiatan',
        'data_rincian' : data_rincian,
        'data_keterangan_entry' : data_keterangan_entry_grouped,
        'total_keseluruhan_sisa_pagu' : total_keseluruhan_sisa_pagu,
        'total_keseluruhan_subtotal_ssh' : total_keseluruhan_subtotal_ssh,
        'total_keseluruhan_pagu_per_keterangan' : total_keseluruhan_pagu_per_keterangan,
        'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
    }

            
    pdf_format = {
        'format': 'A4',
        # 'width': "21cm",
        # "height": "21.97cm",
        'margin': {'top':'2.5cm', 'right':'2.5cm', 'bottom':'2.5cm', 'left':'2.5cm'},
        'display_header_footer':True,
        'header_template':"<div></div>",
        'footer_template':"<div></div>",
        'landscape': True,
    }
    print(pdf_format)
    pdf_file = generate_report('profile/admin/laporan/print_sisa_dana.html', context, pdf_format)

    return HttpResponse(pdf_file, content_type='application/pdf')




    