from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from ..forms import *
from django.contrib import messages
from django.utils import timezone
import os
from django.conf import settings
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ..decorators import *
from django.db.models.functions import Substr, StrIndex
from ..helpers import querytags, get_category

@login_required
@is_verified()
@require_http_methods(["GET"])
def admin_index(request):
    page = request.GET.get('page', 1)
    kategori_list = kategori.objects.filter(status='Aktif')
    archives = kategori.objects.filter(status='Tidak aktif')
    paginator = Paginator(kategori_list, 15)
    try:
        kategori_list = paginator.page(page)
    except PageNotAnInteger:
        kategori_list = paginator.page(1)
    except EmptyPage:
        kategori_list = paginator.page(paginator.num_pages)

    context = {
        'title' : 'Master Kategori',
        'kategori_list' : kategori_list,
        'archives' : archives,
    }
    
    return render(request, 'profile/admin/kategori/index.html', context)

@login_required
@is_verified()
def create(request):
    template = ''
    context = {} 
    form = ''

    if request.method == 'GET':
        form = kategori.objects.all()
        data_kategori = kategori.objects.all()
        context = {
            'title' : 'Data Kategori',
            'form' : form,
            'data_kategori':data_kategori,
        }

        template = 'profile/admin/kategori/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        nama = request.POST.get('nama')
        if nama is not None:
            insert_kategori = kategori()
            insert_kategori.nama = nama
            insert_kategori.save()
            messages.success(request, 'Data Kategori berhasil disimpan.')
            return redirect('profile:admin_kategori')
        messages.error(request, 'Data Kategori gagal disimpan.')
        return render(request, 'profile/admin/kategori/index.html', {'form': form,})

@login_required
@is_verified()
def edit(request, id):
    template = ''
    context = {} 
    form = ''
    
    if request.method == 'GET':
        dt_kategori = kategori.objects.get(id = id)

        context = {
            'title' : 'Ubah Data Kategori',
            'form' : form,
            'edit' : 'true',
            'data_kategori' : dt_kategori,
        }
        template = 'profile/admin/kategori/index.html'
        return render(request, template, context)
    
    if request.method == 'POST':
        data_kategori = kategori.objects.get(id = id)
        nama = request.POST.get('nama')

        if data_kategori is not None:
            data_kategori.nama = nama
            data_kategori.save()

            messages.success(request, 'Data berhasil Diubah')
            return redirect('profile:admin_kategori')

        messages.error(request, 'Data gagal disimpan.')
        return render(request, 'profile/admin/kategori/index.html', {'form': form,})



@login_required
@is_verified()
def softDelete(request, id):
    message = ''
    try:
        doc = kategori.objects.get(id=id)
        doc.status = 'Tidak aktif'
        doc.save()
        message = 'success'
    except kategori.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)

@login_required
@is_verified()
def restore(request, id):
    message = ''
    try:
        doc = kategori.objects.get(id=id)
        doc.status = 'Aktif'
        doc.save()
        message = 'success'

    except kategori.DoesNotExist:
        message = 'error'

    context = {
            'message' : message,
        }

    return HttpResponse(context)