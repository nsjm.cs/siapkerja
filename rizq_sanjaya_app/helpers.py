import os
import numpy
import pprint

import urllib.request
import xml.etree.ElementTree as ET
from django.db import connections
from itertools import islice
from django.conf import settings

from datetime import datetime


def get_user_info(request):
    device_type = browser_type = browser_version = os_type = os_version = ip = '-'

    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    # try:
    #     ip = requests.get("https://api.ipify.org/").text
    # except Exception as e:
    #     pass

    if request.user_agent.is_mobile:
        device_type = "Mobile"
    if request.user_agent.is_tablet:
        device_type = "Tablet"
    if request.user_agent.is_pc:
        device_type = "PC"
    browser_type = request.user_agent.browser.family
    browser_version = request.user_agent.browser.version_string
    os_type = request.user_agent.os.family
    os_version = request.user_agent.os.version_string

    return {
        'ip' : ip,
        'device_type' : device_type,
        'browser_type' : browser_type,
        'browser_version' : browser_version,
        'os_type' : os_type,
        'os_version' : os_version,
    }

def insert_log_visitor(request):
    user_info = get_user_info(request)
    log = LogVisitor()
    log.ip = user_info['ip']
    log.device_type = user_info['device_type']
    log.browser_type = user_info['browser_type']
    log.browser_version = user_info['browser_version']
    log.os_type = user_info['os_type']
    log.os_version = user_info['os_version']
    log.save()

def dictfetchall(cursor):
    # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def querytags(jenis):
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT COUNT(a.tag_id) as jml, c.name, c.slug, b.model FROM taggit_taggeditem a
                LEFT JOIN django_content_type b ON a.content_type_id = b.id
                LEFT JOIN taggit_tag c ON a.tag_id = c.id
            WHERE b.model = %s
            GROUP BY a.tag_id, c.name, c.slug, b.model
            ORDER BY c.name;""",[jenis])
        dt_tags = dictfetchall(cursor)
    return dt_tags

def get_category(typexs):
    with connections['default'].cursor() as cursor:
        cursor.execute("""
            SELECT * FROM rizq_sanjaya_app_category
            WHERE typexs = %s ORDER BY id;""",[typexs])
        hasile = dictfetchall(cursor)
    return hasile


def xmlreader(urlxml):
    files = urllib.request.urlopen(urlxml)
    tree = ET.parse(files)
    datax = tree.getroot()

    return datax

def read_boxicons_css():
    array = []
    fh = open(settings.STATICFILES_DIRS[0]+'/profile/front/vendor/boxicons/css/boxicons.css',"r")

    with fh as f:
        for line in islice(f, 524, None):
            if "bx" in line: 
                array.append(line.split(':')[0].split(".")[1])
    return array

def read_fontAwesome_css():
    array = []
    fh = open(settings.STATICFILES_DIRS[0]+'/profile/front/vendor/fontawesome-free/css/fontawesome.css',"r")

    with fh as f:
        for line in islice(f, 520, None):
            if ".fa-" in line: 
                if ".fa-sr" not in line: 
                    array.append(line.split(':')[0].split(".")[1])
    return array[:-2]

def read_icon(section):
    if section == 'about' or section == 'why-us':
        hasil = read_boxicons_css()
    else:
        hasil = read_fontAwesome_css()

    return hasil


def infogempa():

    # https://isnaininurulkurniasari.medium.com/web-scrapping-dengan-xml-tree-e92d547958a4
    # https://towardsdatascience.com/processing-xml-in-python-elementtree-c8992941efd2

    cinta = {}
    nganu = []
    datax = xmlreader('https://data.bmkg.go.id/DataMKG/TEWS/gempaterkini.xml')
    for x in datax:
        for elem in datax.findall('gempa/'):
            if elem.tag == 'point':
                for chil in elem:
                    cinta[elem.tag] = chil.text
            else:
                cinta[elem.tag] = elem.text

            nganu.append(cinta)

    return nganu

def perkiraancuaca():
    arr_cuaca = {'0':'Cerah|clear-day', '1':'Cerah Berawan|partly-cloudy-day', '2':'Cerah Berawan|partly-cloudy-day',
        '3':'Berawan|cloudy', '4':'Berawan Tebal|cloudy', '5':'Udara Kabur|wind', '10':'Asap|fog',
        '45':'Kabut|fog', '60':'Hujan Ringan|rain', '61':'Hujan Sedang|rain', '63':'Hujan Lebat|sleet',
        '80':'Hujan Lokal|rain', '95':'Hujan Petir', '97':'Hujan Petir'
    }

    # "clear-day", "clear-night", "partly-cloudy-day",
    #             "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
    #             "fog"

    cinta = {}
    cuaca = []
    temps = []
    datax = xmlreader('https://data.bmkg.go.id/DataMKG/MEWS/DigitalForecast/DigitalForecast-Papua.xml')
    for x in datax.findall("./forecast/area/[@id='5014309']/parameter/[@id='weather']/"):
        cinta = x.attrib 
        for z in x:
            cinta['kd_val'] = z.text
            cinta['value'] = arr_cuaca[z.text] 
        cuaca.append(cinta)

    # for x in datax.findall("./forecast/area/[@id='5014309']/parameter/[@id='t']/"):
    #     temps.append(x.attrib)

    return {'cuaca':cuaca, 'temps':temps }
