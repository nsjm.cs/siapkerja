from playwright.sync_api import sync_playwright
import json, sys, os, glob, time

def generate_report(html_content, options, report_id, html_file_path):
    options = json.loads(option)

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True, args=[
        "--no-sandbox",
        "--disable-gpu",
        "--disable-extensions",
        "--disable-dev-shm-usage",
        "--disable-software-rasterizer",
        "--no-first-run",
        "--disable-background-networking",
        "--disable-default-apps"
    ])  
        page = browser.new_page()
        page.set_viewport_size({"width": 200, "height": 100})

        if html_content == 'using_file_path':
            page.goto(f"file://{html_file_path}", wait_until="networkidle")
        else:
            page.set_content(html_content, wait_until="networkidle")

        page.emulate_media(media="print")

        page.pdf(
            **options,
            print_background=True,
        )
        
        browser.close()

    
    pattern = os.path.join(os.getcwd(), 'media', 'temp_files', f"{report_id}*")
    files_to_delete = glob.glob(pattern)
    for file_path in files_to_delete:
        try:
            os.remove(file_path)
            print(f"Deleted: {file_path}")
        except Exception as e:
            print(f"Error deleting {file_path}: {e}")

if __name__ == "__main__":
    html_content = sys.argv[1]
    option = sys.argv[2]
    report_id = sys.argv[3]
    try:
        html_file_path = sys.argv[4]
    except Exception as e:
        html_file_path = ''
    
    generate_report(html_content, option, report_id, html_file_path)
