from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.text import slugify
import string
import random
import uuid

from requests import request
from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.utils import timezone
import datetime
from ckeditor.fields import RichTextField

from mptt.models import MPTTModel, TreeForeignKey

# validate_data
from django.core.exceptions import ValidationError
from django.db import transaction, IntegrityError
from django.db.models import ImageField, FileField

def validate_data(model_name, obj):
    # Cek Integrity
    unique_fields = [{field.name: field.verbose_name} for field in obj._meta.get_fields() if getattr(field, 'unique', False) 
                        and not getattr(field, "primary_key", False)]
    for x in unique_fields:
        filter_ = {}
        for field_name_, verbose_name_ in x.items():
            filter_[field_name_] = getattr(obj, field_name_)
            if obj.__class__.objects.filter(**filter_).exists():
                instance = obj.__class__.objects.get(**filter_)
                if instance != obj:
                    raise IntegrityError(f'Maaf, {verbose_name_.title()} sudah pernah dipakai.')

    if hasattr(obj.__class__, 'Meta'):
        if hasattr(obj.__class__.Meta, 'unique_together'):
            for unique_combination in obj.__class__.Meta.unique_together:
                filter_ = {field: getattr(obj, field) for field in unique_combination}
                if obj.__class__.objects.filter(**filter_).exists():
                    raise IntegrityError(f'Maaf, data <br>{", ".join([str(getattr(obj, field)) for field in constraint.fields])} sudah ada.')

        if hasattr(obj.__class__.Meta, 'constraints'):
            for constraint in obj.__class__.Meta.constraints:
                if isinstance(constraint, models.UniqueConstraint):
                    filter_ = {field: getattr(obj, field) for field in constraint.fields}
                    if obj.__class__.objects.filter(**filter_).exists():
                        raise IntegrityError(f'Maaf, data <br>{", ".join([str(getattr(obj, field)) for field in constraint.fields])} sudah ada.')

        if hasattr(obj, '_meta') and hasattr(obj._meta, 'constraints'):
            for constraint in obj._meta.constraints:
                if isinstance(constraint, models.UniqueConstraint):
                    filter_ = {field: getattr(obj, field) for field in constraint.fields}
                    # Check if the constraint has a condition
                    condition = getattr(constraint, 'condition', None)
                    
                    # Apply condition if it exists
                    if condition is not None:
                        condition_filter = condition.children  # Extract condition as a list of tuples
                        for field, value in condition_filter:
                            # Add the condition fields and values to the filter
                            filter_[field] = value

                    # Debug output for validation
                    print(f"Filter: {filter_}")
                    print(f"Condition: {condition}")
                    print(f"Checking existence for: {obj.__class__.__name__}")

                    if obj.__class__.objects.filter(**filter_).exists():
                        raise IntegrityError(f'Maaf, data <br>{", ".join([str(getattr(obj, field)) for field in constraint.fields])} sudah ada.')

    max_length_fields = [{'field_name': field.name, 'verbose_name': field.verbose_name, 'max_length': field.max_length} 
                            for field in obj._meta.get_fields() if getattr(field, 'max_length', False) 
                            and not getattr(field, "primary_key", False) and not isinstance(field, (ImageField, FileField))]

    for x in max_length_fields:
        if getattr(obj, x['field_name']) is not None and len(getattr(obj, x['field_name'])) > x['max_length']:
            raise ValidationError(f'Maaf, {x["verbose_name"].title()} tidak boleh melebihi {x["max_length"]} karakter.')


""""==============================VALIDATOR======================================"""
def validate_file_pdf(value):
    pass

"""VALIDASI UNTUK FILE DOKUMEN"""
def validate_file_dokumen(value):
    dokumen =[
            # 'application/vnd.ms-excel', 
            # 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
            'application/pdf', 
            'image/jpeg',
            'image/png',
            # 'text/csv',
            # 'application/msword',
            # 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            ]
    if value.file.content_type not in dokumen:
        # raise ValidationError(u'Pastikan ekstensi file adalah .csv, .doc, .docx, .pdf, .xls atau .xlxs.')
        raise ValidationError(u'Pastikan ekstensi file adalah .jpeg, .png, .pdf')

"""VALIDASI UNTUK FILE GAMBAR"""
def validate_file_gambar(value):
    dokumen =[
                'image/jpeg',
                'image/png',
                'image/webp',
                'image/jpg',
            ]
    if value.file.content_type not in dokumen:
        raise ValidationError(u'Pastikan ekstensi file adalah .jpg, .jpeg atau .png.')

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN FILE"""
def validate_file_size_dokumen(value):
    filesize= value.size
    
    # if filesize > 5242880:
    if filesize > 5242880:
        raise ValidationError("Pastikan ukuran File dibawah 5 MB.")
    else:
        return value

"""VALIDASI UNTUK UKURAN MAKSIMAL UKURAN GAMBAR"""
def validate_file_size_gambar(value):
    filesize= value.size
    
    # if filesize > 5242880:
    if filesize > 2242880:
        raise ValidationError("Pastikan ukuran File dibawah 2 MB.")
    else:
        return value


""""==============================FUNGSI======================================"""
"""MEMBERIKAN NILAI RANDOM UNTUK SLUG KALO DUPLIKAT"""
def rand_slug():
    rand = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
    return rand.lower()

COLOR_CHOICES = [
    ('#0d6efd', 'Biru'),
    ('#6610f2', 'Indigo'),
    ('#6f42c1', 'Ungu'),
    ('#d63384', 'Merah Muda'),
    ('#dc3545', 'Merah'),
    ('#fd7e14', 'Oranye'),
    ('#ffc107', 'Kuning'),
    ('#198754', 'Hijau'),
    ('#20c997', 'Teal'),
    ('#0dcaf0', 'Cyan'),
    ('#adb5bd', 'Abu-Abu'),
    ('#000', 'Hitam'),
]
ANNOUNCEMENT_CHOICES = [
    ('ASN', 'ASN'),
    ('Umum', 'Umum'),
]
ROLE_CHOICES = [
    ('admin', 'Admin'),
    ('dinas', 'Dinas'),
    ('perusahaan', 'Perusahaan'),
]

tahun_sekarang = datetime.datetime.now().year

class UUIDTextModel(models.Model):
    id = models.TextField(primary_key=True, default=uuid.uuid4, editable=False, unique = True)

    class Meta:
        abstract = True


""""==============================MODEL TABEL======================================"""
"""BASE MODEL BIAR GA NGULANG2 MASUKIN 3 FIELD KETERANGAN WAKTU INI"""
class Time(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True
        
class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, username, phone, password, **extra_fields):
        values = [email, username, phone,]
        field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
        for field_name, value in field_value_map.items():
            if not value:
                raise ValueError('The {} value must be set'.format(field_name))

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            username=username,
            phone=phone,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, username, phone, password, **extra_fields)

    def create_superuser(self, email, username, phone, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_verified', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        if extra_fields.get('is_verified') is not True:
            raise ValueError('Superuser must have is_verified=True.')

        return self._create_user(email, username, phone, password, **extra_fields)

class Tahun(UUIDTextModel, Time):
    tahun   =   models.IntegerField(default=tahun_sekarang)
    status =   models.BooleanField(default=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        validate_data(self.__class__.__name__, self)


        if int(self.tahun) < timezone.now().year:
            raise ValidationError(f'Maaf, tahun tidak boleh kurang dari Tahun Sekarang.')
        
        
        if self.status:
            self.__class__.objects.filter().exclude(pk = self.pk).update(status=False)

        super().save(*args, **kwargs)

        is_tahapan_exist = self.mastertahapan_set.exists()

        if not is_tahapan_exist:
            dt_tahapan = MasterTahapan(
                    tahun = self,
                    tahapan = 'APBD MURNI',
                    status = True,
                )
            dt_tahapan.save()



#########KATEOGRI###################
class lokasi(UUIDTextModel, Time):
    tahun      =   models.ForeignKey(Tahun, on_delete=models.RESTRICT)
    provinsi   =   models.CharField(max_length=100)
    kabupaten  =   models.CharField(max_length=100)
    status     =   models.CharField(max_length=500,null=True, default='Aktif')

class OPD(UUIDTextModel, Time):
    tahun      =   models.ForeignKey(Tahun, on_delete=models.RESTRICT)
    kodeopd    =   models.CharField(max_length = 100, default = '')
    namaopd    =   models.TextField(null=True)
    alamatopd  =   models.TextField(null=True)
    kontakopd  =   models.CharField(max_length=100)
    emailopd   =   models.CharField(max_length=100)
    status     =   models.CharField(max_length=500,null=True, default='Aktif')
    lokasi     =   models.ForeignKey(lokasi, on_delete=models.RESTRICT)


"""TABEL AKUN UNTUK SELAIN BAWAANNYA DJANGO YANG DIPAKAI"""
class Account(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    username = models.CharField(unique=True, max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(null=True)
    phone = models.CharField(max_length=15)
    date_of_birth = models.DateField(blank=True, null=True)
    avatar = models.ImageField(blank=True, null=True, upload_to='profile/images/avatar/', validators=[validate_file_gambar, validate_file_size_gambar],)
    role = models.CharField(max_length=50, choices=ROLE_CHOICES, default='posting')
    email_verification_token = models.CharField(max_length=100, default='')
    id_opd = models.ForeignKey(OPD, on_delete=models.CASCADE, null=True, blank=True)
    
    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'phone', 'role']

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    def get_short_name(self):
        return self.first_name

class AppSetting(UUIDTextModel, Time):
    nama=models.CharField(max_length=100, unique=True)
    keterangan=models.TextField(null=True)
    tanggal = models.DateField(null=True)
    image = models.ImageField(upload_to='profile/images/diskon/', validators=[validate_file_gambar, validate_file_size_gambar], null=True)  


"""TABEL PROFILSKPD DIGUNAKAN UNTUK MENYIMPAN SEJARAH,VISI&MISI, TUPOKSI, STRUKTUR ORGANISASI, PROFIL PEJABAT,SAMBUTAN KEPALA DINAS"""
class ProfilSKPD(UUIDTextModel, Time):
    title = RichTextUploadingField()
    create_date = models.DateTimeField(auto_now_add=True)
    typexs = models.CharField(max_length=500,default='Sejarah')
    description = RichTextUploadingField()
    foto = models.ImageField(null=True, upload_to='profile/images/profilSKPD', validators=[validate_file_gambar, validate_file_size_gambar])
    dokumen = models.FileField(upload_to='profile/announcement/profilSKPD/', validators=[validate_file_dokumen, validate_file_size_dokumen], null=True, blank=True)
    slug = models.SlugField(unique=True, max_length=150)
    status = models.CharField(max_length=500,default='Publish')
    def __init__(self, *args, **kwargs):
        super(ProfilSKPD, self).__init__(*args, **kwargs)
        self.old_title = self.title

    def save(self, *args, **kwargs):
        if self.old_title != self.title:
            slug = slugify(self.title)
            slug_exists = True
            counter = rand_slug()
            self.slug = slug
            while slug_exists:
                try:
                    slug_exits = ProfilSKPD.objects.get(slug=slug)
                    if slug_exits and slug_exits.id == self.id:
                        self.slug = slug
                        break
                    elif slug_exits:
                        slug = self.slug + '-' + str(counter)
                        counter = rand_slug()
                except ProfilSKPD.DoesNotExist:
                    self.slug = slug
                    break
        super(ProfilSKPD, self).save(*args, **kwargs)
######FOR APP SIAPKERJA#############

#########KATEOGRI###################
class kategori(UUIDTextModel, Time):
    nama   =   models.CharField(max_length=100, unique=True)
    status =   models.CharField(max_length=500,null=True, default='Aktif')

class get_tahapan(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter()

    def is_active(self):
        try:
            return super().get(status = True)
        except self.model.DoesNotExist:
            return None

class MasterTahapan(Time):
    id_tahapan = models.TextField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    tahun = models.ForeignKey(Tahun, on_delete=models.RESTRICT, null=True, blank=True)
    tahapan = models.TextField(null=True)
    status = models.BooleanField(null=True, default = True)

    objects = get_tahapan()

    def save(self, *args, **kwargs):
        if self.status:
            self.__class__.objects.filter().exclude(id_tahapan=self.id_tahapan).update(status=False)
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.tahun.tahun} - {self.tahapan}'

class MasterKegiatan(MPTTModel, Time):
    kegiatan_id = models.TextField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    parent = TreeForeignKey('self', on_delete=models.PROTECT, null=True, blank = True)
    kegiatan_kode = models.CharField(max_length = 100, verbose_name = 'Kode Program / Kegiatan / Rekening Belanja', default = '', blank = True, null = True)
    kegiatan_nama = models.TextField(max_length = 100, default = '', blank = True, null = True)
    tahapan = models.ForeignKey(MasterTahapan, on_delete=models.RESTRICT, null=True, blank=True)
    sumberdana = models.CharField(max_length = 100, default = '')
    bagian = models.CharField(max_length = 50, default = '')
    is_program = models.BooleanField(default=False)
    is_kegiatan = models.BooleanField(default=False)
    is_subkegiatan = models.BooleanField(default=False)
    is_rekening_belanja = models.BooleanField(default=False)
    is_subrekening_belanja = models.BooleanField(default=False)
    is_bagian = models.BooleanField(default = False)
    is_sumberdana = models.BooleanField(default = False)

    is_ssh = models.BooleanField(default=False)
    uraian_ssh = models.CharField(max_length = 255, null = True, default = None)
    volume = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    satuan = models.CharField(max_length = 100, null = True, default = None)
    nilai = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    subtotal = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    
    skpd = models.ForeignKey(OPD, on_delete=models.RESTRICT, null=True, blank=True)


    class MPTTMeta:
        order_insertion_by = ['kegiatan_kode']

    def clean(self):
        super().clean()


        if self.is_program:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode)
            # print('program', self.tahapan, self.skpd, self.kegiatan_kode, data.first(), data)
            if data.first():
                return False, data.first()
        elif self.is_kegiatan:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode, parent__kegiatan_kode = self.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_subkegiatan:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode, parent__kegiatan_kode = self.parent.kegiatan_kode, 
                parent__parent__kegiatan_kode = self.parent.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_rekening_belanja:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode, parent__kegiatan_kode = self.parent.kegiatan_kode, 
                parent__parent__kegiatan_kode = self.parent.parent.kegiatan_kode, 
                parent__parent__parent__kegiatan_kode = self.parent.parent.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_sumberdana:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode, parent__kegiatan_kode = self.parent.kegiatan_kode, 
                parent__parent__kegiatan_kode = self.parent.parent.kegiatan_kode, 
                parent__parent__parent__kegiatan_kode = self.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_bagian:
            data = self.__class__.objects.filter(tahapan = self.tahapan, skpd = self.skpd, kegiatan_nama = self.kegiatan_nama, parent__kegiatan_nama = self.parent.kegiatan_nama, 
                parent__parent__kegiatan_kode = self.parent.parent.kegiatan_kode, 
                parent__parent__parent__kegiatan_kode = self.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_subrekening_belanja: #keterangan
            data = self.__class__.objects.filter(bagian = self.bagian, tahapan = self.tahapan, skpd = self.skpd, kegiatan_nama = self.kegiatan_nama, parent__kegiatan_nama = self.parent.kegiatan_nama, 
                parent__parent__kegiatan_nama = self.parent.parent.kegiatan_nama, 
                parent__parent__parent__kegiatan_kode = self.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.parent.kegiatan_kode)
            if data.first():
                return False, data.first()

        elif self.is_ssh:
            # print(self.nama_penerima, self.parent.kegiatan_nama)
            data = self.__class__.objects.filter(bagian = self.bagian, tahapan = self.tahapan, skpd = self.skpd, uraian_ssh = self.uraian_ssh, parent__kegiatan_nama = self.parent.kegiatan_nama,
                parent__parent__kegiatan_nama = self.parent.parent.kegiatan_nama, 
                parent__parent__parent__kegiatan_nama = self.parent.parent.parent.kegiatan_nama, 
                parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.kegiatan_kode, 
                parent__parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.parent.kegiatan_kode,
                parent__parent__parent__parent__parent__parent__parent__kegiatan_kode = self.parent.parent.parent.parent.parent.parent.parent.kegiatan_kode)
            if data.first():
                # print(self.uraian_ssh, "bagian", self.bagian, self.is_program, self.is_kegiatan, self.is_subkegiatan, self.is_rekening_belanja, self.is_subrekening_belanja, self.parent.kegiatan_nama)
                # print(data.first().uraian_ssh, "bagian", data.first().bagian, data.first().is_program, data.first().is_kegiatan, data.first().is_subkegiatan, data.first().is_rekening_belanja, data.first().is_subrekening_belanja)
                return True, f"<br>Data <br><b>{self.uraian_ssh}</b><br> pada keterangan <br><b>{self.parent.kegiatan_nama}</b><br> bagian <br><b>{self.bagian}</b><br> sudah ada."

        # else:
        #     data = self.__class__.objects.filter(bagian = self.bagian, tahapan = self.tahapan, skpd = self.skpd, kegiatan_kode = self.kegiatan_kode, parent__kegiatan_kode = self.parent.kegiatan_kode)
        #     if data.first():
        #         return False, data.first()

        return None, 'Saving data...'

    def save(self, *args, **kwargs):
        # validate_data(self.__class__.__name__, self)

        status_exists, obj = self.clean()
        if status_exists is False :
            # print('object exists', obj, obj.kegiatan_nama, self.kegiatan_nama)
            # print(f"Skipping save for {self.kegiatan_nama} / {self.nama_penerima} because it already exists.")
            # print()
            return obj
        elif status_exists is True:
            raise ValidationError(obj)
        
        # print('saving object', self.is_program, self.is_kegiatan, self.is_subkegiatan, self.is_rekening_belanja, self.is_subrekening_belanja, self.is_ssh)

        super().save(*args, **kwargs)
        return self



class perusahaan(UUIDTextModel, Time):
    namaperusahaan      =   models.TextField(null=True)
    npwp                =   models.TextField(null=True)
    bank                =   models.TextField(null=True)
    norek               =   models.TextField(null=True)
    alamatperusahaan    =   models.TextField(null=True)
    kontakperusahaan    =   models.CharField(max_length=100)
    emailperusahaan     =   models.CharField(max_length=100)
    namadirektur        =   models.CharField(max_length=100)
    logo                =   models.ImageField(upload_to='profile/images/perusahaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    status              =   models.CharField(max_length=500,null=True, default='Aktif')

JABATAN_CHOICE = (
        ('pptk', 'Pejabat Pelaksanaan Teknis Kegiatan'),
        ('ppk', 'Pejabat Pembuat Komitmen (PPK)'),
        ('pengguna_anggaran', 'Pengguna Anggaran (PA)'),
        ('bendahara_pengeluaran', 'Bendahara Pengeluaran'),
        ('pphp', 'PANITIA PENERIMA HASIL PEKERJAAN (PPHP)'),
        ('ppb', 'PEJABAT PENGADAAN BARANG')
    )

class pejabatpengesah(UUIDTextModel, Time):
    nip                 =   models.TextField(null=True)
    namapegawai         =   models.TextField(null=True)
    jabatan             =   models.CharField(max_length=50, choices=JABATAN_CHOICE, default='jabatan')
    jabatankontrak      =   models.CharField(max_length=50, choices=JABATAN_CHOICE, default='jabatan_kontrak')
    status              =   models.CharField(max_length=500,null=True, default='Aktif')
    opd                 =   models.ForeignKey(OPD, on_delete=models.RESTRICT)

class masterpajak(UUIDTextModel, Time):
    uraipajak           =   models.TextField(null=True)
    persen              =   models.DecimalField(null=True, decimal_places=2, max_digits = 18)
    ket                 =   models.TextField(null=True)
    status              =   models.CharField(max_length=500,null=True, default='Aktif')

class masterpembayaran(UUIDTextModel, Time):
    urai            =   models.TextField(null=True)
    status          =   models.CharField(max_length=500,null=True, default='Aktif')

class SPK(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nospk               = models.TextField(null=True)
    tglspk              = models.DateField(null=True)
    uraipekerjaan       = models.TextField(null=True)
    # sumberdana          = models.TextField(null=True)
    waktupelaksanaan    = models.TextField(null=True)
    opd                 = models.ForeignKey(OPD, on_delete=models.RESTRICT)
    perusahaan          = models.ForeignKey(perusahaan, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')
    tahapan = models.ForeignKey(MasterTahapan, null=True, default = None, on_delete = models.RESTRICT, related_name = 'tahapan_spk')
    program = models.ForeignKey(MasterKegiatan, null=True, default = None, on_delete = models.RESTRICT, related_name = 'program_spk')
    kegiatan = models.ForeignKey(MasterKegiatan, null=True, default = None, on_delete = models.RESTRICT, related_name = 'kegiatan_spk')
    subkegiatan = models.ForeignKey(MasterKegiatan, null=True, default = None, on_delete = models.RESTRICT, related_name = 'subkegiatan_spk')
    bagian = models.ForeignKey(MasterKegiatan, null=True, default = None, on_delete = models.RESTRICT, related_name = 'bagian_spk')
    
class SPKRINCIAN(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    ssh = models.ForeignKey(MasterKegiatan, null=True, default = None, on_delete = models.RESTRICT)
    uraian_ssh = models.CharField(max_length = 255, null = True, default = None)
    volume = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    satuan = models.CharField(max_length = 100, null = True, default = None)
    nilai = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    realisasi = models.DecimalField(default = 0, decimal_places=2, max_digits = 18)
    
class SPKSUBRINCIAN(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    updated_at          = models.DateTimeField(auto_now=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    SPKRINCIAN          = models.ForeignKey(SPKRINCIAN, on_delete=models.RESTRICT)
    suburaianbelanja    = models.TextField(null=True)
    qty                 = models.IntegerField(null=True)
    satuan              = models.TextField(null=True)
    harga               = models.IntegerField(null=True)

class SPP(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nosurat             = models.TextField(null=True)
    totalpembayaran     = models.IntegerField(null=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    masterpembayaran    = models.ForeignKey(masterpembayaran, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')

class BAST(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nobap               = models.TextField(null=True)
    tglbap              = models.DateField(null=True)
    uraitgl             = models.TextField(null=True)
    totalpembayaran     = models.IntegerField(null=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')

class PenerimaBAST(Time):
    id_penerima = models.TextField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=225, null=True)
    jabatan = models.CharField(max_length=225, null=True)
    bast = models.ForeignKey(BAST, on_delete=models.RESTRICT, null=True, blank=True)

class BAP(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nobap               = models.TextField(null=True)
    tglbap              = models.DateField(null=True)
    uraitgl             = models.TextField(null=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')

class PenerimaBAP(Time):
    id_penerima = models.TextField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=225, null=True)
    jabatan = models.CharField(max_length=225, null=True)
    bap = models.ForeignKey(BAP, on_delete=models.RESTRICT, null=True, blank=True)

class FakturPajak(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nofaktur            = models.TextField(null=True)
    tglfaktur           = models.DateField(null=True)
    uraitgl             = models.TextField(null=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')

class FakturPajakRincian(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nofaktur            = models.TextField(null=True)
    pajak               = models.ForeignKey(masterpajak, on_delete=models.RESTRICT, null=True)
    tglfaktur           = models.DateField(null=True)
    totalpembayaran     = models.IntegerField(null=True)
    nilaipajak          = models.DecimalField(null=True, decimal_places=2, max_digits = 18)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    status              = models.CharField(max_length=500, null=True, default='Aktif')

class DokumentasiPekerjaan(UUIDTextModel, Time):
    created_at          = models.DateTimeField(auto_now_add=True)
    deleted_at          = models.DateTimeField(null=True, blank=True)
    nodoc               = models.TextField(null=True)
    tgldoc              = models.DateField(null=True)
    uraidoc             = models.TextField(null=True)
    SPK                 = models.ForeignKey(SPK, on_delete=models.RESTRICT)
    uraibukti1          = models.TextField(null=True)
    uraibukti2          = models.TextField(null=True)
    uraibukti3          = models.TextField(null=True)
    uraibukti4          = models.TextField(null=True)
    uraibukti5          = models.TextField(null=True)
    uraibukti6          = models.TextField(null=True)
    bukti1              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    bukti2              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    bukti3              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    bukti4              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    bukti5              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    bukti6              = models.ImageField(upload_to='profile/images/dokumentasi_pekerjaan/', validators=[validate_file_gambar, validate_file_size_gambar], default='profile/images/produk/no-image.jpg') 
    status              = models.CharField(max_length=500, null=True, default='Aktif')


