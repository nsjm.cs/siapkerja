from django import forms
from ..models import *
from PIL import Image
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from captcha.fields import CaptchaField

###################FORM PROFIL SKPD###################
class Profilskpd(forms.ModelForm):
	class Meta:
		model = ProfilSKPD
		fields = ('title','typexs','description')

class AvatarForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Account
		fields = ('avatar', 'x', 'y', 'width', 'height',)

	def save(self):
		account = super(AvatarForm, self).save()
		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(account.avatar)
		cropped_image = image.crop((x, y, w + x, h + y))
		cropped_image.save(f"{settings.MEDIA_ROOT}{account.avatar.name}")

		return account

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('first_name', 'last_name', 'phone', 'date_of_birth')

class RoleForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ('role',)

class CustomUserCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('email', 'username', 'first_name', 'last_name', 'role', 'phone', 'date_of_birth', 'id_opd')

class AppSettingForm(forms.ModelForm):
	class Meta:
		model = AppSetting
		fields = ('nama', 'keterangan')

class MyCaptcha(forms.Form):
   captcha=CaptchaField()

# master Tahun
class KategoriForm(forms.ModelForm):
	class Meta:
		model = kategori
		fields = ('nama', 'status')

# master Wilayah
class WilayahForm(forms.ModelForm):
	class Meta:
		model = lokasi
		fields = ('tahun','provinsi','kabupaten', 'status')

# master OPD
class OpdForm(forms.ModelForm):
	class Meta:
		model = OPD
		fields = ('tahun','namaopd','alamatopd','kontakopd','emailopd','status','lokasi')

# master Perusahaan
class PerusahaanForm(forms.ModelForm):
	class Meta:
		model = perusahaan
		fields = ('namaperusahaan'  ,'npwp','bank','norek','alamatperusahaan','kontakperusahaan','emailperusahaan','namadirektur','status')

# master Pejabat Pengesah
class PejabatpengesahForm(forms.ModelForm):
	class Meta:
		model = pejabatpengesah
		fields = ('nip','namapegawai','jabatan','jabatankontrak','status','opd')

# master Pajak
class PajakForm(forms.ModelForm):
	class Meta:
		model = masterpajak
		fields = ('uraipajak','persen','ket','status')

# master Pembayaran
class PembayaranForm(forms.ModelForm):
	class Meta:
		model = masterpembayaran
		fields = ('urai','status')

# SPK
class SPKForm(forms.ModelForm):
    class Meta:
        model = SPK
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# SPKRINCIAN
class SPKRINCIANForm(forms.ModelForm):
    class Meta:
        model = SPKRINCIAN
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# SPKRINCIAN
class SPKSUBRINCIANForm(forms.ModelForm):
    class Meta:
        model = SPKSUBRINCIAN
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()


# Surat Permohonan Pembayaran
class SPPForm(forms.ModelForm):
    class Meta:
        model = SPP
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# BAST
class BASTForm(forms.ModelForm):
    class Meta:
        model = BAST
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# BAP
class BASTForm(forms.ModelForm):
    class Meta:
        model = BAP
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# FAKTUR PAJAK
class FAKTURTForm(forms.ModelForm):
    class Meta:
        model = FakturPajak
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

# DOKUMENTASI PEKERJAAN
class DOKUEMNTASIForm(forms.ModelForm):
    class Meta:
        model = DokumentasiPekerjaan
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['created_at'].widget = forms.HiddenInput()

		

		
		

	  			   









