from django.urls import path, include
from .views import *
from django.contrib.auth import views as auth_views


app_name = 'profile'
urlpatterns = [
    # path('', admin.index, name='home'),
    ################################################# PATH URL UNTUK ADMIN################################################# 
    path('', include(
        [
            path('', admin.index, name='admin_home'),
            path('verification/', user.verification, name='admin_verification'),
            path('send_verification/', user.send_verification, name='admin_send_verification'),
            path('email/verify/<uidb64>/<token>/',user.email_verify, name='admin_email_verify'),

          
            # USERS
            path('user/', include(
                [
                    path('', user.admin_index, name='admin_user'),
                    path('create/', user.create, name='admin_user_create'),
                    path('password/edit/', user.edit_password, name='admin_user_password_edit'),
                    path('profile/edit/<str:username>/', user.edit_profile, name='admin_user_edit'),
                    path('avatar/edit/<str:username>/', user.edit_avatar, name='admin_user_avatar_edit'),
                    path('detail/', user.admin_detail, name='admin_user_detail'),
                    path('soft_delete/<str:username>/', user.softDelete, name='admin_user_soft_delete'),
                    path('permanent_delete/<str:slug>/', user.permanentDelete, name='admin_user_permanent_delete'),
                    path('restore/<str:username>/', user.restore, name='admin_user_restore'),
                    path('edit_role/', user.edit_role, name='admin_user_edit_role'),
                    path('non_aktif/<str:id>/', user.non_aktif, name='non_aktif'),
                    path('re_aktif/<str:id>/', user.re_aktif, name='aktif'),
                ]
            )),

            # SETTING APLIKASI
            path('setting/', include(
                [
                    # path('', user.admin_index, name='admin_setting'),
                    path('running_text/', setting.admin_index_running_text, name='admin_setting_running_text'),
                    path('running_text/edit/', setting.admin_index_running_text_edit, name='admin_setting_running_text_edit'),
                    path('link/', setting.admin_index_link, name='admin_setting_link'),
                    path('links/edit/', setting.admin_index_link_edit, name='admin_setting_link_edit'),
                    path('infografis/', setting.admin_index_infografis, name='admin_setting_infografis'),
                   
                    path('me/<str:section>/', setting.admin_index_aboutme, name='admin_setting_aboutme'),
                    path('create/me/<str:section>/', setting.create_aboutme, name='admin_setting_create_aboutme'),
                    path('edit/me/<str:section>/<str:id>/', setting.edit_aboutme, name='admin_setting_edit_aboutme'),
                    path('delete/<str:section>/<str:id>/', setting.delete_aboutme, name='admin_setting_delete_aboutme'),
          
                ]
            )),

            # MASTER KATEGORI================================================
            path('kategori/', include(
                [
                    path('', categori.admin_index, name='admin_kategori'),
                    path('create/', categori.create, name='admin_kategori_create'),
                    path('edit/<str:id>/', categori.edit, name='admin_categori_edit'),
                    path('soft_delete/<str:id>/', categori.softDelete, name='admin_categori_soft_delete'),
                    path('restore/<str:id>/', categori.restore, name='admin_categori_restore'),
                    
                ]
            )),

            # MASTER TAHUN================================================
            path('tahun/', include(
                [
                    path('', tahun.admin_index, name='admin_tahun'),
                    path('create/', tahun.create, name='admin_tahun_create'),
                    path('edit/<str:id>/', tahun.edit, name='admin_tahun_edit'),
                    path('soft_delete/<str:id>/', tahun.softDelete, name='admin_tahun_soft_delete'),
                    path('restore/<str:id>/', tahun.restore, name='admin_tahun_restore'),
                    
                ]
            )),
            # MASTER TURUNAN================================================
            path('tahapan/', include(
                [
                    path('', tahapan.indexTahapan, name='index_tahapan'),
                    path('create/', tahapan.create, name='admin_tahapan_create'),
                    path('edit/<str:id_tahapan>/', tahapan.edit, name='admin_tahapan_edit'),
                    path('soft_delete/<str:id_tahapan>/', tahapan.softDelete, name='admin_tahapan_soft_delete'),
                    path('restore/<str:id_tahapan>/', tahapan.restore, name='admin_tahapan_restore'),
                    
                ]
            )),

            # MASTER KEGIATAN================================================
            path('kegiatan/', include(
                [
                    path('', kegiatan.indexKegiatan, name='index_kegiatan'),
                    path('create/', kegiatan.create, name='admin_kegiatan_create'),
                    path('edit/<str:id_kegiatan>/', kegiatan.edit, name='admin_kegiatan_edit'),
                    path('soft_delete/<str:id_kegiatan>/', kegiatan.softDelete, name='admin_kegiatan_soft_delete'),
                    path('restore/<str:id_kegiatan>/', kegiatan.restoreKegiatan, name='admin_kegiatan_restore'),
                    path('parse-kegiatan-excel/', kegiatan.ParseKegiatanExcelViews.as_view(),name='parse_kegiatan_excel'),
                    
                ]
            )),

            # MASTER WILAYAH================================================
            path('wilayah/', include(
                [
                    path('', wilayah.admin_index, name='admin_wilayah'),
                    path('create/', wilayah.create, name='admin_wilayah_create'),
                    path('edit/<str:id>/', wilayah.edit, name='admin_wilayah_edit'),
                    path('soft_delete/<str:id>/', wilayah.softDelete, name='admin_wilayah_soft_delete'),
                    path('restore/<str:id>/', wilayah.restore, name='admin_wilayah_restore'),
                    
                ]
            )),

             # MASTER SKPD================================================
            path('opd/', include(
                [
                    path('', skpd.admin_index, name='admin_skpd'),
                    path('create/', skpd.create, name='admin_skpd_create'),
                    path('edit/<str:id>/', skpd.edit, name='admin_skpd_edit'),
                    path('soft_delete/<str:id>/', skpd.softDelete, name='admin_skpd_soft_delete'),
                    path('restore/<str:id>/', skpd.restore, name='admin_skpd_restore'),
                    
                ]
            )),

             # MASTER PERUSAHAAN================================================
            path('perusahaan/', include(
                [
                    path('', company.admin_index, name='admin_perusahaan'),
                    path('create/', company.create, name='admin_perusahaan_create'),
                    path('edit/<str:id>/', company.edit, name='admin_perusahaan_edit'),
                    path('soft_delete/<str:id>/', company.softDelete, name='admin_perusahaan_soft_delete'),
                    path('restore/<str:id>/', company.restore, name='admin_perusahaan_restore'),
                    
                ]
            )),

            # MASTER PEJABAT PENGESAH================================================
            path('pejabatpengesah/', include(
                [
                    path('', pengesah.admin_index, name='admin_pejabat_pengesah'),
                    path('create/', pengesah.create, name='admin_pejabatpengesah_create'),
                    path('edit/<str:id>/', pengesah.edit, name='admin_pejabatpengesah_edit'),
                    path('soft_delete/<str:id>/', pengesah.softDelete, name='admin_pejabat_soft_delete'),
                    path('restore/<str:id>/', pengesah.restore, name='admin_pejabat_restore'),
                    
                ]
            )),
            # MASTER PAJAK================================================
            path('pajak/', include(
                [
                    path('', pajak.admin_index, name='admin_pajak'),
                    path('create/', pajak.create, name='admin_pajak_create'),
                    path('edit/<str:id>/', pajak.edit, name='admin_pajak_edit'),
                    path('soft_delete/<str:id>/', pajak.softDelete, name='admin_pajak_soft_delete'),
                    path('restore/<str:id>/', pajak.restore, name='admin_pajak_restore'),
                    
                ]
            )),

            # MASTER PEMBAYARAN================================================
            path('masterstatuspembayaran/', include(
                [
                    path('', statuspembayaran.admin_index, name='admin_status_pembayaran'),
                    path('create/', statuspembayaran.create, name='admin_status_pembayaran_create'),
                    path('edit/<str:id>/', statuspembayaran.edit, name='admin_status_pembayaran_edit'),
                    path('soft_delete/<str:id>/', statuspembayaran.softDelete, name='admin_status_pembayaran_soft_delete'),
                    path('restore/<str:id>/', statuspembayaran.restore, name='admin_status_pembayaran_restore'),
                    
                ]
            )),
            # SPK================================================
            path('trans_spk/', include(
                [
                    path('', spk_.admin_index, name='admin_spk'),
                    path('create/', spk_.create, name='admin_spk_create'),
                    path('edit/<str:id_spk>/', spk_.edit, name='admin_spk_edit'),
                    path('soft_delete/<str:id_spk>/', spk_.softDelete, name='admin_spk_soft_delete'),
                    path('restore/<str:id>/', spk_.restore, name='admin_spk_restore'),
                    path('detail/<str:id_spk>/', spk_.detailSPK, name='admin_spk_detail'),
                    path('createRincianSPK/<str:id>/', spk_.createRincianSPK, name='admin_rincian_spk_create'),
                    path('insertsubrincian/<str:id>/', spk_.insertsubrian, name='admin_insertsubrincian'),
                    path('editsubrincian/<str:id>/<str:id_sub>/', spk_.editsubrian, name='admin_editsubrincian'),
                    path('print_spk/<str:id>/', spk_.cetakSPK, name='admin_spk_print_'),

                ]
            )),

             # PERMOHONAN PEMBAYARAN================================================
            path('permohonan_pembayaran/', include(
                [
                    path('', permohonan.admin_index, name='admin_permohonan_pembayaran'),
                    path('create/<str:id_spk>/', permohonan.create, name='admin_permohonan_create'),
                    path('edit/<str:id_spp>/', permohonan.editSPP, name='admin_permohonan_edit'),
                    path('print/<str:id>/', permohonan.print_permohonan, name='admin_permohonan_cetak'),
                    path('detail_pembayaran/<str:id_spk>/', permohonan.detailBA, name='admin_detail_pembayaran'),
                    path('cetakpembayaran/<str:id>/', permohonan.cetakBA, name='admin_permohonan_cetakba'),
                    path('printkwitansi/<str:id>/', permohonan.printkwitansi, name='admin_permohonan_cetak_kwitansi'),
                    path('cetak_kwitansi/<str:id>/', permohonan.cetakKwitansi, name='admin_permohonan_cetakkwitansi'),
                    # path('soft_delete/<str:id>/', spk_.softDelete, name='admin_spk_soft_delete'),
                    # path('restore/<str:id>/', spk_.restore, name='admin_spk_restore'),
                    # path('detail/<str:id>/', spk_.detailSPK, name='admin_spk_detail'),
                    # path('createRincianSPK/<str:id>/', spk_.createRincianSPK, name='admin_rincian_spk_create'),
                    # path('insertsubrincian/<str:id>/', spk_.insertsubrian, name='admin_insertsubrincian'),
                ]
            )),

            # MANAGEMEN BERKAS================================================
            path('managemen_berkas/', include(
                [
                    path('', managemen.admin_index, name='admin_managemen_berkas'),
                    path('create/<str:id_spk>/', managemen.create, name='admin_managemen_create'),
                    path('edit/<str:id_spk>/', managemen.editBAST, name='admin_managemen_edit'),
                    path('print/<str:id>/', managemen.printBAST, name='admin_managemen_print'),
                    path('cetak_bast/<str:id>/', managemen.cetakBAST, name='admin_managemen_cetakbast'),
                   
                    path('bap/', managemen.bap, name='admin_managemen_bap'),
                    path('edit_bap/<str:id_spk>/', managemen.edit_BAP, name='admin_managemen_edit_BAP'),
                    path('create_bap/<str:id_spk>/', managemen.create_BAP, name='admin_managemen_create_BAP'),
                    path('print_BAP/<str:id>/', managemen.printBAP, name='admin_managemen_print_BAP'),
                    path('cetak_BAP/<str:id>/', managemen.cetakBAP, name='admin_managemen_cetak_BAP'),

                    path('faktur/', managemen.faktur, name='admin_managemen_faktur'),
                    path('faktur_create_no/<str:id_spk>/', managemen.create_NOFAKTUR, name='admin_managemen_create_no_faktur'),
                    path('faktur_cedit_no/<str:id_faktur>/', managemen.edit_NOFAKTUR, name='admin_managemen_edit_no_faktur'),
                    path('detail_faktur/<str:id>/', managemen.detailFAKTUR, name='admin_faktur_detail'),
                    path('cerate_detail_faktur/<str:id_faktur>/', managemen.create_RINCIANFAKTUR, name='admin_create_rincian_faktur_detail'),
                    path('edit_detail_faktur/<str:id>/', managemen.edit_RINCIANFAKTUR, name='admin_edit_rincian_faktur_detail'),
                    path('print_faktur/<str:id>/', managemen.printFaktur, name='admin_managemen_print_faktur_tagihan'),
                    path('load_modal_bap/<str:id>/', managemen.ModalBAP, name='admin_managemen_load_modal_bap'),
                    path('cetak_faktur_bap/<str:id>/', managemen.cetakBAPFaktur, name='admin_managemen_cetak_faktur_bap'),
                    path('delete/<str:id>/', managemen.DeleteDetail, name='admin_detail_faktur_delete'),
                    path('cetak_faktur/<str:id>/', managemen.cetakFaktur, name='admin_managemen_cetak_faktur_tagihan'),
                    path('modal_faktur_pajak/<str:id>/', managemen.ModalFakturPajak, name='modal_print_faktur_pajak'),
                    path('cetak_faktur_pajak/<str:id>/', managemen.cetakFakturPajak, name='admin_managemen_cetak_faktur_pajak'),
                    path('modal_ssp/<str:id>/', managemen.ModalSSP, name='modal_print_ssp'),
                    path('cetak_ssp/<str:id>/', managemen.cetakSSP, name='admin_managemen_cetak_ssp'),
                    # path('soft_delete/<str:id>/', spk_.softDelete, name='admin_spk_soft_delete'),
                    # path('restore/<str:id>/', spk_.restore, name='admin_spk_restore'),
                    # path('detail/<str:id>/', spk_.detailSPK, name='admin_spk_detail'),
                    # path('createRincianSPK/<str:id>/', spk_.createRincianSPK, name='admin_rincian_spk_create'),
                    # path('insertsubrincian/<str:id>/', spk_.insertsubrian, name='admin_insertsubrincian'),
                ]
            )),

              # DOKUMENTASI PEKERJAAN================================================
            path('dokumentasi/', include(
                [
                    path('', dokumentasi_pekerjaan.admin_index, name='admin_dokumentasi_pekerjaan'),
                    path('create/', dokumentasi_pekerjaan.create, name='admin_dokumentasi_create'),
                    path('print/<str:id>/', dokumentasi_pekerjaan.printdoc, name='admin_dokumentasi_print'),
                    path('get-urai-doc/<str:id>/', dokumentasi_pekerjaan.getUrai, name='get_urai_doc'),
                    # path('print/<str:id>/', permohonan.print, name='admin_permohonan_cetak'),
                    # path('printkwitansi/<str:id>/', permohonan.printkwitansi, name='admin_permohonan_cetak_kwitansi'),
                    # path('soft_delete/<str:id>/', spk_.softDelete, name='admin_spk_soft_delete'),
                    # path('restore/<str:id>/', spk_.restore, name='admin_spk_restore'),
                    # path('detail/<str:id>/', spk_.detailSPK, name='admin_spk_detail'),
                    # path('createRincianSPK/<str:id>/', spk_.createRincianSPK, name='admin_rincian_spk_create'),
                    # path('insertsubrincian/<str:id>/', spk_.insertsubrian, name='admin_insertsubrincian'),
                ]
            )),

              # REKAPITULASI TAGIHAN ================================================
            path('rekapitulasi_tagihan/', include(
                [
                    path('', laporan.admin_index, name='admin_rekapitulasi_tagihan'),
                    path('print_laporan_tagihan/', laporan.CetakLaporanTagihan, name='cetak_laporan_tagihan'),
                    path('print_laporan_tagihan_excel/', laporan.CetakLaporanTagihanExcel, name='cetak_laporan_tagihan_excel'),
                ]
            )),
              # REALISASI ================================================
            path('realisasi/', include(
                [
                    path('', realisasi.admin_index, name='admin_realisasi'),
                    path('print_laporan_realisasi/', realisasi.CetakLaporanRealisasi, name='cetak_laporan_realisasi'),
                ]
            )),
              # SISA DANA ================================================
            path('sisa_dana/', include(
                [
                    path('', sisa_dana.admin_index, name='admin_sisa_dana'),
                    path('print_laporan_sisa_dana/', sisa_dana.CetakLaporanTagihan, name='cetak_laporan_sisa_dana'),
                ]
            )),

            path('utility/', include(
                [
                    path('render_kegiatan', utility_views.render_kegiatan.as_view(), name='render_kegiatan'),
                ]
            )),
        ]
    ))
]