import re, math, os
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.conf import settings
import io, sys, os, uuid, time, math, random
from django.db.models.functions import Cast
from django.db import models
from django.core import files
from PIL import Image, ImageDraw, ImageFilter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from io import BytesIO
from rizq_sanjaya_app.models import SPK, DokumentasiPekerjaan
from django.db.models import Sum, F, Value, DecimalField, Q
from django.db.models.functions import Coalesce

def admin_only():
    def decorator(func):
        def wrap(request, redirect_to = 'admin:index_admin', *args, **kwargs):
            if request.user.role == 'admin':
                return func(request, *args, **kwargs)
            else:
                messages.error(request, "Anda tidak diperbolehkan mengakses halaman ini.")
                return redirect(reverse(redirect_to))
        return wrap
    return decorator
    
def check_is_email(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
    # pass the regular expression
    # and the string into the fullmatch() method
    if(re.fullmatch(regex, email)):
        return True
    else:
        return False

def truncate(f, n):
    return math.floor(f * 10 ** n) / 10 ** n

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0 B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return f"{str(s).replace('.', ',')} {size_name[i]}"

def get_folder_size(path):
    size_media = 0

    for path, dirs, files in os.walk(path):
        for f in files:
            fp = os.path.join(path, f)
            size_media += os.path.getsize(fp)
    size_media = convert_size(size_media)

    return size_media

def get_size_format(b, factor=1024, suffix="B"):
    """
    Scale bytes to its proper byte format
    e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if b < factor:
            return f"{b:.2f}{unit}{suffix}"
        b /= factor
    return f"{b:.2f}Y{suffix}"


class PagingHelper:
    keyword_query = ''
    page_num = 1
    per_page = 10
    # def __init__(self):

    @classmethod
    def __filtering_spk(cls, keyword_query, args):
        kwargs_filter = {}

        if cls.keyword_query != '':
            # tglspk
            kwargs_filter['nospk__icontains'] = cls.keyword_query
            kwargs_filter['uraipekerjaan__icontains'] = cls.keyword_query
            kwargs_filter['opd__namaopd__icontains'] = cls.keyword_query
            kwargs_filter['perusahaan__namaperusahaan__icontains'] = cls.keyword_query

            for x in args:
                kwargs_filter[x] = cls.keyword_query

        return kwargs_filter

    @classmethod
    def __paging(cls, data, page_num, per_page):
        paginator = Paginator(data, per_page)
        try:
            page_obj = paginator.page(page_num)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)

        return page_obj

    @classmethod
    def __argument_init(cls, request, args):
        cls.keyword_query = request.GET.get('query', '').strip()
        cls.page_num = request.GET.get('page', 1)
        cls.per_page = request.GET.get('per_page', 10)
        
        return cls.__filtering_spk(cls.keyword_query, args)

    @classmethod
    def __basic_query(cls, request, model, *additional_filter, **kwargs):
        
        kwargs_filter = cls.__argument_init(request, additional_filter)

        if kwargs['use_related']:
            query = model.objects.prefetch_related(kwargs['related_field']).filter(Q(**kwargs_filter, _connector=Q.OR), status='Aktif')
        else:
            query = model.objects.filter(Q(**kwargs_filter, _connector=Q.OR), status='Aktif')

        query = query.annotate(total_tagihan=Coalesce(Sum(F('spkrincian__realisasi'), output_field=DecimalField()), Value(0, output_field=DecimalField()))).order_by('-created_at') 

        return cls.__paging(query, cls.page_num, cls.per_page)

    @classmethod
    def __basic_query_dokumentasipekerjaan(cls, request, model, *additional_filter, **kwargs):
        
        kwargs_filter = cls.__argument_init(request, additional_filter)

        if kwargs['use_related']:
            query = model.objects.select_related(kwargs['related_field']).filter(Q(**kwargs_filter, _connector=Q.OR), status='Aktif')
        else:
            query = model.objects.filter(Q(**kwargs_filter, _connector=Q.OR), status='Aktif')

        # query = query.annotate(total_tagihan=Coalesce(Sum(F('spkrincian__realisasi'), output_field=DecimalField()), Value(0, output_field=DecimalField()))).order_by('-created_at') 

        return cls.__paging(query, cls.page_num, cls.per_page)


    @classmethod
    def spk(cls, request):
        page_obj = cls.__basic_query(request, SPK, use_related = False)
        return page_obj

    @classmethod
    def spp(cls, request):
        page_obj = cls.__basic_query(request, SPK, 'spp__nosurat__icontains', use_related = True, related_field = 'spp_set') 
        return page_obj

    @classmethod
    def bast(cls, request):
        page_obj = cls.__basic_query(request, SPK, 'bast__nobap__icontains', use_related = True, related_field = 'bast_set')
        return page_obj

    @classmethod
    def bap(cls, request):
        page_obj = cls.__basic_query(request, SPK, 'bap__nobap__icontains', use_related = True, related_field = 'bap_set')
        return page_obj

    @classmethod
    def fakturpajak(cls, request):
        page_obj = cls.__basic_query(request, SPK, 'fakturpajak__nofaktur__icontains', use_related = True, related_field = 'fakturpajak_set')
        return page_obj

    @classmethod
    def dokumentasipekerjaan(cls, request):
        page_obj = cls.__basic_query_dokumentasipekerjaan(request, DokumentasiPekerjaan, use_related = True, related_field = 'SPK')
        return page_obj




def paging():
    pass


def upload_crop_img(inmem_image, path_to_save, id_x, id_y, id_w, id_h):
    filename = f'{str(uuid.uuid4())}.jpg'
    try:
        x = float(id_x)
        y = float(id_y)
        w = float(id_w)
        h = float(id_h)
        
        output = BytesIO()
        image = Image.open(inmem_image)
        if image.mode in ("RGBA", "P"): image = image.convert("RGB")
        cropped_image = image.crop((x, y, w + x, h + y))

        # after modifications, save it to the output
        cropped_image.save(output, format='JPEG')
        output.seek(0)

        return InMemoryUploadedFile(output,       # file
                                 'ImageField',               # field_name
                                 f"{path_to_save}{filename}",           # file name
                                 'image/jpeg',       # content_type
                                 sys.getsizeof(cropped_image),  # size
                                 None)            # content_type_extra)

    except Exception as e:
        raise Exception(f'Debug crop image {e}')


def display_value(choices,field):
    options = [
         When(**{field: k, 'then': Value(v) })
          for k,v in choices
    ]
    return Case(
        *options,output_field=CharField()
    )


def format_clear(value):
    value = value.replace('.', '').replace(',', '.')
    value = float(value)
    value = int(value)
    return value

def get_data_all_section(request):
    tahun = request.session.get('tahun', None)

    data_transaksi_departemen = trans_departement.objects.filter(penganggaran_detail__penganggaran__tahun=tahun ,deleted_at__isnull=True).order_by('-created_at')
    dana_terpakai_departemen = data_transaksi_departemen.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0

    data_transaksi_operasional = T_Operasional.objects.filter(penganggaran_detail__penganggaran_operasional__tahun = tahun,deleted_at__isnull = True).order_by('-created_at')
    dana_terpakai_operasional= data_transaksi_operasional.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0

    data_transaksi_gaji = trans_gaji.objects.filter(penganggaran_detail__penganggaran__tahun = tahun, deleted_at__isnull=True).order_by('-created_at')
    dana_terpakai_gaji = data_transaksi_gaji.aggregate(total_jumlah=Sum('nilai_terpakai'))['total_jumlah'] or 0

    return {
        "dana_terpakai_departemen": dana_terpakai_departemen,
        "dana_terpakai_operasional": dana_terpakai_operasional,
        "dana_terpakai_gaji": dana_terpakai_gaji,
    }


def get_data_sidebar(request, value):
    jumlah_anggaran_departemen = 0
    dana_terpakai_departemen = 0
    persentase_dana_terpakai_departemen = 0 
    tahun = request.session.get('tahun', None)


    if value == 'departemen':
        data_transaksi = trans_departement.objects.filter(penganggaran_detail__penganggaran__tahun=tahun ,deleted_at__isnull=True).order_by('-created_at')
        penganggaran = t_anggaran.objects.filter(deleted_at__isnull=True, tahun=tahun)

        jumlah_anggaran_departemen = penganggaran.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0
        dana_terpakai_departemen = data_transaksi.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0

        if jumlah_anggaran_departemen > 0:  
            persentase_dana_terpakai_departemen = (dana_terpakai_departemen / jumlah_anggaran_departemen) * 100
            persentase_dana_terpakai_departemen = int(round(persentase_dana_terpakai_departemen))  
        else:
            persentase_dana_terpakai_departemen = 0
        
        data_all_sections = get_data_all_section(request)
    
    elif value == 'operasional':
        data_transaksi = T_Operasional.objects.filter(penganggaran_detail__penganggaran_operasional__tahun = tahun,deleted_at__isnull = True).order_by('-created_at')
        penganggaran = Penganggaran_Operasional.objects.filter(deleted_at__isnull = True)

        jumlah_anggaran_departemen = penganggaran.aggregate(total_jumlah=Sum('subtotal_operasional'))['total_jumlah'] or 0
        dana_terpakai_departemen = data_transaksi.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0

        data_transaksi_gaji = trans_gaji.objects.filter(penganggaran_detail__penganggaran__tahun=tahun ,deleted_at__isnull=True).order_by('-created_at')

        if jumlah_anggaran_departemen > 0:
            persentase_dana_terpakai_departemen = (dana_terpakai_departemen / jumlah_anggaran_departemen) * 100
            persentase_dana_terpakai_departemen = int(round(persentase_dana_terpakai_departemen))
        else:
            persentase_dana_terpakai_departemen = 0

    elif value == 'gaji':
        data_transaksi = trans_gaji.objects.filter(penganggaran_detail__penganggaran__tahun = tahun, deleted_at__isnull=True).order_by('-created_at')
        penganggaran = t_anggaran_gaji.objects.filter(deleted_at__isnull=True)

        jumlah_anggaran_departemen = penganggaran.aggregate(total_jumlah=Sum('nilai'))['total_jumlah'] or 0
        dana_terpakai_departemen = data_transaksi.aggregate(total_jumlah=Sum('nilai_terpakai'))['total_jumlah'] or 0

        if jumlah_anggaran_departemen > 0:  
            persentase_dana_terpakai_departemen = (dana_terpakai_departemen / jumlah_anggaran_departemen) * 100
            persentase_dana_terpakai_departemen = int(round(persentase_dana_terpakai_departemen))  
        else:
            persentase_dana_terpakai_departemen = 0

    elif value == 'dashboard':
        data_transaksi_gaji = trans_gaji.objects.filter(penganggaran_detail__penganggaran__tahun=tahun ,deleted_at__isnull=True).order_by('-created_at')
        penganggaran_gaji = t_anggaran_gaji.objects.filter(tahun=tahun, deleted_at__isnull=True)

        jumlah_anggaran_gaji = penganggaran_gaji.aggregate(total_jumlah=Sum('nilai'))['total_jumlah'] or 0
        dana_terpakai_gaji = data_transaksi_gaji.aggregate(total_jumlah=Sum('nilai_terpakai'))['total_jumlah'] or 0

        data_transaksi = trans_departement.objects.filter(penganggaran_detail__penganggaran__tahun=tahun, deleted_at__isnull=True).order_by('-created_at')
        penganggaran = t_anggaran.objects.filter(tahun=tahun, deleted_at__isnull=True)

        jumlah_anggaran = penganggaran.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0
        dana_terpakai = data_transaksi.aggregate(total_jumlah=Sum('subtotal'))['total_jumlah'] or 0

        dana_terpakai_departemen = dana_terpakai_gaji + dana_terpakai
        jumlah_anggaran_departemen = jumlah_anggaran_gaji + jumlah_anggaran


        if jumlah_anggaran_departemen > 0:  
            persentase_dana_terpakai_departemen = (dana_terpakai_departemen / jumlah_anggaran_departemen) * 100
            persentase_dana_terpakai_departemen = int(round(persentase_dana_terpakai_departemen))  
        else:
            persentase_dana_terpakai_departemen = 0

        data_all_sections = get_data_all_section(request)


    data = {
        'persentase_dana_terpakai': persentase_dana_terpakai_departemen,
        'jumlah_anggaran': jumlah_anggaran_departemen,
        'dana_terpakai': dana_terpakai_departemen
    }
    return data


def hitung_perubahan_persen(tahun, tahun_sebelumnya, model, field_name):

    query = model.objects.filter(
        tahun__in=[tahun, tahun_sebelumnya],
        deleted_at__isnull=True
    ).annotate(
        tahun_int=Cast('tahun', models.IntegerField())
    ).values('tahun_int').annotate(
        total_jumlah=Sum(field_name)
    )

    print(query)
    jumlah_tahun_sekarang = next((data['total_jumlah'] for data in query if data['tahun_int'] == int(tahun)), 0)
    jumlah_tahun_sebelumnya = next((data['total_jumlah'] for data in query if data['tahun_int'] == tahun_sebelumnya), 0)

    if jumlah_tahun_sebelumnya > 0:
        perubahan_persen = ((jumlah_tahun_sekarang - jumlah_tahun_sebelumnya) / jumlah_tahun_sebelumnya) * 100
        status = 'naik' if perubahan_persen > 0 else ('turun' if perubahan_persen < 0 else 'sama')
        return int(perubahan_persen), status
    else:
        return 0,'sama'


class TOTPVerification:
    def __init__(self):
        self.digits = "0123456789"
        self.OTP = ""
        self.number_of_digits = settings.TOKEN_DIGIT
        self.number_of_order = settings.TOKEN_ORDER

        print('ini token', settings.TOKEN_DIGIT)
        # validity period of a token. Default is 30 second.
        self.token_validity_period = settings.TOKEN_VALIDATE_TIME_PERIODE

    def generate_pin(self):
        for i in range(self.number_of_digits) :
            self.OTP += self.digits[math.floor(random.random() * 10)]
        return self.OTP

    def generate_token(self):
        # get the TOTP object and use that to create token
        while True:
            otp_temp = self.generate_pin()
            try:
                with transaction.atomic():
                    otp = OTP.objects.select_for_update().get(trx_otp_digit=otp_temp)
                    continue
            except Exception as e:
                return otp_temp



    def verify_token(self, email_client, token):
        
        data = {
            'status':False
        }
        try:
            data_token = OTP.objects.get(trx_email_associate = email_client, trx_otp_digit = token, otp_status = True, otp_category = 'verification_token')
            token_time = data_token.trx_expired
            diff = datetime.now() - token_time
            sec = diff.total_seconds()
            minutes_ = sec / 60
            if sec > 5:
                data['status'] = False
                data['message'] = 'Token has expired'
            else:
                data['status'] = True
                data['message'] = 'Token verified'
            all_data_token_associate = OTP.objects.filter(trx_email_associate = email_client).delete()
        except Exception as e:
            data['message'] = 'Token error'
            print('Error verify_token support', e)
        return data

def send_email_otp(recepient, token):
    print('ini adalah token', settings.APP_TITLE_)
    msg = render_to_string('email_template/verifikasi_otp.html', {'token':token})
    email = EmailMultiAlternatives(f'[OTP] Kode Verifikasi', msg, f'{settings.APP_TITLE_} <{settings.EMAIL_SENDER_ALIAS}>',recepient)
    email.content_subtype = "html"
    return email.send()