from django import template
from datetime import date
from num2words import num2words
import roman


register = template.Library()

@register.filter
def lower(value):
    return value.lower()
    
@register.filter(name='get_item')
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def batch(sequence, count):
    result = []
    for i in range(0, len(sequence), count):
        result.append(sequence[i:i + count])
    return result

register = template.Library()

@register.filter(name='capital_case')
def capital_case(value):
    words = value.split()
    return ' '.join(word.capitalize() for word in words)

@register.filter(name='terbilang')
def terbilang(value):
    if isinstance(value, str):
        try:
            value = int(value)
        except ValueError:
            return "Invalid input" 
    return f"{num2words(value, lang='id')} rupiah".title()

@register.filter(name='terbilang_angka')
def terbilang_angka(value):
    if isinstance(value, str):
        try:
            value = int(value)
        except ValueError:
            return "Invalid input"  
    return f"{num2words(value, lang='id')}".title()

@register.filter(name='index_to_letter')
def index_to_letter(index):
    """Converts a zero-based index to a letter (A, B, C, ...)."""
    return chr(index + 65) 

@register.filter(name='index_to_roman')
def index_to_roman(index):
    """Converts a zero-based index to a Roman numeral (I, II, III, ...)."""
    return roman.toRoman(index + 1)


@register.filter(name='conditional_counter')
def conditional_counter(context, condition, counter_name):
    # Get the counter from the context or initialize it
    counter = context.get(counter_name, 0)
    
    # Increment counter if the condition matches
    if condition:
        counter += 1
        
    # Store the updated counter back in the context
    context[counter_name] = counter
    return counter



bulanIndonesia = [
    'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
    'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
]


hariIndonesia = [
    'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'
]


def terbilangnilai(angka):
    bilangan = [
        'Nol', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan', 
        'Sepuluh', 'Sebelas', 'Dua Belas', 'Tiga Belas', 'Empat Belas', 'Lima Belas', 'Enam Belas',
        'Tujuh Belas', 'Delapan Belas', 'Sembilan Belas', 'Dua Puluh', 'Dua Puluh Satu', 'Dua Puluh Dua',
        'Dua Puluh Tiga', 'Dua Puluh Empat', 'Dua Puluh Lima', 'Dua Puluh Enam', 'Dua Puluh Tujuh',
        'Dua Puluh Delapan', 'Dua Puluh Sembilan', 'Tiga Puluh', 'Tiga Puluh Satu'
    ]
    if angka < 32: 
        return bilangan[angka]
    return str(angka)

def tahun_terbilang(tahun):
    return num2words(tahun, lang='id').capitalize()


@register.filter(name='tanggal_lafal')
def tanggal_lafal(value):
    if not value:
        return ""
    
    try:
        tanggal = value
        day = tanggal.day
        month = tanggal.month - 1  
        year = tanggal.year
        weekday = hariIndonesia[tanggal.weekday()]
        
        tanggal_terbilang = (
            f"{weekday} Tanggal {terbilangnilai(day)} "
            f"Bulan {bulanIndonesia[month]} "
            f"Tahun {tahun_terbilang(year)}"
        )
        return tanggal_terbilang
    except Exception as e:
        return f"Error: {str(e)}"

@register.filter
def intcomma_no_decimal(value):
    try:
        value = float(value)
        if isinstance(value, float) and value.is_integer():
            return str(value).split('.')[0]
        # Check if it's an integer
        elif isinstance(value, int):
            return str(value)
        return round(value, 2)
    except Exception as e:
        return value