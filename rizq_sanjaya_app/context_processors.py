import os
import numpy

from datetime import datetime
from .models.profile import AppSetting, MasterTahapan

def global_variables(request):
    # links = Link.objects.all()
    # menu_pages = Halaman.objects.filter(deleted_at=None).order_by('id')
    # notif_complaint = Complaint.objects.filter(deleted_at=None,  read_status=False).count()
    # notif_guestbook = GuestBook.objects.filter(deleted_at=None,  read_status=False).count()
    # payment_footer = AboutMe.objects.filter(jenis='header', section='payment')

    app_setting = AppSetting.objects.all()
    nama_website = alamat = alamat_link = embed_maps = telepon = telepon_text  = whatsapp = whatsapp_text = email = 'Belum Disetting'
    meta_keywords = meta_author = meta_property_title = meta_property_description = 'Belum Disetting'
    link_facebook = link_twitter = link_youtube = link_instagram = link_tiktok = 'Belum Disetting'
    nomor_spk = nomor_bap = nomor_bast = nomor_bapa =nomor_faktur =nomor_dok = 'Belum Disetting'
    header_layanan = header_galeri = header_kontak = 'Belum Disetting' 
    about_text = newletter_text = 'Lorem ipsum dolor sit amet'

    for appx in app_setting:
        if appx.nama == 'nama_website':
            nama_website = appx.keterangan
        elif appx.nama == 'alamat':
            alamat = appx.keterangan
        elif appx.nama == 'alamat_link':
            alamat_link = appx.keterangan
        elif appx.nama == 'embed_maps':
            embed_maps = appx.keterangan
        elif appx.nama == 'telepon':
            telepon = appx.keterangan
        elif appx.nama == 'telepon_text':
            telepon_text = appx.keterangan
        elif appx.nama == 'whatsapp':
            whatsapp = appx.keterangan
        elif appx.nama == 'whatsapp_text':
            whatsapp_text = appx.keterangan
        elif appx.nama == 'email':
            email = appx.keterangan
        elif appx.nama == 'meta_keywords':
            meta_keywords = appx.keterangan
        elif appx.nama == 'meta_author':
            meta_author = appx.keterangan
        elif appx.nama == 'meta_property_title':
            meta_property_title = appx.keterangan
        elif appx.nama == 'meta_property_description':
            meta_property_description = appx.keterangan
        elif appx.nama == 'link_facebook':
            link_facebook = appx.keterangan
        elif appx.nama == 'link_instagram':
            link_instagram = appx.keterangan
        elif appx.nama == 'link_youtube':
            link_youtube = appx.keterangan
        elif appx.nama == 'link_twitter':
            link_twitter = appx.keterangan
        elif appx.nama == 'link_tiktok':
            link_tiktok = appx.keterangan
        elif appx.nama == 'header_layanan':
            header_layanan = appx.keterangan
        elif appx.nama == 'header_galeri':
            header_galeri = appx.keterangan
        elif appx.nama == 'header_kontak':
            header_kontak = appx.keterangan
        elif appx.nama == 'about_text': 
            about_text = appx.keterangan
        elif appx.nama == 'newletter_text':
            newletter_text = appx.keterangan
        elif appx.nama == 'nomor_spk':
            nomor_spk = appx.keterangan
        elif appx.nama == 'nomor_bap':
            nomor_bap = appx.keterangan
        elif appx.nama == 'nomor_bast':
            nomor_bast = appx.keterangan
        elif appx.nama == 'nomor_bapa':
            nomor_bapa = appx.keterangan
        elif appx.nama == 'nomor_faktur':
            nomor_faktur = appx.keterangan
        elif appx.nama == 'nomor_dok':
            nomor_dok = appx.keterangan

        


    domain = '127.0.0.1:8000'
    try:
        domain = request.get_host()
    except:
        pass

    # deskripsi_pay = 'Cara pembayaran belum disetting, silahkan hubungi administrator.'
    # image_1 = image_2 = ''
    # for x in payment_footer:
    #     deskripsi_pay = x.deskripsi if x.deskripsi != None else 'Cara pembayaran belum disetting, silahkan hubungi administrator.'
    #     image_1 = x.youtube
    #     image_2 = x.icon
    try:
        tahapan = MasterTahapan.objects.is_active()
        tahapan_aktif = tahapan
        tahapan_aktif_id = tahapan.id_tahapan
        tahun_tahapan = str(tahapan.tahun.tahun)
    except Exception as e:
        print('cek', e)
        tahapan_aktif = None
        tahapan_aktif_id = None
        tahun_tahapan = None

    romawi = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"]

    bulan_sekarang = datetime.now().month
    bulan_romawi = romawi[bulan_sekarang - 1]


    return {
        'nama_website': nama_website,
        'alamat': alamat,
        'alamat_link': alamat_link,
        'embed_maps': embed_maps,
        'telepon': telepon,
        'telepon_text': telepon_text,
        'whatsapp': whatsapp,
        'whatsapp_text': whatsapp_text,
        'email': email,
        'meta_keywords': meta_keywords,
        'meta_author': meta_author,
        'meta_property_title': meta_property_title,
        'meta_property_description': meta_property_description,
        'link_facebook': link_facebook,
        'link_youtube': link_youtube,
        'link_twitter': link_twitter,
        'link_instagram': link_instagram,
        'link_tiktok': link_tiktok,

        'about_text' : about_text,
        'nomor_dok' : nomor_dok,
        'nomor_faktur' : nomor_faktur,
        'nomor_bapa' : nomor_bapa,
        'nomor_bast' : nomor_bast,
        'nomor_bap' : nomor_bap,
        'nomor_spk' : nomor_spk,
        'tahun_tahapan' : tahun_tahapan,
        'bulan_romawi' : bulan_romawi,
        'newletter_text' : newletter_text,
        # 'links': links,
        # 'menu_pages': menu_pages,
        # 'notif_complaint': notif_complaint,
        # 'notif_guestbook': notif_guestbook,
        'domain': domain,
        'header_layanan': header_layanan,
        'header_galeri': header_galeri,
        'header_kontak': header_kontak,
        'tahapan_aktif': tahapan_aktif,
        'tahapan_aktif_id': tahapan_aktif_id,
        # 'payment_footer': {'deskripsi':deskripsi_pay, 'image_1': image_1, 'image_2': image_2},
    }
